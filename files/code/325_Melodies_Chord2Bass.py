#325_Melodies_Chord2Bass

#Set beats per minute
Clock.bpm=128
#Set root note
Root.default="A"
#Set scale
Scale.default=Scale.minor
#Chord progression
chords = var([(0,2,4,6),(1,1,3,6),(2,4,6),(1,4,6)])
#First bass example
bassline=var([0,-6,2,4],4)
bassline2=[0,0,0,-1,-1,-1,-1,2,2,2,2,-3,-3,-3,-3,0] 
bassline3=[0,0,0,0,-1,-1,-1,-1,2,2,2,2,-3,-3,-3,-3]
bassline4=[0,-1,0,1,-1,-1,0,1,3,3,0,-1,-3-3,-2,-1]

print(SynthDefs)

#Synths

#Chords
s1 >> charm(chords,oct=4,dur=4,sus=4)

#Bass example 1
s2 >> orient(bassline,oct=3,dur=4,amplify=4/5)

#And with some rhythm
s3 >> pluck(bassline2,oct=3,dur=1,amplify=2)

#Bass example 2
s4 >> viola(bassline,oct=3,dur=Pvar([1,2,1],[1,2,1]),sus=1/2,amplify=4/5)

#Bass example 3
s5 >> pulse(bassline3,oct=[3,3,4,3],dur=1,amplify=4/5)

#Bass example 4
s6 >> pasha(bassline4,oct=3,dur=1/4,sus=1/4,shape=2/3,amplify=2/5)



b1 >> play("<[VV]....V..><..o.><---[--]>",sample=7)

b2 >> play(var(["[ss]",".[ss]"]),amplify=4/5)


