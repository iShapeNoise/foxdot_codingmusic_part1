#241_loop_synth


#Import Python module to get path
import os
path = os.getcwd() + "/"


#Set beats per minute
Clock.bpm=125
#Set root note to D#
Root.default.set(0)
#Set scale to a minor Scale P[0, 2, 3, 5, 7, 8, 10]
Scale.default = Scale.chromatic
#Get chords
bass=[1,3,3,1]
chords=[(1,3,6),(3,6,8),(3,6,10),(1,6,8)]
notes=[1,3,6,8,10]

#Use loop function to play recorded voices or analog instruments
p1 >> loop(path + "125bpm_robgrace.wav", dur=32)

#SynthDefs
s1 >> pluck(chords,oct=4,dur=PDur(3,8),sus=2,amplify=2/3).every(16,"shuffle")

s2 >> dbass(bass,oct=(4,5),dur=1/4,sus=1/2,chop=PDur(3,5),shape=1/5,formant=linvar([1,4],16),pan=linvar([-3/5,3/5],8),amplify=2/5)

s3 >> klank(bass,dur=1,chop=4,shape=2/3,formant=linvar([1,3],8),pan=[-2/3,2/3],amplify=2/5).offbeat()


#Drumset
b1 >> play("<V....V..V...[VV]V..><..o.><--[--]->",amplify=2/5)

b2 >> play("-",rate=3/4,sample=3,amplify=2/4)

b3 >> play(".s",sample=2)
