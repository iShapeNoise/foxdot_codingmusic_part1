#331_UsingModes_IntroExamples


#EXAMPLE 1



#Set speed
Clock.bpm=140
#Set root note
Root.default.set(2)
#Set scale
Scale.default=Scale.dorian
#Set bass lines
bassline=[0,2,4,0,7]
bassline2 = PShuf(bassline)
#Create chords made of bassline 3rd and 5th note, while inverse notes the are to high or to low for a good chord progression 
chords = Pvar([[-3,0,2],[-3,-1,2],[-3,-2,1],[-3,0,2],[-3,0,2]],[6,4,2,2,2])
#Melody line
seq = [2,-1,-3,2,5,4,2]

print(bassline2)

#Bassline 1
s1 >> klank(bassline,oct=4,dur=[6,4,2,2,2],shape=2/5,amplify=3/5,amp=var([0,1],[16,160]))
#Bassline 2
s2 >> space(PShuf(Scale.default),oct=3,shape=1/4,room=3/5,mix=1/2,amplify=3/5,amp=var([0,1],[32,144]))
#Chords
s3 >> pluck(chords,oct=6,dur=1/2,room=4/5,mix=1/2,shape=2/7,amplify=1/2,amp=var([0,1],[32,144]))
#Melody
s4 >> sinepad(seq,oct=6,dur=[3,3,2,3,1,2,2],room=3/4,mix=1/2,amplify=4/5,amp=var([0,1],[48,32,16,32,32,16]))


#Bass Drum
b1 >> play("V..v....",rate=4/5,sus=4,sample=2,formant=var([4,3,2,1],4),lpf=1000,amplify=3/5)
#Bass Drum2
b2 >> play("v..V....",rate=1,sample=-1,room=1/2,mix=1/2,amplify=4/5)
#Bass Drum 3
b3 >> play(Pvar(["..","V."],[128,48]),rate=2,sample=-1,amplify=3/5)
#Snare
b4 >> play(Pvar(["....o...","....o..o"],[12,4]),rate=7/5,sample=4,lpf=linvar([2400,4000],8),room=4/5,mix=1/2,amplify=3/5)
#Snare2
b5 >> play(Pvar(["..[i.].","..i[.i]"],[12,4]),sample=7,room=2/3,mix=1/2,amplify=3/5)
#Shaker
b6 >> play(".[ss]..",rate=7/5,room=1/2,mix=1/2)
#HiHat
b7 >> play("-.-.").every(16,"mirror")
#HiHat2
b8 >> play(Pvar(["..","-[--]"],[128,48]),sample=8,amplify=4/5)





#EXAMPLE 2



#Set speed
Clock.bpm=120
#Set root note
Root.default.set(4)
#Set scale
Scale.default=Scale.phrygian
#Set bass lines
bassline=[0,0,0,0,0,0,0,1]
bassline2=[-1,-1,-1,-1,-1,-1,-1,0]
#Create chords made of bassline 3rd and 5th note, while inverse notes the are to high or to low for a good chord progression 
chords = Pvar([(0,2,4),(0,2,5),(0,3,5),(0,3,4)])
chords2 = Pvar([(-1,2,4),(-1,2,5),(-1,3,5),(-1,3,4)])
#Melody line
seq = [2,-1,-3,2,5,4,2]

print(SynthDefs)

#Bass 1
s1 >> donk(Pvar([bassline,bassline2,bassline],[96,32,32]),oct=5,dur=1/2,shape=1/3,amplify=3/5,amp=var([0,1],[16,144]))

#Bass 2
s2 >> bass(Pvar([bassline,bassline2,bassline],[96,32,32]),oct=5,dur=1/2,shape=1/5,amplify=3/5,amp=var([0,1],[32,128]))

#Chords
s3 >> saw(Pvar([chords,chords2,chords],[96,32,32]),oct=6,dur=4,room=3/5,mix=1/2,lpf=1300,amplify=4/5,amp=var([0,1],[32,128]))

#Melody
s4 >> sinepad(seq,oct=6,dur=[3,3,2,3,1,2,2],shape=1/5,formant=0,room=3/4,mix=1/2,amplify=3/5,amp=Pvar([0,1],[48,16,16,16,16,16,16,16]))


#Bass Drum
b1 >> play("d..d....",rate=3/5,sus=2,sample=8,formant=1,lpf=1400,room=3/4,mix=1/2,amplify=3/5)
#Snare
b2 >> play(Pvar(["....g...","....g..g"],4),rate=1,sample=4,room=2/5,mix=1/2,amplify=2/5)
#HiHat
b3 >> play("----",rate=7/5,sample=2,amplify=2/5)

