#321_BillieJean_IntroExample

#Set beats per minute
Clock.bpm=117
#Set root note to E
Root.default="E"
#Set scale to a minor Scale P[0, 2, 3, 5, 7, 8, 10]
Scale.default = Scale.minor
#Get chords
chords=[(0,2,4),(0,1,3,5),(0,2,4,6),(0,1,3,5)]

#SynthDefs
s1 >> pluck(chords,oct=5,dur=[3/2,5/2],sus=2)

#Drumset
b1 >> play("<V....V..V...[VV]V..><..o.><---->",amplify=2/5)