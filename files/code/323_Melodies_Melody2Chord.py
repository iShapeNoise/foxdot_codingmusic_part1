#323_Melodies_Melody2Chord

#Set beats per minute
Clock.bpm=93
#Set root note
Root.default="A"
#Set scale
Scale.default=Scale.minor
#Create melody
#If you can not remember the scale list numbers, use 
print(Scale.minor)
#Melody sequence
seq=[0,1,2,1,4,5,2,6,4,3]
#Set the chords, after you figured a nice chord progression
chords = var([(0,2,4),(-1,1,3),(0,2,4),(-1,4,6)])


#Synths
s1 >> saw(seq,dur=[2,1,1,4,3,1,1,1,1,1],formant=4,amplify=2/5)

s2 >> keys(chords,oct=4,dur=4,shape=3/5)

#Beats
b1 >> play("<X....X..X..[X.].X..><..o.><---->")


