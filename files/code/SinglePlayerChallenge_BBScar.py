Clock.bpm = 70

Scale.default=Scale.melodicMajor

mtf1 = P[0, _, 1, 2, 0, _, -1, 3]
mtf2 = P[0, -2, 1, [-1, _], 0, 1, [_, -1], 2]
rnode = PRand(Scale.default, seed=PxRand(1, 32))

s1 >> dbass(Pvar([mtf1.offadd([-3, rnode]).zip([3, -2, [rnode, rnode * 1/2]]), mtf2.offadd([-1, 3, [rnode, rnode * 1/2]])], [16, 24, 8, 4, 12, 16]), 
    oct = var([4, PRand([5, 6, 7], seed = 9)], [1, 3]), 
    dur = [[4, 2], Pvar(P[2, 1, 1/2].offadd(1/3), [4, 12])], 
    sus = Pvar([12, 4], [1, 3]),
    shape = (P[0, PRand([-1/8, 0, 1/8]), 0, 1/12] - linvar([0, 1/12], PxRand(4, 16))) * PRand([0, 1], seed = 21),
    formant = [0, [1, 2], 0, expvar([0, 5], 4)],
    bend = [0, PRand([1/3, 1/2, -1/2, -1/3, 1/8, -1/8], seed = 7)], 
    benddelay = linvar([0, 1], [[12, 0],[4, 0]]),
    tremolo = P[[0, 3], 2, 0, 3].zip([0, 1]), 
    vib = PxRand([0, 8], seed = 10), 
    chop = var([0, PxRand([0, 16], seed = 12)], PxRand([2, 16], seed = 42)), 
    lpf = expvar([200, 2400], [4, 12, 16, 16, 8, 24]), 
    lpr = 1/3, 
    pan = [(-2/3, 2/3), PWhite(-2/3, 2/3)], 
    room = linvar([3/4, 1], PxRand([4, 32])), 
    mix = PRand([1/2, 1/3, 3/4], PxRand([2, 16]), seed = 19), 
    amplify= 5/9)
