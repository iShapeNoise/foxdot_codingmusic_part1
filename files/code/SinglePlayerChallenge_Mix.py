# KittyClock &      
# Modified by BBScar

# Import Python module
import string 
# TEMPO
Clock.bpm=200
# ROOT
Root.default.set(var([0, 2], 256))
# SCALE 
Scale.default=Scale.minor
print(Scale.default)
# NOTES
m1a = P[2, 6, 4, -2]
m1b = P[var([0, 2, -2, 2], 16), 4, 8]
m1c = P[var([[0, P*(0, 0, 0, 0, var([0, 8, 6, 4], 16))], 0], 16)]
m1d = P[var([[P*(8, 7, 6, 5, 4), P*(4, 6, 8)], 0], 16)]
m2a = P[0, _, 1, 2, 0, _, -1, 3]
m2b = P[0, -2, 1, [-1, _], 0, 1, [_, -1], 2]
m2c = PRand(Scale.default, seed=PxRand(1, 32))

# SYNTHS
s1 >> piano(
    var([(m1a, m1b, m1c, m1d), (m2a, m2b, m2c)], 128),
    amp=(1 * var([linvar([1, 1/4], 0.25), 1, PBern(16, 0.9)], 16), var([0.4, 0.6], 4)), dur=(1,2),
    oct=(4, PRand([5, 6], seed=7)),
    vib=0.5, 
    vibdepth=0.5,
    lpf=(var([0, 600], 32), linvar([400, 4000], 64)),
    chop=(linvar([0, 1], [28, 4]), 0),
    shape=(2/3, 0), 
    formant=(0, var([1, 0], 4)),
    slide=var([0, var([2, -0.5, 0], 3)], [3, 1]),
    room=PxRand(1, 2) / 2,
    mix=var([1/2, 4/5], [12, 4]),
    pan=(expvar([0, -0.5], 12), expvar([0, 0.5], 16))
)

s2 >> dbass(var([Pvar([m1a.offadd([-3, m1c]).zip([3, -2, [m1c, m1c * 1/2]]), m1b.offadd([-1, 3, [m1c, m1c * 1/2]])], [16, 24, 8, 16, 16, 16])
    , Pvar([m2a.offadd([-3, m2c]).zip([3, -2, [m2c, m2c * 1/2]]), m2b.offadd([-1, 3, [m2c, m2c * 1/2]])], [16, 24, 8, 24, 8, 16, 16])], 128), 
    oct=var([4, PRand([5, 6, 7], seed = 9)], [1, 3]), 
    dur=[[4, 8], Pvar(P[4, 2, 1].offadd(2/3), [8, 24])], 
    sus=Pvar([12, 4], [2, 6]),
    shape=(P[0, PRand([-1/8, 0, 1/8]), 0, 1/12] - linvar([0, 1/12], PxRand(4, 16))) * PRand([0, 1], seed = 21),
    formant=[0, [1, 2], 0, expvar([0, 5], 4)],
    bend=[0, PRand([1/3, 1/2, -1/2, -1/3, 1/8, -1/8], seed = 7)], 
    benddelay=linvar([0, 1], [[12, 0],[4, 0]]),
    tremolo=P[[0, 3], 2, 0, 3].zip([0, 1]), 
    vib=PxRand([0, 8], seed = 10), 
    chop=var([0, PxRand([0, 4], seed = 12)], PxRand([2, 4], seed = 42)), 
    lpf=expvar([200, 2400], [4, 12, 16, 16, 8, 24]), 
    lpr=PWhite(3/4, 1), 
    pan=[(-2/3, 2/3), PWhite(-2/3, 2/3)], 
    room=linvar([3/4, 1], PxRand([4, 32])), 
    mix=PRand([1/2, 1/3, 3/4], PxRand([2, 16]), seed = 19), 
    amplify=6/9
)

# SAMPLES
b1 >> play(
    (
        #Joining characters to strings using strings
        ''.join([
            str(var('X^V', 2)),
            str(var(['x', 't', 'h', 'd'], [[14, 28], [2, 4]])),
            str(var(['n', '-', ':', 'u'], 16)),
            'v'
        ]),
        var(string.ascii_lowercase, [14, 2])
    ),
    tremolo=(0, var([0, 4], [3, 1, 6, 2, 28, 4, 56, 8, 64, 0, 128, 32])),
    striate=var([0, 1, 2, 0], 8),
    pan=(0, PWhite(-0.5, 0.5)),
    sample=(
        [
            PRand(PRange(8, 15), 8),
            PRand(PRange(10), 16),
            var(PRange(10), 4),
            0
        ],
        PRand(PRange(20), 1.5)
    ),
    rate=(
        var([1, -1], [120, 8]) * var([1, 0.25], [56, 8]),
        var([1, -1], [16, 8, 128, 0, 64, 32, 64, 0]) * expvar([0.15, 0.75], 1/3)
    ),
    hpf=(var([0, 800], [14, 2]), var([0, 600], [56, 8])),
    hpr=0.75,
    lpf=(PWhite(2000, 5000), var([300, 600], [28, 4])),
    lpr=PWhite(0.85, 1),
    room=(0, 0, [1, 0, 0.5]), mix=0.25,
    dur=var([0.5, 0.25], [3, 1] * 4 + [6, 2] * 2 + [28, 4]) * var([2, 1], [6, 1])
)
