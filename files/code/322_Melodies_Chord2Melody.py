#322_Melodies_Chord2Melody

#Set beats per minute
Clock.bpm=93
#Set root note
Root.default="A"
#Set scale
Scale.default=Scale.minor
#Create chords
chords = var([(0,2,4),(-2,1,3),(0,3,5),(-2,2,4)])
seq=[4,5,3,5,3,6,4,3]

print(SynthDefs)

#Synths
s1 >> swell(chords,oct=4,dur=4,sus=5)

s2 >> varsaw(seq,oct=6,dur=[3,1,3,3,1,1,2,2])

#Beats
b1 >> play("<X....X..X..[X.].X..><..o.><---->")
