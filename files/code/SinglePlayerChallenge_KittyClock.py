Clock.bpm = 200

import string

sp >> play(
    (
        ''.join([
            str(var('X^V', 2)),
            str(var(['x', 't', 'h', 'd'], [[14, 28], [2, 4]])),
            str(var(['n', '-', ':', 'u'], 16)),
            'v'
        ]),
        var(string.ascii_lowercase, [14, 2])
    ),
    tremolo=(0, var([0, 4], [3, 1, 6, 2, 28, 4, 56, 8, 64, 0, 128, 32])),
    striate=var([0, 1, 2, 0], 8),
    pan=(0, PWhite(-0.5, 0.5)),
    sample=(
        [
            PRand(PRange(8, 15), 8),
            PRand(PRange(10), 16),
            var(PRange(10), 4),
            0
        ],
        PRand(PRange(20), 1.5)
    ),
    rate=(
        var([1, -1], [120, 8]) * var([1, 0.25], [56, 8]),
        var([1, -1], [16, 8, 128, 0, 64, 32, 64, 0]) * expvar([0.15, 0.75], 1/3)
    ),
    hpf=(var([0, 800], [14, 2]), var([0, 600], [56, 8])),
    hpr=0.75,
    lpf=(PWhite(2000, 5000), var([300, 600], [28, 4])),
    lpr=PWhite(0.85, 1),
    room=(0, 0, [1, 0, 0.5]), mix=0.25,
    dur=var([0.5, 0.25], [3, 1] * 4 + [6, 2] * 2 + [28, 4]) * var([2, 1], [6, 1])
)
