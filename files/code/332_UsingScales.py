#Chromatic scale of actual notes in a list
chromatic = [C, C#, D, D#, E, F, F#, G, G#, A, A#, B]

#therefore
chromatic = [0,1,2,3,4,5,6,7,8,9,10,11]

#show list of indices 
print(chromatic)

#Show list of available scales
print(Scale.names())
#['aeolian', 'altered', 'bebopDom', 'bebopDorian', 'bebopMaj', 'bebopMelMin', 'blues', 'chinese', 'chromatic', 'custom', 'default', 'diminished', 'dorian', 'dorian2', 'egyptian', 'freq', 'halfDim', 'halfWhole', 'harmonicMajor', 'harmonicMinor', 'hungarianMinor', 'indian', 'justMajor', 'justMinor', 'locrian', 'locrianMajor', 'lydian', 'lydianAug', 'lydianDom', 'lydianMinor', 'major', 'majorPentatonic', 'melMin5th', 'melodicMajor', 'melodicMinor', 'minMaj', 'minor', 'minorPentatonic', 'mixolydian', 'phrygian', 'prometheus', 'romanianMinor', 'susb9', 'wholeHalf', 'wholeTone', 'yu', 'zhi']

#Play up the chromatic scale one note per second
s1 >> piano(chromatic)

#To set the default scale use
Scale.default = Scale.minorPentatonic
#You also can use the name of scale in quotes
Scale.default = "minorPentatonic"

#Play a bit with different scales
s1 >> piano(Pvar([[0,1,3,5],[3,4,5,6]],16),oct=4)

#To use a different scale for a particular instrument use scale
s2 >> piano(Pvar([[3,5,7],[3,5,9]],12),scale=Scale.minor,oct=(5,6),dur=Pvar([PDur(3,5),1],8),amplify=PRand([1/2,3/4,1]))

s3 >> piano(Pvar([[0,2,3,5],[1,3,5,7]],8),oct=6,dur=var([PRand([1/4,1/2,1]),PRand([1/2,1])],4),amplify=PRand([1/2,3/4,1]))
