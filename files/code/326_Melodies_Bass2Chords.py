#326_Melodies_Bass2Chords

#Set beats per minute
Clock.bpm=120
#Set root note
Root.default="A"
#Set scale
Scale.default=Scale.minor

#First bass example
bassline=[0,0,0,0,0,0,0,1]

#Chord progression
chords = var([(0,2,4),(1,3,5),(0,2,4),(-1,1,3),(0,2,4)])


#Synths

#Bass 
s1 >> jbass(bassline,oct=3,dur=1/2,shape=2/5)

#Chords
s2 >> dirt(chords,oct=5,dur=[4,3,1,4,4],amplify=3/5)




b1 >> play("<V....V..><..o.><....k..d><---[--]>",sample=var([4,2],16),amplify=2/5)

b2 >> play(var(["[ss]",".[ss]"]),amplify=3/5)
