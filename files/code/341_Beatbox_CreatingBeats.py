#341_Beatbox_CreatingBeats


#BASICS

#Drum kick
b1 >> play("X...X...",amplify=2/3)

#Snare
b4 >> play("..o...o.",amplify=2/3)

b4 >> play("<..o...o.><.[.o]......>",amplify=(2/3,1/5))

#HiHat
b7 >> play("-.-.-.-.")

b7 >> play("-.-.-.-.").every(16,"mirror").every(8,"stutter",2)

#Syncopation example (Change rythmical dynamic via volume)
b7 >> play("--------",sample=3,amplify=[1/3,1/3,2/3,1/3,1/3,2/3,1/3,2/3])

#Swing example (Add little variations in timing of 8th and 16th)
b7 >> play("--------",sample=3,delay=PWhite(-1/7,1/7),amplify=2/3)





#EXAMPLE: HOUSE

#Set beats per minutes
Clock.bpm=120

#Bass
b1 >> play("X.",rate=4/5,sample=2,amplify=2/3)

#Clap
b4 >> play("..*.",sample=3,amplify=2/5)
#Shaker
b5 >> play("s",rate=1,sample=2,delay=PRand([0,PWhite(-1/20,1/20)]),amplify=1/2)

#Closed HiHat
b7 >> play("-",rate=4/5,sample=3,delay=PRand([0,PWhite(-1/20,1/20)]),amplify=2/3)
#Crash Cymbal
b8 >> play("#",rate=5/4,sample=4,dur=8,sus=8,amplify=4/5)



#EXAMPLE: DRUM N BASS

#Set beats per minutes
Clock.bpm=170

#Bass
b1 >> play("V....V..VV...V..",rate=4/5,sample=2,amplify=2/3)
b2 >> play("v......[vvvv]",sample=4,amplify=2/3)

#Snare
b4 >> play(Pvar(["..o.","..o[.o.]"],[12,2]),sample=2,amplify=2/5)
b5 >> play("..i.",amplify=PRand([3/5,PWhite(2/3,3/5)]))

#Closed HiHat
b7 >> play("s",rate=4/5,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)]))
b8 >> play("-",rate=7/5,pshift=linvar([0,16],8),sample=2,shape=1/3,amplify=7/5)



#EXAMPLE: DUBSTEP

#Set beats per minutes between 140-150
Clock.bpm=140

#Bass
b1 >> play(Pvar(["V...V...","V[..V.]..[V..V][..V.].[..V.]"],16),dur=1,rate=6/5,sample=6,amplify=2/3)

b2 >> play(Pvar(["X...X...","X[..X.]..[X..X][..X.].[..X.]"],16),dur=1,sample=2,amplify=2/3)

b3 >> play(Pvar(["v...v...","v[..v.]..[v..v][..v.].[..v.]"],16),dur=1,sample=4,amplify=2/3)

#Snare
b4 >> play(Pvar(["..o...o.","..o...oo"],16),dur=1,rate=3/4,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)]))

b5 >> play(Pvar(["..i...i.","..i...ii"],16),dur=1,sample=4,amplify=2/5)

b6 >> play(Pvar(["..h...h.","..h...hh"],16),dur=1,sample=5,amplify=2/5)

#Closed HiHat
b7 >> play("-",dur=1/2,rate=3/5,pshift=linvar([0,8],8),sample=4,amplify=4/5)

b8 >> play("s",dur=1/2,rate=1,sample=1,amplify=PRand([3/5,PWhite(2/3,3/5)]))



#BuildUp
c1 >> play("V.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),rate=6/5,sample=6,amplify=Pvar([2/3,0],[30,2]))

c2 >> play("X.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),sample=2,amplify=Pvar([2/3,0],[30,2]))

c3 >> play("v.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),sample=4,amplify=Pvar([2/3,0],[30,2]))

c4 >> play("o.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),rate=3/4,sample=2,amplify=Pvar([2/5,0],[30,2]))

c5 >> play("i.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),sample=4,amplify=Pvar([2/5,0],[30,2]))

c6 >> play("h.",dur=Pvar([1,1/2,1/4,1/16],[16,8,4,4]),sample=5,amplify=Pvar([2/5,0],[30,2]))


gB = Group(b1,b2,b3,b4,b5,b6,b7,b8)

gB.amp=Pvar([1,0],[64,32])

gC = Group(c1,c2,c3,c4,c5,c6)

gC.amp=Pvar([0,1],[64,32])

#Add delays, pshift, rate, or reverse to create different patterns





#EXAMPLE: TRAP


#Set beats per minutes between 140-150
Clock.bpm=140

#Bass
b1 >> play("[VV]..V[.V]V.[.V].V..V.V.V.",dur=1,rate=6/5,sample=-1,amplify=2/3)
b2 >> play("[XX]..X[.X]X.[.X].X..X.X.X.",dur=1,sample=2,amplify=2/3)

#Snare
b4 >> play("..o.",dur=1,rate=3/4,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)]))
b5 >> play(".H..",dur=1,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)

#Closed HiHat
b7 >> play("[--]",dur=PRand([4,2,1,1/2,PDur(3,8)*2,PDur(3,7)*2],1/4),rate=3/4,sample=3,amplify=3/5)
b8 >> play("[--]",dur=PRand([4,2,1,1/2,1/4]),rate=2/4,sample=-1,amplify=3/5)


#Some fun with synths
s1 >> dub(PRand([0,3,5],1/4),oct=(3,4),dur=4,chop=PRand([6,8]),shape=2/3,amplify=1/3)

s2 >> space(s1.degree,oct=(4,5),dur=4,chop=PRand([3,4]),room=3/5,mix=1/2,amplify=6/5).offbeat()

s3 >> pulse([0,2,3,5,7,9],oct=var([3,4,5]),dur=PRand([1/2,1/4],6),shape=2/3,formant=var([3,0,2],1/2),room=3/4,mix=1/2,pan=[-2/3,2/3],amplify=3/5)





#EXAMPLE: HIP HOP

#Set beats per minutes between 70-93 bpm
#Default bass 1 and 3, snare 2 and 4
Clock.bpm=80

#Bass
b1 >> play("X..X....X.XX....",rate=var([4/5,1],8),formant=2,sample=5,amplify=5/3,amp=1)

b1 >> play("V..V....V.VV....",rate=var([4/5,1],8),sample=-1,amplify=2/3)

b2 >> play("V..V....V.VV....",dur=1/2,sample=var([4,0],4),amplify=2/3)

#Snare
b4 >> play("..i.",rate=3/4,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)]))

b5 >> play(".H.......H......",dur=1/2,rate=7/5,sample=1,delay=1/16,pan=(-5/7,5/7),amplify=2/5)

#Some random
b6 >> play(".k",dur=PRand([4,2,1,1/2,PDur(3,8)*2,PDur(3,7)*2],1/4),rate=2/4,sample=PRand([0,1,2,3,4,5]),amplify=3/5)

#Closed HiHat
b7 >> play("--.-",rate=3/4,sample=3,amplify=3/5)

#Open Hat N or =
b8 >> play(".............#..",rate=7/5,sample=2,amplify=1,amp=1)

b9 >> play("[ss]",rate=3/4,sample=2,hpf=linvar([800,6000],1),amplify=3/5,amp=1).every(PRand([4,8,12,16]),"stutter",PRand([2,3,4]))





#EXAMPLE: FOOTWORK

Clock.bpm=154


#Bass
b1 >> play("X..X..X.X..X..X.",dur=1,rate=6/5,sample=-1,amplify=2/3)

b2 >> play("V..V..V.V..V..V.",dur=1,sample=1,amplify=2/3)

b3 >> play("{([XX])([X.])([X...])}",dur=1,rate=PRand([3/4,3/5,1,7/5],1/4),shape=linvar([1/7,3/5],16),amplify=2/5,amp=1).every(PRand([2,4,8,16]),"stutter",PRand([2,3,5]))

#Snare
b4 >> play("............H...",dur=1,rate=3/4,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)])).every(PRand([4,8,12]),"stutter",PRand([2,3]))

b5 >> play("......o.......o[oo.o]",dur=1,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)

b6 >> play("",dur=PRand([4,2,1,1/2,PDur(3,8)*2,PDur(3,7)*2],1/4),rate=2/4,sample=-1,amplify=3/5)

#HiHat
b7 >> play("..-.....",rate=3/4,sample=3,amplify=3/5)

b8 >> play("-",dur=1,sample=3,amplify=4/5)

b9 >> play("{([--])(M)}",dur=1,rate=PRand([3/4,3/5,1,7/5],1/4),sample=2,shape=linvar([1/7,3/5],16),amplify=1/5,amp=1).every(PRand([2,4,8,16]),"stutter",PRand([3,5]))

#synth fun

s1 >> space(Pvar([[9,2],[9,3],[9,4],[5,3]],[4,4,4,4]),oct=(4,5),dur=var([3,1]),sus=var([3,1]),formant=4,room=3/4,mix=1/2,amplify=3/5,amp=1)

s2 >> pulse(s1.degree+PRand(Scale.default),dur=1/2,formant=3)





#EXAMPLE: FUNK


Clock.bpm=118



#1

#Bass
b1 >> play("X.[XX]X[.X]XX[.X]",dur=1,rate=6/5,sample=2,amplify=2/3)

b2 >> play("X.[XX]X[.X]XX[.X]",dur=1,sample=2,amplify=2/3)

b3 >> 

#Snare
b4 >> play("..[.H].[.H]..[.H]",rate=3/4,sample=2,amplify=PRand([2/5,PWhite(2/3,1/5)])).stop()

b5 >> play("....p..p.p....p..p..p..p.p.pp...",dur=1/4,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)

#HiHat
b7 >> play("-.-.-.---...-.-.--..-.-.-.-.-...",dur=1/4,rate=3/4,sample=3,amplify=3/5)




#2

#Bass
b1 >> play("..VV....VV..V...",dur=1/4,rate=6/5,sample=-1,amplify=2/3)

b2 >> play("",dur=1,sample=2,amplify=2/3)

#Snare
b4 >> play("H.....H.........",dur=1/4,rate=3/4,sample=2,amplify=PRand([3/5,PWhite(2/3,3/5)]))

b5 >> play("p.....p.........",dur=1/4,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)

#HiHat
b7 >> play("-.-.-.---...-.-.--..-.-.-.-.-...",dur=1/4,rate=1,sample=-1,amplify=3/5)

b8 >> play("-.-.-.-.",dur=1/4,rate=1,sample=3,amplify=4/5)




#3

#Bass
b1 >> play("VV...[VV]..",dur=1/2,rate=6/5,sample=-1,amplify=1/3)

b2 >> play("VV...[VV]..",dur=1/2,sample=linvar([0,5],4),amplify=1/3)

#Snare
b4 >> play("..[o.][.o][.o].[o.][.o]",dur=1/2,rate=2,sample=5,amplify=PRand([2/5,PWhite(1/3,2/5)]))

b5 >> play("....i..i.i..i..i",dur=1/4,rate=1,sample=3,pan=(-5/7,5/7),amplify=3/5)

b6 >> play("..o.",dur=1,rate=2,sample=5,pan=(-5/7,5/7),amplify=3/5)

#HiHat
b7 >> play("[-.-.][-.--][-...][-.-.][--..][-.-.][-.-.][-...]",dur=1,rate=1,sample=2,amplify=1)

b8 >> play("[-.]",dur=1/2,rate=1,sample=-1,amplify=4/5,amp=1) 

b9 >> play("[ll].-.",sample=var([3,4,0],16),formant=linvar(5,8),amplify=4/5,amp=1).every(PRand([2,4,8,16]),"stutter",PRand([2,3,5]))

b0 >> play("[ss]",rate=1,sample=2,shape=2/3,amplify=4/5,amp=1).every(PRand([2,4,8,16]),"stutter",PRand([2,3,5]))
