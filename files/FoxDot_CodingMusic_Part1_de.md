# FoxDot_CodingMusic_Part1


  <img src="images/FoxDot_CodingMusic_FeatureImg.png" width="960"/>

## Description

FoxDot ist eine einfache, auf Python basierende interaktive Programmierumgebung. Sie kommuniziert mit SuperCollider , einer leistungsstarken Audiosynthese, um Musik zu machen. FoxDot organisiert musikalische Ereignisse mit einer benutzerfreundlichen und einfach zu verstehenden Weise. Dies bring Spaß am Musizieren mit LiveCode, sowohl für Programmierneulinge, als auch für Veteranen gleichermaßen.

Der Workshop führt dich durch die Grundlagen von FoxDot. Du kannst anschliessend sogleich beginnen, allein oder mit anderen Menschen zu musizieren. Die FoxDot Umgebung kann auch in traditioneller Weise zum Schreiben musikalischer Kompositionen verwendet werden.

3 virtuelle Proberäume stehen unter [pitchglitch](https://pitchglitch.cc/) zur Verfügung.

---

## Index <a name="index"></a>

[Vorbereitung](#preparation)

1. [Eine Einführung](#introduction)

   1.1. [Was ist Live-Codierung?](#livecoding)

   1.2. [Warum Code für Musik verwenden?](#whycode)

   1.3. [Was ist FoxDot?](#whatfoxdot)

2. [Erste Schritte mit FoxDot](#firststeps)

   2.1. [Spielerobjekte (Players)](#players)

   2.2. [Muster (Patterns)](#patterns)

   2.3. [Zeitvariablen (TimeVars)](#timevars)

   2.4. [Spieler "play" und "loop"](#play&loop)

   2.5. [Gruppen](#groups)

3. [Zunächst ein wenig Musiktheorie](#musictheory)

   3.1. [Songstruktur](#songstructure)

   3.2. [Akkorde und Noten](#chords&notes)

   3.3. [Skalen und Modi](#scales)

   3.4. [BeatBox](#beatbox)

4. [Live Jam](#livejam)

   4.1. [Wifi Verbindung](#wifi)

   4.2. [Starte Troop aus](#runtroop)


---

## Vorbereitung <a name="preparation">

Für den Kurs benoetigen wir verschiedene Software installiert. Diese Anleitung soll helfen, die Software auf verschiedenen Platformen selbst zu installieren.

### SuperCollider

SuperCollider ist die Basis, die wir benötigen, um die Plattform „FoxDot“ überhaupt benutzen zu können. Es ist die Computersprache, mit der die Instrumente programmiert worden, welche wir im Kurs spielen werden. Es installiert auch den Audioserver, der den eigentlichen Sound erzeugt.


| Betriebssystem | Anweisungen |
|----------------|-----------------------|
| Windows  |  Installiere [SuperCollider 3.10.4 32bit](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4_Release-x86-VS-95e9507.exe) oder [SuperCollider 3.10.4 64bit](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4_Release-x64-VS-95e9507.exe)|
| Mac   |  Installiere [SuperCollider 3.10.4](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4-macOS-signed.zip) |
| Linux   | 1. Führe im Terminal eine Update des Systems durch:<br>***sudo apt update***<br>2. Installiere Linux Paketmanager<br>***sudo apt-get install synaptic***<br>3. Starte das Program mit:<br>***sudo synaptic***<br>4. Suche, aktiviere und installiere **supercollider** |



### sc-plugins

Um noch mehr aus der Software und auch später aus FoxDot zu bekommen, müssen wir auch alle notwendigen Plug-ins installieren. Diese kommen in Form eines Komplettpakets, welches wir in ein bestimmtes Verzeichnis kopieren werden.


| Betriebssystem | Anweisungen |
|----------------|-----------------------|
| Windows / Mac   | 1. Download [sc-plugins](https://github.com/supercollider/sc3-plugins/releases/tag/Version-3.10.0-rc1) und entpacke das Archiv im Downloadverzeichnis <br>2. Starte das SuperCollider Programm und gebe folgende Zeile ein, um den Pfad zum Erweiterungsordner anzugeben:<br>***Platform.userExtensionDir***<br>3. Um Code in SuperCollder auszuloesen, setze den Cursor auf die Codezeile und drücke Cmd + Eingabe (macOS) oder Ctrl + Eingabe (Win).<br>4. Verschiebe das gesamte sc-plugin Verzeichnis in diesen Erweiterungsordner |
| Linux   |  1. Starte den Programmmanager im Terminal mit:<br>***sudo synaptic***<br>2. Suche, aktiviere und installiere **sc-plugins**  |


### Python 3

FoxDot ist in Python geschrieben, eine leicht zu lernende Programmiersprache. Um FoxDot installieren und starten zu können, muss Python3 installiert sein. Es ist möglich, dass es bereits als Standard zum Betriebssystem gehört (z. B. einige Linuxversionen).


| Betriebssystem | Anweisungen |
|----------------|-----------------------|
| Windows | Download und installiere [Python 3](https://www.python.org/downloads/windows/) |
| Mac | Download und installiere [Python 3](https://www.python.org/downloads/mac-osx/) |
| Linux   |  1. Starte den Programmmanager im Terminal mit:<br>***sudo synaptic***<br>2. Suche, aktiviere und installiere **python3**  |


### pip

pip ist ein Library-Installierer fuer Python und macht es einfacher Erweiterungen und Programme fuer/von Python zu installieren

| Betriebssystem | Anweisungen |
|----------------|-----------------------|
| Windows   | 1. Download [get-pip.py](https://bootstrap.pypa.io/get-pip.py) und speichere die Datei dort, wo Python3 installiert ist.<br><img src="images/pip_install_1_win.jpg" width="512"/><br>2. Öffne einen Terminal und führe folgende Befehle aus:<br>***cd Pfad_zum_installierten_Python3***<br>***python3 get-pip.py***<br>3. Nach der Installation, kannst du die installierte Version mit folgendem Befehl ueberpruefen:<br>***pip3 --version***|
| Mac   | 1. Download [get-pip.py](https://bootstrap.pypa.io/get-pip.py) und speichere die Datei dort, wo Python3 installiert ist.<br>2. Öffne einen Terminal und führe folgende Befehle aus:<br>***cd Pfad_zum_installierten_Python3***<br>***python3 get-pip.py***<br>3. Nach der Installation, kannst du die installierte Version mit folgendem Befehl ueberpruefen:<br>***pip3 --version*** |
| Linux   |  1. Starte den Programmmanager im Terminal mit:<br>***sudo synaptic***<br>2. Suche, aktiviere und installiere **python3-pip**   |

### FoxDot

Da wir nun pip installiert haben, sind die folgenden Schritte für jedes Betriebssystem gültig.

1. Öffne ein Terminal auf deinem Computer und fuehre folgende Befehlszeilen aus:

  > pip install python3-tk

  > pip install FoxDot

2. Starte SuperCollider und installiere FoxDot notwendige Module in SuperCollider mit folgender Befehlszeile:

  > Quarks.install("FoxDot")

  > Quarks.install("SafetyNet")

**Wie vorher, gehe mit deinem Cursor ueber die jeweilige Zeile und press Ctrl + Eingabe (Cmd + Eingabe), um den Befehl auszuloesen**

*Hint: Es gibt eine grafische Fensterversion, um Quarkelemente zu installieren. Nutze dafuer die Befehlszeile:*

> Quarks.gui


### git

Git ist eine freie Software zur verteilten Versionsverwaltung von Dateien, die durch Linus Torvalds initiiert wurde. Wir werden dies nutzen, um Troop herunterzuladen. Dies kann auch direkt für spezielle Versionen für FoxDot genutzt werden.


| Betriebssystem | Anweisungen |
|----------------|-----------------------|
| Windows | Download und installiere [Git](https://git-scm.com/download/win) |
| Mac | Download und installiere [Python 3](https://sourceforge.net/projects/git-osx-installer/files/) |
| Linux   |  1. Starte den Programmmanager im Terminal mit:<br>***sudo synaptic***<br>2. Suche, aktiviere und installiere **git**  |


### Troop

Da wir nun git installiert haben, sind die folgenden Schritte für jedes Betriebssystem gültig.

1. Öffne ein Terminal, und klone Troop auf deinen Computer mit folgendem Befehl:

> git clone https://github.com/Qirky/Troop

2. Zum Ausführen von Troop, gebe folgende Befehle innerhalb des Troopverzeichnisses an.

* Fuer den Servercomputer:

  >python3 run-server.py

* Fuer den Klientencomputer:

  >python3 run-client.py


#### Hoffe das es alles geklappt hat. Los gehts!


---

#### [Zurück zum Index](#index)

## 1. Eine Einführung <a name="introduction">

### 1.1. Was ist Live Coding? <a name="livecoding"></a>

* Interaktives Programmieren als Kunstperformance wie Musik- oder Videokunst
>“Live Coding ist eine neue immer interessantere Richtung in der elektronischen Musik und in Videokunst. Live Coders enthüllen und verdrahten die Innereien einer Software, während sie improvisierte Musik erzeugt.” - toplap.org
* Verwenden von Code zur Beschreibung von Regeln für ein Kunstwerk
* Live-Notation / Komposition als Performance
* Der Code kann in Echtzeit geändert und erneut ausgeführt werden, während das Programm weiterlaeuft (Musik während der Ausführung komponieren).
* Bringt Computersprache in ein soziales Umfeld und macht so das Codieren zu einer sozialen Aktivität

<img src="images/foxdot_workshop_1.jpg" width="960"/>


### 1.2. Warum Code für Musik verwenden? <a name="whycode"></a>

* Klassische Musik mit Notation auf Blättern ist bereits ein Code zum Schreiben von Musikstücken
* Tonhöhe, Dauer, Lautstärke in Noten ist ein Code, der von Musikern gelesen werden kann
* Mit Live-Codierung kannst du:
  * flexible Beschreibungsregeln nutzen
  * den Code ohne Benutzeroberfläche hacken
  * mit deiner Komposition interagieren, während die Musik spielt
  * am Rande der Echtzeit arbeiten

<img src="images/foxdot_workshop_2.jpg" width="960"/>


### 1.3. Was ist FoxDot? <a name="whatfoxdot"></a>

* Entwickler: Ryan Kirkbride, Leeds UK
* FoxDot ist ein Python-Paket, das mit einer eigenen IDE geliefert wird und Musik abspielt, indem auf alle auf einem lokalen SuperCollider-Server gespeicherten SynthDefs mit einigen benutzerdefinierten Syntaxbits zugegriffen wird.
* Python ist eine benutzerfreundliche objektorientierte und einfach lesbare Programmiersprache.
* SuperCollider ist eine Programmiersprache, die ursprünglich 1996 von James McCartney für Echtzeit-Audiosynthese und algorithmische Kompositionen veröffentlicht wurde und unter der FoxDot-Umgebung ausgeführt wird.
* Live-Codierung mit Python über FoxDot bietet über seine reaktiven und dynamischen Objekte zugängliche Zustände
* FoxDot konzentriert sich auf Musikmuster, nicht auf die digitale Signalverarbeitung (DSP), die von SuperCollider programmiert und über OSC (OpenSoundControl) gesteuert wird.
* FoxDot hat eine saubere Syntax, die leicht zu lesen ist, sodass der Code für das Publikum und traditionelle Musiker verständlich ist.
* Das Systemdiagramm (Schema) zeigt, was FoxDot tut:

  <img src="images/foxdot_workshop_3.jpg" width="960"/><br>

  - FoxDot ist eine Python-Bibliothek, die in eigenen Python-Projekten oder über die Tkinter-IDE verwendet werden kann.
  - Es ist auch möglich, andere IDEs für die Arbeit mit FoxDot zu konfigurieren.
  - Die musizierenden Objekte können Zeitplanereignisse wie Timing, Status und Muster sein.
  - SuperCollider bietet SynthDefs (Instruments / FXs) und PBinds (Patterns / Sequences), die über OSC adressiert und verwaltet werden können.

#### Eine Ergänzung zu FoxDot ist Troop
  - Ermöglicht das kollaboratives Musizieren mit FoxDot.
  - Enthält ein in Python geschriebenes Server- und Client-Skript.
  - Es kann mehrere Computer an einen Sound-PC anschließen, auf dem der SuperCollider-Audioserver ausgeführt wird und der die gesamte digitale Signalverarbeitung für FoxDot übernimmt.
  - Auf diese Weise können viele Menschen ein Musikstück aufführen und mit dem Code interagieren, den andere in der Gruppe geschrieben haben.


  <img src="images/foxdot_workshop_4.jpg" width="960"/><br>

---

#### [Zurück zum Index](#index)

### 2. Erste Schritte mit FoxDot <a name="firststeps"></a>

Lass uns Schritt für Schritt die Grundfunktionen des Editors erfahren.

Zuerst müssen Sie FoxDot starten.

1. Execute Supercollider IDE
2. Gebe FoxDot.start ein und drücke STRG + EINGABETASTE
>Der Audioserver sollte starten und alle Synthesizer und Effekte wurden in den Speicher geladen
>Jetzt hört Supercollider auf die Nachrichten von FoxDot
3. Öffne ein Terminal und wechsele in deinen Projektordner
4. Führe nun „python3 -m FoxDot“ oder „python -m FoxDot“ aus.
>Der GUI-Editor von FoxDot sollte sich öffnen

#### Übung:
*Gehe durch das Menü und überprüfe alle Optionen. Dabei finde und aktiviere:*

> Toogle Beat Counter

*und*

> Use SC3-Plugins

Der Beatcounter zeigt unten links im Programm den Takt über 4 Takteinheiten an. Es ist wie ein visuelles Metronom.
Die zweite Option aktiviert alle Plug-ins. Dadurch wird z. B. das Piano funktionieren.

Der Editor ist interaktiv. Wenn Du also den Cursor über eine Linie oder einen lückenlosen Block positionierst oder einen Block auswählst, kannst Du den auszuführenden Code in Echtzeit durch das Drücken von Ctrl + Eingabe ausführen.

#### Übung:
*Probieren wir ein wenig Python-Code aus, um zu lernen, wie man ihn benutzt!*

Gib die folgende Zeile in den Textteil des Editors und presse Ctrl/Cmd/Strg + Eingabe, während der Cursor auf dieser Zeile positioniert ist.

    2 + 2

Die Ausgabe eines ausgeführten Codes wird in der Konsole im unteren Fenster des Programms angezeigt.
Die Konsole zeigt die eingegebene Zeile an. Benutze die Python-Funktion print(), um das Ergebnis anzuzeigen:

    print(2 + 2)

Jetzt werden wir die Gleichung in eine Variable packen. Variablen werden wir oft benutzen. Schreibe die 2 Zeilen direkt untereinander, damit es als Block komplett ausgeführt werden kann:

    a = 2 + 2
    print(a)

Variablen können auch kombiniert werden:

    a = 2
    b = 3
    c = a + b
    print(c)

Wenn du lediglich eine Zeile innerhalb des Blocks ausführen möchtest, gehe mit dem Cursor über die Zeile und presse Alt + Eingabe.

Die allgemeine Philosophie von FoxDot besteht darin, so einfach wie möglich "Player"-Objekte zu erstellen, die Schlüsselwortargumente verwenden, die die SuperCollider Pbind-SynthDef-Beziehung widerspiegeln und ihre Aktionen auf einer global zugänglichen Uhr planen. Einfacher gesagt: Du erzeugst Instrumente (Player) und deren Spielweise (Attribute) und spielst deren Noten und Akkorde im angegebenen Tempo unter Zuhilfenahme einer Systemuhr ab.

Wenn Du mehr über eine Funktion oder eine Klasse erfahren möchtest, gebe einfach "help" gefolgt vom Namen des Pythonobjektes in Klammern ein:

    help(object)

Ein SynthDef ist im Wesentlichen das digitale Instrument, das FoxDot als Spieler (Player) unter deiner Anleitung verwendet.


#### [Zurück zum Index](#index)

### 2.1. Spieler Objekte (Playerobjekte)<a name="players"></a>

Foxdot hat eine Reihe von verschiedenen Instrumenten zur Verfügung, welche du als Spielerobjekte nutzen kannst.

Führe einfach den folgenden Befehl aus, um einen Blick auf die vorhandene Auswahl von FoxDot SynthDefs zu werfen:

    print(SynthDefs)

Wähle ein Instrument aus und erstelle ein FoxDot-Player-Objekt mit der Doppelpfeilsyntax wie im folgenden Beispiel. Wenn dein gewähltes SynthDef "pluck" wäre, kannst du ein Objekt "p1" erstellen:

    p1 >> pluck()

Um ein einzelnes Spielerobjekt zu stoppen, führe einfach p1.stop() aus. Um alle Player-Objekte anzuhalten, drücke STRG (Ctrl/Cmd) + . >> Dies ist eine Verknüpfung zum Befehl Clock.clear().

Die Variable "p1" kann aus 2 Buchstaben bzw. 1 Buchstabe + 1 Zahl bestehen (z. B. pp oder s1). Du wirst deine eigene Strategie in der Namengebung finden, damit du die doppelte Nutzung von Bezeichnungen verhinderst.

Das Symbol >> ist in Python normalerweise für eine Art von Operation wie + oder - . In FoxDot wird dieses Symbol als Teil des Players genutzt, das wie = gesehen werden kann.

Wenn Du deinem Instrument p1 jetzt eine Zahl gibst, wird es diese Noten spielen. Die Standardnote ist C, welche im vorherigen Beispiel p1 >> pluck() gespielt wurde. Mit einer Liste in eckigen Klammern kannst du nun eine Folge von Noten erzeugen.

    s1 >> pluck([0, 2, 4])

Um den jeweiligen Noten eine Zeit zuzuordnen, verwende var. Im folgenden Beispiel wird jede Note 4 Takteinheiten gespielt:

    s1 >>pluck(var([0, 2, 4], 4))

Takt 0 –> Note 0 >> Takt 4 –> Note 2 >> Take 8 –> Note 4 >> Takt 12 –> Note 0 >> Takt 16 –> Note 2 >> Takt 20 –> Note 4 …

Benutze eine weitere Liste, um Noten eine explizite Zeit zu geben:

    s1 >>pluck(var([0, 2, 4], [2, 2, 4]))

Verwende nun Argumente mit Bezeichnung, um die Spielweise des Instruments zu gestalten. Im folgenden Beispiel wird die Octave *oct* erhöht (Standard ist 5), die Notenspieldauer *dur* (Standard ist 1) und die Lautstärke *amp* variert (Standard ist 1).

    s1 >> pluck([0, 2, 4], oct=6, dur=[1, 1/2, 1/2], amp=[1, 3/4, 3/4])

Du kannst Noten und Variablen gruppieren, indem du mehrere Werte von Argumenten in runden Klammern einfügst. Im folgenden Beispiel spielen wir 2 Noten gleichzeitig und erweitern den Stereoeffekt im Attribut *pan*:

    s2 >> bass([(0, 9),(3, 7)], dur=4, pan=(-1, 1))

Du kannst sogar ein Spielerobjekt einem anderen Spieler folgen lassen. Im Beispiel fügt s2 eine Triade zu jeder gespielten Bassnote von s1 hinzu:

    s1 >> bass([0, 2, 3, 4], dur=4)
    s2 >> pluck(dur=1/2).follow(s2) + (0, 2, 4)

Neben *.follow()* kannst du auch das Argument *.degree* (ohne Klammern) verwenden, um anderen Spielern zu folgen:

    s3 >> pluck(s1.degree + 2)

Dies ist auch mit allen anderen Argumenten möglich. Zum Beispiel *s1.oct*, *s1.dur* und so weiter.

#### Übung:
*1. Verwende print(SynthDef), um alle verfügbaren Synthesizer anzuzeigen und probiere sie aus.*
*2. Bilde eine kleine Basslinie mit 1 - 8 Noten, Akkorde mit 1 - 8 Akkorden und eine kleine Melodie.*
*3. Verwende einige der Attribute: die Oktavenvariable oct =, die Dauervariable dur = und / oder der Amplitudenverstärkungswert amplify =, um ein besseres Ergebnis zu erzielen!*

**Hinweis: Die Standardoktave in FoxDot ist 5, was in der konventionellen Musiktheorie Oktave 3 ist!**

In meinem Beispiel gehört ich 3 Spieler, um ein volles Klavier zu verbessern:

    p1 >> piano([0, 1, 0, -1], oct=4, dur=2, amplify=3/4)
    p2 >> piano([(2, 4), (0, 2), (3, 5), (1, 3), (2,4), (0,2), (-1,1), (-3,-1)], dur=1, amplify=2/3)
    p3 >> piano([0, 4, 2, 4, 1, 2, 1, 3, 2, 3, 5, 7, -1, 3, -3, 1], oct=6, dur=1/2).every(32, "reverse")

Dies ist das gleiche wie:

    bassline = [0, 1, 0, -1]
    chords = [(2, 4), (0,2), (3, 5), (1, 3), (2, 4), (0, 2), (-1, 1), (-3, -1)]
    melody = [0, 4, 2, 4, 1, 2, 1, 3, 2, 3, 5, 7, -1, 3, -3, 1]
    p1 >> piano(bassline, oct=4, dur=2, amplify=3/4)
    p2 >> piano(chords, dur=1, amplify=2/3)
    p3 >> piano(melody, oct=6, dur=1/2).every(32, "reverse")

Mit print(Player.get_attributes()) erhältst du eine Liste der zur Verfügung stehenden Attribute. Die meisten dieser Attribute haben einen Standardwert von 0 oder 1.

**Wenn du beginnst diese Werte auszuprobieren, benutze die Lautsprecher anstelle von Kopfhörern, um dich vor Gehörschaden zu schützen**

Soundbeispiele können auch mit dem Sample Playerobjekt wiedergegeben werden, das wie folgt erstellt wird:

    b1 >> play("x-o-")

Jedes Zeichen bezieht sich auf eine andere Audiodatei. Um Samples gleichzeitig abzuspielen, erstelle einfach ein neues Playerobjekt:

    b1 >> play("xxox")
    b2 >> play("---(-=)", pan=0.5)

Die Buchstaben/Zeichen von b2 in runden Klammern verkürzt die Schreibweise, welche anderenfalls “-------=” wäre. Das spart eine Menge Platz ein und führt zu komplexeren Spielweisen.

Wenn du Zeichen in eckige Klammern setzt, werden diese doppelt so schnell abgespielt. Das bedeutet das 2 Zeichen in eckigen Klammern die gleiche Zeit eines einzelnen Zeichens repräsentiert:

    b3 >> play("x[--]o(=[-o])")

Weiterhin kannst du Methoden nutzen, um die Reihenfolge der gespielten Audiodateien oder Noten zu variieren.

    b4 >> play(“----”, sample=-1).every(4, ”stutter”,3)

#### Übung:
*Gehe die Zeichen durch und höre dir die verschiedenen verfügbaren Beispiele an. Verwende dabei das Attribut sample = [:8]. Die Audiodateien oder Samples werden wiederholt, wenn der Charakter weniger als 9 Samples (0 - 8 sind 9 Zahlen) im dedizierten Ordner enthält!*


| Bezeichnung | Buchstabe/Zeichen |
|-------------|-------------------|
| Kick        | A v V x X W       |
| Snare/Rim   | D i I o O t u     |
| Hihat       | : = - a n N       |
| Clap/Snap   | * h H             |
| Cymbal/Crash| / # e E           |
| Tom/Tom-like| m M p P w         |
| Percussion  | & + d f l r R y   |
| SoundFX     | \ b F k L Q Y z Z |
| Voice       | 1 2 3 4 ! < ? c C |
| Bell        | T                 |
| Various     | $ ; B g G j J K q U |
| Noise       | @ %               |
| Shaker      | s S               |
| Ride        | ~                 |


#### [Zurück zum Index](#index)

### 2.2. Muster (Patterns)<a name="patterns"></a>

Playerobjekte verwendet Pythonlisten (in anderen Sprachen als Arrays bezeichnet), um sich selbst zu sequenzieren. Du hast diese bereits in vorhergehenden Beispielen verwendet. In diesem Abschnitt werden wir uns tiefer mit Listen und Mustern beschäftigen. Das Grundlagenwissen wird dir die Tür zu einer hohen Flexibilität öffnen und interessante Gestaltungsformen bieten.

Versuche beispielsweise, eine Liste wie folgt mit zwei zu multiplizieren:

    print([1,  2,  3] * 2)

Konsolenausgabe >> [1, 2, 3, 1, 2, 3]

Entspricht das Ergebnis deiner Erwartung?

Wenn du die internen Werte in Python bearbeiten möchtest, musst du eine for-Schleife verwenden:

    l = []
    for i in [1,  2,  3]:
          l.append(i * 2)
    print(l)

oder im Listenverständnis:

    print([i*2 for i in [1,2,3]])

Konsolenausgabe >> [2, 4, 6]

Was aber, wenn du die Werte in einer Liste abwechselnd mit 2 und 3 multiplizieren möchtest?

FoxDot verwendet einen Containertyp namens "Pattern", um dieses Problem zu lösen. Sie verhalten sich wie reguläre Listen, aber jede mathematische Operation, die daran ausgeführt wird, wird für jedes Element in der Liste ausgeführt und bei Verwendung eines zweiten Musters paarweise.

Das Basismuster kann folgendermaßen erstellt werden:

    print(P[1, 2, 3] * 2)

Konsolenausgabe >> P[2, 4, 6]

    print(P[1, 2, 3] + [3, 4])

Konsolenausgabe >> P[4, 6, 6, 5, 5, 7]

Beachte, wie in der zweiten Operation die Ausgabe aus allen Kombinationen der beiden Muster besteht >> [1 + 3, 2 + 4, 3 + 3, 1 + 4, 2 + 3, 3 + 4].

#### Übung:
*Probiere einige andere mathematische Operatoren aus und sehe, welche Ergebnisse du erhältst!*


Was passiert, wenn du Zahlen in Klammern wie P[1,2,3] * (1,2) gruppierst?

    P[P(1, 2), P(2, 4), P(3, 6)]

Es gibt mehrere andere Pattern-Klassen in FoxDot, mit denen du Arrays von Zahlen generieren kannst, welche sich jedoch genauso verhalten wie das Basismuster.

    print(classes(Patterns.Sequences))
    print(classes(Patterns))

In Python kannst du mit dem Syntaxbereich (Start, Stopp, Schritt) einen Bereich von Ganzzahlen generieren. Standardmäßig ist Start 0 und Schritt 1.

Mit *PRange(Start, Stopp, Schritt)* kannst du ein Musterobjekt mit den entsprechenden Werten erstellen. Das erste Beispiel zeigt die äquivalente Funktion in Python, die zweite ist die vereinfachte Musterfunktion in FoxDot *PRange*:

    print(list(range(10)))

Konsolenausgabe >> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


    print(PRange(10))

Konsolenausgabe >> P[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


    print(PRange(10) * [1, 2])

Konsolenausgabe >> P[0, 2, 2, 6, 4, 10, 6, 14, 8, 18]

Aber was ist mit dem Kombinieren von Mustern? In Python kannst du zwei Listen mit dem Operator + verketten (aneinander anhängen). FoxDot-Muster verwenden dies jedoch, um die Daten in der Liste zu ergänzen. Um zwei Pattern-Objekte miteinander zu verbinden, kannst du das Pipe-Symbol verwenden, mit dem Linuxbenutzer möglicherweise vertraut sind. Es wird zum Verbinden von Befehlszeilenprogrammen verwendet, indem die Ausgabe eines Prozesses als Eingabe an einen anderen gesendet wird.

    print(PRange(4) | [1,7,6])

Konsolenausgabe >> P[0, 1, 2, 3, 1, 7, 6]

In FoxDot gibt es verschiedene Arten von Mustersequenzen (und die Liste wächst noch), die das Generieren dieser Zahlen etwas erleichtern. Um beispielsweise die erste Oktave einer pentatonischen Tonleiter von unten nach oben und wieder zurückzuspielen, kannst du zwei PRange-Objekte verwenden:

    p1 >> pluck(PRange(5) | PRange(5,0,-1), scale=Scale.default.pentatonic)

Die PTri-Klasse erledigt dies für dich:

    p1 >> pluck(PTri(5), scale=Scale.default.pentatonic)


#### Übung:
*Probiere die unteren Beispiele aus, um einen Einblick in die möglichen Musterfunktionen zu erhalten!*


#### Musterfunktionen

Es gibt mehrere Funktionen, die ein Wertemuster für uns generieren, um nützliche Dinge in FoxDot zu erreichen, wie z. B. Rhythmen und Melodien. Diese Abschnit ist eine Liste von Pattern-Funktionen mit Beschreibungen und Beispielen.

Als Eingabeargumente für FoxDot-Player verwendet, können diese selbst als Muster behandelt und ihre Methoden direkt angewendet werden, z. B. *PDur(3, 8).reverse()*. Du kannst auch jedes Eingabeargument durch ein Muster oder eine TimeVar-Funktion ersetzen, um ein erweitertes Muster oder ein Pvar-Muster zu erstellen. Schauen wir uns einige Beispiele an:

**PStep(n, value, default=0)** >> Gibt ein Muster zurück, bei dem jeder n-Term 'Wert' ist, ansonsten 'Standard'.

    s1 >> varsaw(PStep(3, [0, 2, 1, 4, 2, 5], [-2, [-2, -1]]), oct=(4, 6), dur=1/4, sus=1/8, lpf=linvar([200, 4000], 8), amplify=3/4)

**PSum(n, total, **kwargs)** >> Gibt ein Muster der Länge 'n' zurück, dessen Summe 'total' ergibt. Zum Beispiel: PSum(3,8) -> P[3, 3, 2] PSum(5,4) -> P[1, 0.75, 0.75, 0.75, 0.75].

    s1 >> donk(P[:2], oct=[[5, 6], 6], dur=PSum(12, 8), sus=1/2, amplify=2/4)

**PRange(start,stop=None,step=None)** >> Gibt ein Pattern zurück, das zu Pattern(range(start, stop, step)) äquivalent ist.

    s1 >> piano([0, 2, 0, 1], oct=4, dur=2, sus=1, amplify=3/5)
    s2 >> piano(Pvar([[0, 2, 4, 2], [0, 4, 2, 1], PRange(0, 8, var([2, 1], 4))], [4, 4, 8]), dur=Pvar([1/2, PDur([3, 5], 8)], [1, 3]), amplify=5/7)

**PTri(start, stop=None, step=None)** >> Gibt ein zu Pattern(range(start, stop, step)) äquivalentes Muster mit angehängter umgekehrter Form zurück.

    s1 >> piano([0, 2, 0, 1], oct=4, dur=2, sus=1, amplify=3/5)
    s2 >> piano(Pvar([[0, 2, 4, 2], [0, 4, 2, 1], PTri(0, 8, var([2, 1], 4))], [4, 4, 8]), dur=Pvar([1/2, PDur([3, 5], 8)], [1, 3]), amplify=5/7)

**PEuclid(n, k)** >> Gibt den euklidischen Rhythmus zurück, der 'n' Impulse so gleichmäßig wie möglich über 'k' Schritte verteilt. z.B. PEuclid(3, 8) gibt P[1, 0, 0, 1, 0, 0, 1, 0] zurück.

    s1 >> blip(Pvar([P[:2], P[:3]], 16), oct=4, dur=1/2, amplify=3/7*PEuclid([3, 5, 5, 3], [7, 8]))


**PSine(n=16)** >> Gibt Werte eines Zyklus einer Sinuswelle zurück, die in 'n' Teile aufgeteilt ist.

    s1 >> fuzz(PSine(8), dur=1/2, sus=1/4, formant=1, room=1/2, mix=1/3, pan=PSine(32), amplify=2/3)

**PDur(n, k, dur=0.25)** >> Gibt die tatsächliche Dauer basierend auf euklidischen Rhythmen (siehe PEuclid) zurück, wobei dur die Länge jedes Schrittes ist. z.B. PDur(3, 8) gibt P[0.75, 0.75, 0.5] zurück.

    s1 >> bass(PWalk(3), oct=5, dur=Pvar([PDur(5, 7), PDur(5, 8)], 16), amplify=2/5)
    s2 >> pulse(Pvar([P[:3], P[:2]], 8), oct=5, dur=PDur(2, 3), sus=1/8, lpf=expvar([400, 4000], 16), lpr=3/4, amplify=4/7, amp=P10(16))

**PBern(size=16, ratio=0.5)** >> Gibt ein Muster aus Einsen und Nullen basierend auf dem Verhältniswert (zwischen 0 und 1) zurück. Dies wird als Bernoulli-Folge bezeichnet.

    b1 >> play("S", sample=[1, 3], amplify=2/3, amp=PBern(16, 1/2))
    b2 >> play("S", dur=PBern(24, 1/2), delay=[0, 1/2], sample=5, amplify=2/3, amp=1)

**PBeat(string, start=0, dur=0.5)** >> Gibt ein Muster von Dauern basierend auf einer Eingabezeichenfolge zurück, wobei Nicht-Leerzeichen einen Puls bezeichnen.

    s1 >> donk(dur=PBeat(". . . ..", start=0, dur=[1] + [1/2] + [1] + [1/2] * 2), amplify=2/3)
    s2 >> bell(dur=PBeat(".   . .   ..", start=0, dur=1/2), amplify=1/3)

**PSq(a=1, b=2, c=3)**

    s1 >> piano(PSq(1, 2, 3) - var([0, P[:2] * 2], [4, 8]), amplify=2/3)
    print(PSq(1, 2, 3))


#### Mustergeneratoren

Wir wissen, dass Muster eine feste Länge haben und basierend auf einer Funktion generiert werden können. Manchmal ist es jedoch nützlich, Muster von unendlicher Länge zu haben, beispielsweise beim Generieren von Zufallszahlen. Hier kommen Mustergeneratoren ins Spiel. Ähnlich wie bei Python-Generatoren, bei denen nicht alle Werte auf einmal im Speicher gehalten werden, außer wenn Python-Generatoren normalerweise ein Ende haben – FoxDot-Mustergeneratoren haben dies nicht!

**PRand(lo,hi,seed=None)/PRand([values])** >> Gibt eine Reihe von zufälligen ganzen Zahlen zwischen lo und hi (einschließlich) zurück. Wenn hi weggelassen wird, liegt der Bereich zwischen 0 und lo. Anstelle des Bereichs kann eine Liste von Werten bereitgestellt werden, und PRand gibt eine Reihe von Werten zurück, die zufällig aus der Liste ausgewählt werden.

    var.ch1 = var([PRand([0, 2, 4, 8], seed=PxRand(200))], 4)
    var.ch2 = var([PRand([0, 1, 3, 5], seed=PxRand(200))], [8, 4, 4])
    s1 >> piano([var.ch1, var.ch2], dur=1/2, amplify=2/3)

**PxRand(lo, hi) / PxRand([values])** >> Identisch mit PRand, aber es werden keine Elemente wiederholt.

    s1 >> pluck(PWalk(4), dur=PxRand([2, 2/3, 2/3, 1/3, 1, 1, 1/2, 1/2, 3/4]), oct=6, formant=3, tremolo=3, room=2/3, mix=1/3, amplify=2/3)

**PwRand([values], [weights])** >> Verwendet eine Gewichtungsliste, um anzugeben, wie oft Elemente mit demselben Index aus der Werteliste ausgewählt werden.
Ein Gewicht von 2 bedeutet, dass es doppelt so wahrscheinlich kommissioniert wird wie ein Artikel mit einem Gewicht von 1.

    s1 >> sitar(PWalk(4), dur=PwRand([2, 2/3, 1/3, 1, 1/2, 3/4, 1/4], [2, 4, 5, 3, 7, 6, 1]), oct=PwRand([6, 6, 7, 5], [4, 3, 2, 1]), room=2/3, mix=1/2, amplify=2/3)

**P10(n)**>> Gibt ein Muster der Länge n einer zufällig generierten Reihe von Einsen und Nullen zurück.

    s1 >> pulse(Pvar([[0, 1], [0, 2]], 16), oct=4, dur=2, sus=1, amplify=3/4)
    s2 >> pulse(P[:4], dur=1/2, sus=1/4, amplify=3/4, amp=P10(16))

**PAlt(pat1, pat2, *patN)** >> Gibt ein Muster zurück, das durch Abwechseln der Werte in den angegebenen Sequenzen generiert WIRD.

0, -2, 0, 8, 2, 1, 0, 9, 4, 3, 7, 0, -2, 0, 5 ...

    mtf1 = [0, 2, 4]
    mtf2 = [-2, 1, 3]
    mtf3 = [0, 0, 2]
    s1 >> piano(PAlt(mtf1, mtf2, mtf3, [8, 9, 7, 5]), dur=1/2)

**PJoin(patterns)** >> Fügt eine Liste von Mustern zusammen.

    mtf1 = [0, 2, 6, 4]
    mtf2 = [1, 3, 7, 5]
    s1 >> arpy(Pvar([mtf1, mtf2, mtf1, PJoin([mtf1, mtf2])], 8), oct=5, dur=1/2, formant=3, room=1/2, mix=1/3)

**PPairs(seq,func=<lambda>)** >> Verknüpft eine Sequenz mit einer zweiten Sequenz, die durch Ausführen einer Funktion am Original erhalten wird. Standardmäßig ist dies Lambda n: 8 - n.

    s1 >> sitar(PPairs([0, 4, 2, 0, 6, 4], lambda n: var([n*3, n-1], [12, 4])), oct=4, dur=1/2, amplify=3/7)

**PQuicken(dur=0.5, stepsize=3, steps=6)** >> Gibt eine Gruppe von Verzögerungsbeträgen zurück, die allmählich abnehmen.

    b1 >> play("m", dur=1, delay=[PQuicken(dur=2, stepsize=2, steps=3), PQuicken(dur=2, stepsize=2, steps=6)], sus=1/8, amplify=2/5)
    b2 >> play("t", dur=4, delay=PQuicken(dur=1, stepsize=4, steps=3), sample=2, amplify=3/5)
    b3 >> play("S", dur=4, delay=2 + PQuicken(dur=1/2, stepsize=2, steps=3), amplify=2/3)

**PRhythm(durations)** >> Wandelt alle Tupel/PGroups in Verzögerungen um, die mit dem PDur-Algorithmus berechnet werden.

    b1 >> play("V", dur=PRhythm([0, 1/2, 0, 1/4, 1, 3/4]), delay=0, sample=12, amplify=2/3)

**PShuf(seq)** >> Gibt eine gemischte Version von seq zurück. In diesem Beispiel wird eine Funktion verwendet, um die Liste automatisch zu mischen.

    def updateShuffle(n=0):
        beats=32
        if n % beats == 0:
            var.mtf = var([PShuf([0, 1, 3, 4, -1])], 1)
        Clock.future(1, updateShuffle, args=(n+1,))
    updateShuffle()
    s1 >> ambi(var.mtf, oct=(5, 6), dur=1, sus=1/4, echo=[0, 1/2], echotime=2, room=2/3, mix=1/3, amplify=1/2)

**PStretch(seq, size)** >> Gibt 'seq' als Pattern zurück und wird geloopt, bis seine Länge 'size' ist, z.B. PStretch([0,1,2], 5) gibt P[0, 1, 2, 0, 1] zurück.

    var.mtf1 = var([0, 1, 2, 4, [3, 5], 0, 2, 4], 1/2)
    s1 >> karp(PStretch(var.mtf1, 12), oct=6, dur=[1/2, 2/3], shape=1/8, formant=0, rate=1/8, amplify=2/3)

**PStrum(n=4)**

    var.mtf1 = var([0, 1, 2, 0, [4, 2], 3, -2, [-1, 4]], 1/2)
    s1 >> marimba(var.mtf1, oct=var([5, 6], [1/2, 3/2]), dur=Pvar([PStrum(5), PStrum(2)], 16), shape=1/4, room=1/2, mix=1/2, amplify=1)

**PStutter(seq, n=2)** >> Erstellt ein Muster, sodass jedes Element im Array n-mal wiederholt wird (n kann ein Muster sein).

    var.mtf1 = var([0, 6, 4, 2], 2)
    s1 >> quin(PStutter([var.mtf1], 2), oct=4, dur=PStutter([1, 1/2], 4), sus=1/4, amplify=2/3)

**PZip(pat1, pat2, patN)** >> Erzeugt ein Muster, das mehrere Muster 'zippt'. PZip([0,1,2], [3,4]) erzeugt das Muster P[(0, 3), (1, 4), (2, 3), (0, 4), (1, 3 ), (2, 4)].

    s1 >> faim(PZip([0, 2], [2, -2, 4, 6]), oct=6, dur=2, atk=1/6, chop=2, lpf=1800, vib=2, amplify=1/2)

**PZip2(pat1, pat2, rule=<lambda>)** >> Wie PZip, verwendet aber nur zwei Patterns. Verbindet Werte, wenn sie die Regel erfüllen.

    s1 >> faim(PZip2([0, 2], [2, -2, 4, 6], rule = <lambda>), oct=6, dur=2, atk=1/6, chop=2, lpf=1800, vib=2, amplify=1/2)

**Pvar** >> TimeVar, die Listen anstelle einzelner Werte speichert (var,sinvar,linvar,expvar).

    s1 >> gong(P[Pvar([[0, 2], [2, 4], [4, 6], [2, 4]], 2)], dur=1/2, lpf=expvar([800, 8000], [4, 0]), pan=sinvar([-2/3, 2/3], 8), amplify=4/5)

**PWhite(lo, hi)** >> Gibt zufällige Gleitkommazahlen zwischen lo und hi zurück.

    s1 >> arpy((0, var(PRand([Scale.default]), 8)), oct=var([5, 6], [24, 8]), dur=PDur(5, 8), room=1/2, mix=sinvar(1/3, 3/4), pan=PWhite(-1, 1), amplify=2/3)

**PChain(mapping_dictionary)** >> Basierend auf einer einfachen Markov-Kette mit gleichen Wahrscheinlichkeiten. Nimmt ein Wörterbuch mit Elementen, Zuständen und möglichen zukünftigen Zuständen. Jeder zukünftige Zustand hat die gleiche Wahrscheinlichkeit, ausgewählt zu werden. Wenn ein möglicher zukünftiger Zustand nicht gültig ist, wird ein KeyError ausgelöst.

    s1 >> rave(PChain([0, 8, 6, 3, -2, 0, -3]), dur=1/4, sus=1/8, amplify=1/2)

**PWalk(max = 7, step = 1, start = 0)** >> Gibt eine Reihe von Ganzzahlen zurück, bei denen jedes Element eine Schrittweite voneinander entfernt ist und der Wert im Bereich +/- des Maximums liegt. Das erste Element kann mit start ausgewählt werden.

    s1 >> dirt(PWalk(6, 2), dur=[1/2, PSum(4, 3)], oct=6, shape=1/3, lpf=1800, pan=(-2/3, 2/3), amplify=1/4)

**PFibMod()** >> Gibt die Fibonacci-Folge zurück.

    s1 >> feel(PFibMod()[:7] + var([0, -3, 0], 8), dur=1, shape=1/4, chop=128, room=3/4, mix=1/2)


#### [Zurück zum Index](#index)

### 2.3. Zeitvariablen (TimeVars) <a name="timevars"></a>

Ein TimeVar ist eine Abkürzung für "Time Dependent Variable" und ist eine wichtige Funktion von FoxDot.

Ein TimeVar hat eine Reihe von Werten, zwischen denen es nach einer vordefinierten Anzahl von Beats wechselt, und wird mit einem var-Objekt mit der Syntax var ([Liste_von_Werten], [Liste_von_Spieldauer]) erstellt. Dies haben wir bereits in der Sektion Player in einem Beispiel genutzt.

Beispiel:

Die Dauer kann ein einzelner Wert sein

    a = var([0, 3], 4)

'a' hat anfänglich den Wert 0

    print(int(Clock.now()), a)

Konsolenausgabe >> 0, 0

Nach 4 Schlägen ändert sich der Wert auf 3:

    print(int(Clock.now()), a)

Konsolenausgabe >> 4, 3

Nach weiteren 4 Schlägen ändert sich der Wert auf 0

    print(int(Clock.now()), a)

Konsolenausgabe >> 8, 0

Wenn ein TimeVar in einer mathematischen Operation verwendet wird, werden die betroffenen Werte auch zu TimeVars, die den Status ändern, wenn das ursprüngliche TimeVar den Status ändert. Dies kann sogar mit Mustern verwendet werden:

Schlag (Beat) ist 0

    a = var([0, 3], 4)
    print(a + 5)

Konsolenausgabe >> 5

Schlag (Beat) ist 4

    print(a + 5)  

Konsolenausgabe >> 8


Schlag (Beat) ist 8 und a hat wieder einen Wert von 0

    b = PRange(4) + a
    print(b)

Konsolenausgabe >> P[0, 1, 2, 3]

Schlag (Beat) ist 12 und hat einen Wert von 3

    print(b)

Konsolenausgabe >> P[3, 4, 5, 6]


### 2.3.1. TimeVar Typen

**linvar(values, dur)** >> Diese TimeVar wechselt zwischen Werten auf einer linearen Skala, daher der Name linvar. Wie bei allen TimeVars verwendet es eine Reihe von Werten und Dauern als Eingabe. Wenn dur weggelassen wird, wird der Wert des aktuellen Meters verwendet.

Im Laufe der Zeit ändert sich der Wert so, dass er nach der angegebenen Dauer (in diesem Beispiel 4 Schläge) genau 1 beträgt. Nach Ablauf dieser Dauer beginnt der Linvar, seinen Wert linear in Richtung des nächsten Eingabewerts zu ändern. das ist 0. Während der nächsten 4 Schläge nimmt der Wert linear in Richtung 0 ab.

**sinvar(values, dur)** >> Anstatt sich zwischen Werten mit einer linearen Rate zu ändern, ändert sich ein Sinvar mit einer Rate, die von einer Sinuswelle abgeleitet wird.


**expvar(values, dur)** >> Die Änderungsrate der expvar ist exponentiell, fängt also klein an, endet aber in großen Schritten.

Beispiel mit linvar() Typ:

    s1 >> donk(P[:2], oct=[[5, 6], 6], dur=PSum(12, 8), sus=1/2, amplify=linvar([0, 1], 4))


### 2.3.2. Anwendung

Es sollte beachtet werden, dass wenn ein Spielerobjekt eine TimeVar-Funktion mit allmählicher Änderung verwendet, der darin gespeicherte Wert zum Zeitpunkt des Auslösens der Note verwendet wird. Das bedeutet, dass Du nach dem Spielen einer Note keine zeitliche Wertänderung in der Note selbst hörst. Probiere diese Codezeilen selbst aus:

Keine allmähliche Änderung der Hochpassfrequenz:

    p1 >> dirt(dur=4, hpf=linvar([0, 4000], 4))

Scheinbare allmähliche Änderung der Hochpassfrequenz:

    p2 >> dirt(dur=1/4, hpf=linvar([0, 4000], 4))

Du kannst auch eine Dauer von 0 verwenden, um die allmähliche Änderung sofort zu überspringen und zum nächsten Wert zu wechseln. Dies ist nützlich, um Werte „zurückzusetzen“ und Drops zu erstellen.

Hebe den Hochpassfrequenzfilter auf 4000Hz, dann auf 0 zurück:

    p1 >> dirt(dur=1/4, hpf=expvar([0, 4000], [8, 0]))

Genau wie bei normalen TimeVars-Funktionen können TimeVars mit allmählicher Änderung in andere TimeVars verschachtelt werden, um die Anwendung der Werte besser zu verwalten. Zum Beispiel können wir die Hochpassfilterfrequenz nur bei den letzten 4 Schlägen eines 32-Beat-Zyklus wie folgt erhöhen.

Verwende eine normale TimeVar-Funktion, um den Wert für 28 Schläge auf 0 zu setzen:

    p1 >> dirt(dur=1/4, hpf=var([0, expvar([0, 4000], [4, 0])], [28, 4]))


### 2.3.3. Zeitvariablenfunktion als Mustern

**Pvar(patterns, dur)** >> Bisher haben wir nur einzelne Werte in einer TimeVar gespeichert, aber manchmal ist es sinnvoll, ein ganzes Pattern-Objekt zu speichern. Du kannst dies nicht mit einer regulären TimeVar tun, da jedes Muster in der Eingabeliste von Werten als eine verschachtelte Liste von Einzelwerten behandelt wird. Um dieses Verhalten zu vermeiden, musst du eine Pvar, kurz für Pattern-TimeVar (Zeitvariablenmuster) verwenden. Es wird genau wie jede andere TimeVar erstellt, aber Werte können ganze Listen/Muster sein.

    a = Pvar([[0, 1, 2, 3], [4, 5, 6]], 4)
    print(Clock.now(), a)

Konsolenausgabe >> 0, P[0, 1, 2, 3]

Du kannst sogar einen Pvar innerhalb eines Musters verschachteln, wie du es bei einem normalen Pattern tun würdest, um abwechselnde Werte abzuspielen.

Alternate the alternating notes every 8 beats

    p1 >> pluck([0, 1, 2, Pvar([[4, 5, 6, 7], [11, 9]], 8)], dur=1/4, sus=1)


#### [Zurück zum Index](#index)

### 2.4. Spieler “play” und “loop” <a name="play&loop"></a>

Derzeit gibt es in FoxDot zwei Sonderfall-Synthesizer für die Wiedergabe von einer gespeicherten Audiodatei *play* (spiel) und *loop* (schleife). In diesem Abschnitt werden wir uns diese genauer ansehen.

#### 2.4.1 Das “play” Objekt

Im Gegensatz zu anderen Synthesizern in FoxDot sollte das erste Argument bei "play" eine Zeichenfolge sein, keine Zahlen. Dies hast du bereits im oberen Teil benutzt. In der Zeichenfolge können dadurch mehr Informationen codiert werden, als das Zeichen selbst bedeutet. Jeder Charakter bezieht sich auf eine Reihe von Audiodateien wie Kicks, Hi-Hats, Snares und andere Sounds.

Das einfachste Schlagzeugmuster für Disco ist:

    b1 >> play("x-o-")

Du kannst verschiedene Arten von Klammern verwenden, um der Sequenz weitere Informationen hinzuzufügen. Wenn du zwei oder mehrere Zeichen in eine runde Klammer setzt, wechselt sich der Sound mit der neuen Schleife nacheinander ab:

Einfaches Muster:

    b1 >> play("(x-)(-x)o-")

Verschachtelte Klammern für mehr Abwechslung:

    b1 >> play("(x-)(-(xo))o-")

Wenn du mehrere Zeichen in eckige Klammern setzt, werden diese nacheinander in einem Schritt abgespielt:

Spiele ein Triplett im vierten Schlag:

    b1 >> play("x-o[---]", dur=1)

Die eckigen Klammern kannst du auch in den runden Klammern verwenden:

    b1 >> play("(x-)(-[-x])o-")

Innerhalb der eckigen Klammern, kannst du wiederum auch runde Klammern nutzen:

    b1 >> play("x-o[-(xo)]")

Du kann auch geschweifte Klammern verwenden, um eine Stichprobe nach dem Zufallsprinzip auszuwählen und dadurch deiner Sequenz Abwechslung verleihen:

Wähle im vierten Schlagschritt zufällig eine Probe aus:

    b1 >> play("x-o{-ox}")

Die geschweiften Klammern können auch wiederum eckige Klammer enthalten:

    b1 >> play("x-o{[--]ox}")

Oder die geschweiften Klammern können innerhalb von eckigen Klammern genutzt werden:

    b1 >> play("x-o[-{ox}]")

#### Attribut *sample*

Jedes Zeichen bezieht sich auf Audiodateien in einem Ordner mit selben Namen. Die Audiodateien sind in alphabetischer Reihenfolge angeordnet. Verwende das Attribut sample, um eine Audiodatei in diesem Ordner auszuwählen.

    b1 >> play("x-o-", sample=1)

Wie bei jedem anderen Argument kann dies eine Liste (nacheinander) oder sogar ein Tupel (gleichzeitig) von Werten sein.

    p1 >> play("x-o-", sample=[0, 1, 2])

    p1 >> play("x-o-", sample=(0, 3))

Das Beispiel für ein einzelnes Zeichen kann innerhalb der Zeichenfolge selbst angegeben werden, indem das Zeichen mit einem „|“ + der Positionsnummer umgeben wird:

Spiele sample=2 für den Buchstaben 'o':

    p1 >> play("x-|o2|-")

Dies wird den angebenden Wert unter sample überschreiben

    p1 >> play("x-|o2|-", sample=3)

Diese Syntax kann eine der Klammern enthalten, die zuvor für das Zeichen und die Zahlen verwendet wurden:

Ändere die Probennummer:

    p1 >> play("x-|o(12)|-")

Wechsele das Zeichen:

    p1 >> play("x-|(o*)2)|-")

Spiele mehrere verschiedene Samples in einem Schritt:

    p1 >> play("x-|o[23]|-")

Spiele eine zufällige Stichprobenauswahl:

    b1 >> play("x-|o{1[23]}|-")


#### Überlagerungssequenzen

Du kannst auch Zeichen kleiner und größer als verwenden, um mehrere Sequenzen gleichzeitig zu überlagern. Beginne mit zwei separaten Sequenzen und füge dann diese in einer einzigen Codezeile zusammen.

    b1 >> play("x-o-")
    b2 >> play("..+.+.[.+]")

***Hinweis:Der Punkt ist äquivalent zum Leerzeichen. Nach meiner Ansicht hilft der Punkt, um die zeitliche Positionierung besser zu erkennen.***

Wir können jede Sequenz zwischen "<>" Zeichen in einer einzigen Sequenz platzieren und sie gleichzeitig spielen lassen:

    b1 >> play("<x-o-><..+.+.[.+]>")

Dies ist gleichwertig zu:

    b1 >> play(P["x-o-"].zip(P["..+.+.[.+]"]))

*Zip kann man als Reissverschluss verstehen.*

Jede "Ebene" bezieht sich auf den Index einer Gruppe, sodass für eine Gruppe von Werten, die einem Spielerobjekt zugewiesen wurden, jede "Ebene" nur von einem dieser angegebenen Werte betroffen ist. Dies lässt sich am besten anhand eines Beispiels demonstrieren:

Schwenke jede Sequenz hart auf den linken und rechten Kanal durch eckige Klammern im Attribut *pan*:

    b1 >> play("<x-o-><..+.+.[.+]>", pan=[-1, 1])

Weite den Stereoeffekt durch die Nutzung durch runde Klammern:

    b1 >> play("<x-o-><..+.+.[.+]>", pan=(-1, 1))

Ändere die in der ersten Ebene verwendete Audiodatei:

    b1 >> play("<x-o-><..+.+.[.+]>", sample=(2, 0))

Sei vorsichtig, wenn du mehrere Ebenen mit einigen Funktionen wie offadd kombinierst, da dadurch neue Ebenen erstellt werden, wenn sie nicht vorhanden sind.

Der folgende Code wirkt sich nur auf die zweite Ebene aus, sodass die erste Ebene nicht betroffen ist:

    b1 >> play("<x-o-><..+.+.[.+]>", sample=(2, 0)).every(4, "sample.offadd", 2)

#### Übung:

*Versuche, mit deinen bevorzugten Samples einen 16-Takt-Rhythmus zu erstellen.
Verwende Clock.bpm (z. B. Clock.bpm = 93), um den Takt pro Minute oder die Rhythmusgeschwindigkeit in der Zeit zu ändern!*


#### 2.4.2. Das “loop” Objekt

In dieser Sektion beginnen wir gleich mit einer Übung.

#### Übung:
*Suche unter [www.wavsource.com](https://www.wavsource.com/) oder [www.findsounds.com](https://www.findsounds.com/) und nach 2-3 kurzen Audiodateien. Am besten sind Stimmen, Gesang, Beatloops, Instrumente oder Umgebungsgeräusche.*

Mit dem Loop-Synthesizer kannst du längere Audiodateien (> 1 Sekunde) abspielen und bearbeiten. Gib zunächst den Dateinamen inklusive dem absoluten Pfad, den du spielen möchtest an. Danach setze die Dauer in Beats:

    l1 >> loop("Pfad/zu/meiner/Datei.wav", dur=32)

Du kannst Dateien in einem speziellen Ordner in FoxDot / snd / _loop_ ablegen, der geöffnet werden kann, indem du im FoxDot-Menü auf "Hilfe & Einstellungen" und dann auf "Beispielordner öffnen" klickst. Wenn deine Datei dort abgespeichert ist, brauchst du nicht den vollständigen Pfad (oder die Erweiterung) für Dateien anzugeben:

    l1 >> loop("meine_datei", dur=4)

Um alle Dateien in diesem Ordner anzuzeigen, verwenden Sie print(Samples.loops). Wenn diese mit der Wiedergabereihenfolge spielen möchten, kannst nach dem Dateinamen, den FoxDot basierend auf der Dauer durchläuft, ein Argument "Position" angeben.

Spiele die ersten 4 Schläge (Beats) der Reihe nach ab:

    l1 >> loop("meine_datei", P[:4], dur=1)

Spiele die ersten 4 Schläge (Beats) in zufälliger Reihenfolge ab:

    l1 >> loop("my_file", P[:4].shuffle(), dur=1)

Wenn du die Beats per Minute (bpm) der Audiodatei kennst und sie im aktuellen Tempo abspielen möchtest, kannst du dem Player ein Tempo-Argument geben. Zum Beispiel könnte my_file ein Schlagzeugschlag mit 135 Schlägen pro Minute sein, aber das aktuelle Tempo ist 120. Du kannst dann das Tempo von my_file wie folgt an die Uhr anpassen:

Die ersten 4 Schläge in 1er Schlagschritt:

    l1 >> loop("meine_datei", P[:4], dur=1, tempo=135)

Die ersten 4 Schläge in 1/2 Schlagschritt:

    l1 >> loop("meine_datei", P[:8]/2, dur=1/2, tempo=135)

Wenn du die Audiodatei auf diese Weise zeitlich dehnst, ändert sich die Tonhöhe oder Pitch. Mit dem Attribute *striate* ist es möglich, die Audiodatei zeitlich zu verlängern, ohne diese Informationen der Tonhöhe zu verlieren. Die Datei wird in viele kleine Segmente zerschnitten und über den Wert der neuen Dauer verteilt wiedergegeben. Je größer die Audiodatei ist, desto größer ist die Anzahl der zerschnittenen Teile. Im benutzten Beispiel möchtest du möglicherweise einen Streifenwert von 100-200 für eine flüssigere Wiedergabe verwenden:

Dehne die Datei mit 100 Segmenten:

    l1 >> loop("meine_datei", dur=4, striate=100)

Dehne es mit 10 Segmenten - höre auf den Unterschied:

    l1 >> loop("meine_datei", dur=4, striate=10)

**! Bitte beachte, dass dies derzeit nicht für zeitdehnende Dateien mit schnelleren Tempi funktioniert!**


Alle FoxDot-Effekte können mit dem Loop-Synthesizer verwendet werden. Experimentiere also und finde heraus, was für die jeweilige Audiodatei am besten geeignet ist. Die Verwendung vom Attribut *slide* mit negativen Werten kann den „DJ-Scratching“-Effekt aus dem Hip-Hop der alten Schule wiederherstellen, da die Wiedergaberate auf 0 und dann wieder zurück verlangsamt wird:

    l1 >> loop("meine_datei", P[:8]/2, dur=1/2, slide=[0,0,-2])

Spiele einen aufgenommenen Titel ab:

    p1 >> loop("/absoluter_Pfad_zu_deinem_Kursverzeichnis/125bpm_robgrace.wav", dur=32)

#### Übung:
*Implementiere deine Audiodateien in einem Rhythmus und/oder benutze timevar-Funktionen und Attribute, um eine Collage zu erzeugen!*


#### [Zurück zum Index](#index)

### 2.5. Gruppen <a name="groups"></a>

Gruppen sind nützlich, um mehrer Player-Objekte gleichzeitig zu kontrollieren. Ein Piano besteht, kann aus einer Bassline, Akkordline und Melodielinie bestehen. Attribute wie Lautstärke kann dann einfacher eingestellt werden. Das kommt auch zu Hilfe, wenn du Übergänge mit Filtereffekten arrangieren möchtest (z. B. High Pass Filter auf das gesamte Schlagzeug).

    s1 >> piano(Pvar([[0, 3, 7, -2, 0, 5], [3, 0, 7, 3, 0]], [12, 8]), oct=4, dur=PDur(3, 8), sus=var([s1.dur, s1.dur*2], [6, 2]), amplify=var([2/3, 1/2], 8), amp=1)
    s2 >> piano(Pvar([[2, 5], [0, 7]], 16),oct=var([5, 6], [6, 2]), dur=var([1, 2], 32), amplify=var([1/2, 2/3], 16), amp=1)
    s3 >> piano((s1.degree,note), oct=(4, 5), dur=var([PDur(3, 8), 1], PRand(8)), amplify=2/5, amp=1)
    gPiano = Group(s1, s2, s3)

Um nun das Piano leise zu stellen, benutzt du einfach:

    gPiano.amp = 0

Es existieren bereits mehrere Gruppenobjekte in FoxDot für bestimmte Gruppen von Player-Objekten basierend auf dem Variablennamen, die mit dem Suffix '_all' enden. Also für jedes Zeichen, z.B. „s“ gibt es eine Gruppe namens s_all, die s1, s2, s3, …, s9 enthält. Wenn du also deine Player nach Variablennamen organisieren, kannst du ganz einfach Effekte anwenden oder alle auf einmal stoppen:

    s1 >> pads([0, 4, -2, 3], dur=4)
    s2 >> pluck([0, 1, 3, 4], dur=1/4)

Benutze die Gruppe um das Filterattribute auf alle Player-Objekte anzuwenden

    s_all.hpf = 500

Dies ist auch nützlich für

    s_all.amp = 0

Mit *.stop()* kannst du die komplette Gruppe von Spielern unterbrechen

    s_all.stop()

Mit *.solo()* schalten sich alle anderen Playerobjekte stumm, d. h. nur die Playerobjekte dieser Gruppe sind zu hören.

    s_all.solo()

Mit *.only()* stoppt alle Spieler, die nicht in der Gruppe sind.

    s_all.only()


---

#### [Zurück zum Index](#index)

### 3. Zunächst ein bisschen Musiktheorie <a name="musictheory"></a>

### 3.1. Songstruktur <a name="songstructure"></a>

Ein typisches Pop-Arrangement besteht aus Intro, Verse, Brücken, Refrain und Outro. Es gibt unterschiedliche Versionen davon, aber das sind die Grundlagen.

Allgemeine Strukturen für ein Lied kann wie folgt aussehen:

* Intro (4 Bars)
* 1. Vers (8 -16 Bars) + Prechorus (Optional)
* Refrain (8 - 16 Bars)
* 2. Vers (8 - 16 Bars) + Prechorus (Optional)
* Refrain (8-16 Bars)
* Outro (4 Bars)

Bars sind 4 Schläge oder Beats. Also 4 Bars bedeutet in FoxDot 16 gezählt als Schläge oder Beats.

Weitere Strukturen, während A Vers ist, B Refrain ist, C Brücke ist:

**ABABCA** >> Vers / Refrain / Vers / Refrain / Brücke / Refrain
**AABA** >> Vers / Vers / Brücke / Vers
**ABAB** >> Vers / Refrain / Vers / Refrain (simplified version of the ABABCB)
**AAA** >> Vers / Vers / Vers

Das Folgende ist ein Beispiel für eine Songstruktur in gängiger elektronischer Musik:

| Intro | Break | Buildup   | Drop  | Break |  Buildup  | Drop  | Outro |
|-------|-------|-----------|-------|-------|-----------|-------|-------|
|16 Bars|16 Bars|4/8/16 Bars|16 Bars|16 Bars|4/8/16 Bars|16 Bars|16 Bars|


#### 3.1.1 Intro

* Das Intro ist so ziemlich alles, was du daraus machen willst.
* Viele Songs beginnen nur mit der Melodie, die dann aufsteigt.
* Du kannst sogar eine melodische Frage erstellen, die vom Rest des Songs oder etwas Ähnlichem beantwortet wird.
* Das Wichtigste ist, nicht zu lange beim Intro zu bleiben und es schnell zu binden.


#### 3.1.2. Break

* Weniger laut, weniger basslastig, weniger Instrumente.
* Dies wird verwendet, um aufzubrechen, worauf der Hörer geachtet hat. In der elektronischen Musik nimmst du normalerweise das Schlagzeug heraus und fügst dem nächsten Teil einen ansteigenden Klang hinzu.
* Eine Brücke / Pause kann leistungsfähiger sein, indem neue Instrumente hinzugefügt oder die Tonart geändert werden.
* Versuche dies bei 8 Takten oder weniger zu halten.
* Die Brücke ist eine Abkehr von dem, was wir zuvor in einem Song gehört haben.
* Dies gilt sowohl für die Texte als auch für die Musik.
* Textlich ist es eine Gelegenheit für eine neue Perspektive.
* Musikalisch ist es eine Chance, dem Hörer etwas anzubieten, das er vorher noch nicht gehört hat, um das Lied interessant zu halten.


#### 3.1.3. Buildup

* Geht normalerweise von Break zum Drop und kann sogar Stille sein.
* Es bildet eine emotionale Spannung im Zuhoerer, welche dann im Drop aufgeloest wird.


#### 3.1.4. Drop

* Am lautesten, am meisten Spaß zu hören.
* Der Moment in einem Dance Track, in dem die Spannung nachlässt und der Beat einsetzt. Dies setzt ein enorme Energie frei, die während des Fortschreitens eines Songs entsteht.
* Nach dem Momentumaufbau steigt die Tonhöhe, die Spannung wird größer und lauter, bis plötzlich - der Fall.


#### 3.1.5. Riser

* Ein Riser ist wie die Break Sektion, nur dass er in Arpeggio, welches eine Art Aufbau in jedem folgenden Abschnitt aufweist.
* Normalerweise kein Schlag und die letzten 8 oder 16 Takte.
* Wenn der nächste Teil hereinkommt, wird er viel mehr Energie haben und sollte der Höhepunkt des Stücks sein.


#### 3.1.6. Outro

* Dies wird verwendet, um das Lied aufzulösen und eine reibungslose Landung zu erreichen.
* Einige Songs haben kein Outro, andere wiederum haben ein langes Outro.
* Sie können auch einen letzten Sinn hinzufügen, indem du eine Coda (Die abschließende Passage einer Komposition) oder eine starke Trittfrequenz am Ende deines Tracks hinzufügst.


#### [Zurück zum Index](#index)

### 3.2. Akkorde und Noten <a name="chords&notes"></a>

#### 3.2.1. Akkorde

Ein Akkord ist in der Musik das gleichzeitige Erklingen mindestens dreier unterschiedlicher Töne, die sich harmonisch deuten lassen.

Wir können Akkorde in verschiedene Typen einteilen, je nachdem, wie viele Noten sie haben. Wir können sie haben in:

* Gruppen von zwei Noten – die als Intervalle oder Dyaden bezeichnet werden
* Gruppen von drei Tönen – die als Dreiklangakkorde bezeichnet werden
* Gruppen von vier oder mehr Noten – die normalerweise als Septakkorde oder erweiterte Akkorde bezeichnet werden

Ein Dreiklang nimmt den Grundton oder die erste Note der Tonleiter, den dritten Tonleitergrad und den fünften Tonleitergrad ein, wobei jedes Intervall eine Terz ist.

Zum Beispiel hat die c-Moll-Tonleiter die Töne C – D – Es – F – G – Ab – B – C. Nehme die 1., 3. und 5. Note (C – Es – G), um einen c-Moll-Dreiklang zu bilden.

Ein Septakkord verwendet den Grundton, den 3., 5. und 7. Tonleitergrad, so dass ein Cmin7-Akkord das Bb hinzufügen würde (C – Es – G – Bb).
c-Moll-Septakkord

Erweiterte Akkorde fügen die 9., 11. und 13. Tonleiter hinzu (die Oktaven der 2., 4. bzw. 6.).


**Akkordumkehrung**

Wenn Du einen Akkord hast, bei dem die tiefste Note nicht die Note ist, nach der der Akkord benannt ist, nennen wir dies eine Akkordumkehrung. Eine Akkordumkehrung nimmt eine andere Anfangsnote (auch Bassnote genannt) und baut den Akkord von dort aus auf. Akkordumkehrungen werden überwiegend verwendet, um eine leichtere Stimmführung durch verschiedene Akkordfolgen, insbesondere im Bass, zu ermöglichen.

Merkmale einer Akkordumkehrung sind:

* Der Grundton (Root) befindet sich nicht in der Basis.
* Erziele eine gleichmäßige Dynamik, indem du die Akkorde neu anordnest, indem du die Oktave der Noten so änderst, dass sie näher am ersten Akkord ausgerichtet sind, und so die höchste Note an die Bassnote anpassen
* Die Umkehrung eines 5. wird zu einem 4. und Visa-Vers
* Die invertierte Dur 2. wird zur Moll 7. und die Moll 7. zur Dur 2.
* Major 6 invertiert wird ein Moll 3. und ein Moll 3. wird ein Major 6.

**Akkordprogression**

Um eine gut klingende und interessante Melodie zu erzeugen, musst du sorgfältig auswählen, wie jede Note zur nächsten Note übergeht und wie sich jede Note zu den Noten in ihrer Umgebung verhält. Noten dürfen nicht zu weit voneinander entfernt sein, und normalerweise möchtest du, dass die Notizen innerhalb der Tonart oder verwandter Tonarten bleiben.

Das gleiche Konzept wird für Harmonie verwendet. Da ein Lied normalerweise aus mehr als einem Akkord besteht, musst du jeden Akkord mit dem davor und danach in Beziehung setzen, damit die harmonische Bewegung gut und interessant klingt. Hier kommt eine Akkordprogression ins Spiel.

Eine Akkordprogression liegt vor, wenn mehrere verschiedene Akkorde nacheinander gespielt werden.


**Dur <<>> Moll**

Dur und Moll bilden die beiden Seiten der sprichwörtlichen Medaille, wenn es darum geht, die Tonart eines Liedes oder einer Komposition zu definieren.
Lieder sind entweder in einer Dur- oder einer Moll-Tonart. Manchmal enthalten komplexere Lieder oder Stücke Modulationen (Tonartwechsel), und wir können sowohl Dur- als auch Molltonarten in einem einzigen Werk vertreten sehen.
Allerdings können Dur- und Molltonarten (und ihre korrelierenden Modi) nicht gleichzeitig auftreten, zumindest in der tonalen Musik. Jedes Stück oder jeder Abschnitt eines Stückes muss entweder Dur ODER Moll sein. Sie können nicht beides sein.

Dur- und Moll-Songs basieren auf ihren entsprechenden Tonleitern (Modi). Dies informiert sowohl über den Inhalt der Melodie als auch über die Harmonie eines Stückes.

Mit anderen Worten, Lieder mit Dur-Tonart werden aus Noten ausgewählt, die in einer bestimmten Sieben-Noten-Dur-Tonleiter (wie C-Dur oder F-Dur usw.) gefunden werden. In Moll gestimmte Lieder werden aus siebenstimmigen Moll-Tonleitern (wie c-Moll oder f-Moll usw.) ausgewählt. Bei Moll gibt es jedoch eine übergeordnete Moll-Tonleiter, die als natürliches Moll bezeichnet wird, sowie zwei Varianten, die jeweils als harmonisches und melodisches Moll bezeichnet werden.

Darüber hinaus folgen Dur- und Moll-Akkordfolgen normalerweise den primären Kadenzen (harmonische Prüfsteine) des Modus, von dem sie abgeleitet sind. Dur-gestimmte Stücke enden fast immer auch auf einem Dur-„Homebase“-Akkord. Dieser Akkord wird normalerweise mit römischen Ziffern als „I“ bezeichnet.

Das Gegenteil ist bei Liedern mit Moll-Tonart der Fall. Gelegentlich jedoch überraschen klassische Stücke mit Moll-Tonart den Hörer, indem sie plötzlich mit einer großen Terz im „Homebase“- oder „I“-Akkord enden. Dieses unerwartete Umschalten gibt der Musik einen plötzlichen Auftrieb. Der klassische Begriff dafür ist Picardie-Drittel.


Erzeuge ein Moll aus einem Dur Akkord bei der Senkung des 3., 6. und 7. Tonleitergrads um eine Note.

Moll:

| Moll | Dim | Dur | Moll | Moll | Dur | Dur |
|------|-----|-----|------|------|-----|-----|

Dur:

| Dur  | Moll | Moll | Dur | Dur | Moll | Dim |
|------|------|------|-----|-----|------|-----|

Am Beispiel:

| Am | B0 | C | Dm | Em | F | G |
|----|----|---|----|----|---|---|

Cm Beispiel:

| Cm | D0 | D# | Fm | Gm | G# | A# |
|----|----|----|----|----|----|----|


* Eine Moll-Tonleiter kann erreicht werden, indem der 3., 6. und 7. Dur-Ton um eine Note abgesenkt wird.

    print(Scale.major)

Konsolenausgabe >> P[0, 2, 4, 5, 7, 9, 11]

    print(Scale.minor)

Konsolenausgabe >> P[0, 2, 3, 5, 7, 8, 10]

Wenn du nur einen Akkord in Moll ändern möchtest, senke die dritte Note.

Eine melodische Moll-Tonleiter wird durch Erhöhen der 6. und 7. Note der Moll-Tonleiter erzeugt.

    print(Scale.minor)

Konsolenausgabe >> P[0, 2, 3, 5, 7, 8, 10]

    print(Scale.melodicMinor)

Konsolenausgabe >> P[0, 2, 3, 5, 7, 9, 11]

Beispiele 7th moll skala of E (E3, F#3, G3, A3, B3, C4, D4, E4)

* E3, G3, B3, D4 – m7 (add F#4 for m9)
* F#3, A3, C4, E4 – Dim7 (add G4 for Dim9)
* G3, B3, D4, F#4 – Maj7 (add A4 for Maj9)
* A3, C4, E4, G4 – m7 (add B4 for m9)
* B3, D4, F#4, A4 – m7 (add C5 for m9)
* C4, E4, C4, B4 – Maj7 (add D5 for Maj9)
* D4, F#4, A4, C5 – Major chord with minor7 – Dom7 (add E5 for Dom9)



#### Beispiel: Billy Jean Intro (Michael Jackson)

* Skala: Moll
* Root: E
* Chords:

| Em |F#m/E| Em7 | F#m/E |
|----|-----|----|-----|
|    | C#3 | D3 | C#3 |
| B2 | A2  | B2 | A2  |
| G2 | F#2 | G2 | F#2 |
| E2 | E2  | E2 | E2  |

Die Zahl dahinter bezieht sich auf die Oktave. In FoxDot ist das mittlere C = 5, sodass Du immer 2 hinzufügen musst, wenn du vom Notenblatt komponierst.

Tempo:

    Clock.bpm = 117

Grundton E:

    Root.default = “E”

Tonleiter auf Moll:

    Scale.default = Scale.minor

Akkorde in einer Liste:

    chords=[(0, 2, 4), (0, 1, 3, 5), (0, 2, 4, 6), (0, 1, 3, 5)]

Instrument (Playerobjekt):

    s1 >> pluck(chords,oct=3,dur=[3/2,5/2],sus=2)

Schlagzeug:

    b1 >> play("<V....V..V...[VV]V..><..o.><---->")


#### Übung:
*Versuche ein eigenes Beispiel zu erzeugen! Du kannst auch die untere Richtlinie verlassen und ein eigenes Stück auszuprobieren!*

#### Bass und Akkorde of “Get Lucky” (Daft Punk)

Bass:

| B1 | D2 | F#2| E2 |
|----|----|----|----|

Akkorde:

| Bm | D | F#m| Em |
|----|----|----|----|

Im vierten Akkord gibt es eine Note, die vom Nachbarn F # m (Quintenkreis) entlehnt wurde:

| F#2 | A2 | C#3| B2 |
|----|----|----|----|
| D2 | F#2| A2 | G#2 (F#m chord key) |
| B1 | D2 | F#2| E2 |

Als Extra kannst du versuchen unter Nutzung von Timevars ein wenig Abwechslung zu erzeugen:

Drop: Komplett
Break: Ohne Schlagzeug
Buildup: Mischung von Break und Drop

Mit 4 Noten / Akkorden, die alle 16 Beats gespielt werden, sieht die Songstruktur folgendermaßen aus:

| Intro | Break | Buildup   | Drop  | Break | Buildup   | Drop  | Outro |
|-------|-------|-----------|-------|-------|-----------|-------|-------|
|16 Beats|32 Beats|32 Beats|64 Bars|32 Bars|32 Bars|64 Bars|48 Bars|


#### 3.2.2. Melodien

#### Von Akkorden zu Melodien

Eine Möglichkeit besteht darin, eine Akkordfolge zu erstellen und dann die Melodie in den Akkorden zu finden

| E3 | D3 | F3 | E3 |
|----|----|----|----|
| C3 | B2 | D3 | C3 |
| A2 | G2 | A2 | G2 |

Wie bereits erwähnt, liegt die Oktave in FoxDot 2 Schritte über dem üblichen Wert, das mittlere C beträgt 5 und nicht 3.

Lass uns die Akkorde mit 93 Schlägen pro Minute mit A als Grundton und Moll-Tonleiter spielen

    Clock.bpm = 93
    Root.default=”A”
    Scale.default=Scale.minor
    chords = var([(0,2,4), (-1,1,3), (0,3,5), (-2,2,4)])

Akkorde:

    s1 >> swell(chords, oct=5, dur=4, sus=5)

Ein Schlagzeugschlag kann dabei helfen, eine gute Melodie zu finden:

    b1 >> play("<X....X..X..[X.].X..><..o.><---->",sample=0)

Der einfachste Weg, um mit einer Melodie zu beginnen, besteht darin, die höchsten Noten der Akkorde zu nehmen

Du möchtest jedoch deinen Akkordnoten einige Nicht-Akkord-Noten hinzufügen:

| E4 | F4 | D4 | F4 | D4 | G4 | E4 | D4 |
|----|----|----|----|----|----|----|----|

    seq=[4,5,3,5,3,6,4,3]
    s2 >> pulse(seq, oct=6, dur=[3, 1, 3, 3, 1, 1, 2, 2])


#### Von der Melodie zu den Akkorden

In diesem Beispiel beginnen wir mit einer Melodie, um passende Akkorde daraus zu erhalten, hier die Melodie

| A3 | B3 | C4 | B3 | E4 | F4 | C4 | G4 | E4 | D4 |
|----|----|----|----|----|----|----|----|----|----|

Lass uns das Tempo, den Grundton und die Skala setzen:

    Clock.bpm = 93
    Root.default=”A”
    Scale.default=Scale.minor

Die basierende Melodie:

Wenn du dich nicht an die Nummern der Skalenliste erinnern kannst, verwende print(Scale.minor).

    seq=[0, 1, 2, 1, 4, 5, 2, 6, 4, 3]

Instrument:

    s1 >> saw(seq, dur=[2, 1, 1, 4, 3, 1, 1, 1, 1, 1], formant=4, amplify=2/5)

Die verfügbaren Akkorde (mit 7.) für die in der Melodie gespielten Noten lauten wie folgt:


| G4 | A4 | B4 | C5 | D5 | E5 | F5 |
|----|----|----|----|----|----|----|
| E4 | F4 | G4 | A4 | B4 | C5 | D5 |
| C4 | D4 | E4 | F4 | G4 | A4 | B4 |
| A3 | B3 | C4 | D4 | E4 | F4 | G4 |

Hier ein gutes Beispiel für eine Trip-Hop-ähnlichen Track:

| E4 | D4 | E4 | E4 |
|----|----|----|----|
| C4 | B3 | C4 | G4 |
| A3 | G3 | A3 | C4 |

Fügen wir der Melodie die Akkorde hinzu:

    chords = var([(0, 2, 4),(-1, 1, 3),(0, 2, 4),(-1, 6,4 )])
    s2 >> keys(chords, oct=4, dur=4, shape=3/5)

Und ein Schlagzeugschlag:

    b1 >> play("<X....X..X..[X.].X..><..o.><---→", sample=0)


**Füge eine Gegenmelodie hinzu (Arpeggio)**

Halten wir es einfach und verwende die Akkordnoten, um mit den Akkorden zu spielen.
Mit der Gegenmelodie wollen wir dem Track einen Rhythmus hinzufügen.

Als 4. der Sequenz in einem 4-Beat-Takt fügen wir den 2. wie hier gezeigt hinzu:

    chords = var([(0, 2, 4), (-1, 1, 3), (0, 2, 4), (-1, 4, 6)])

wird

    seq2 = [0, 2, 4, 2, -1, 1, 3, 1, 0, 2, 4, 2, -1, 4, 6, 4]

Fügen wir nun ein weiteres Instrument hinzu, das die Gegenmelodie spielt

    s3 >> karp(seq2, dur=1)


#### Von der Akkordfolge bis zu den Basslinien

Im folgenden Beispiel basiert die Akkordfolge auf A in Moll, während ein Grundton auf eine höhere Oktave angehoben und ein Grundton abgesenkt wird

| G3 | G3 |    |    |
|----|----|----|----|
| E3 | F3 | G3 | G3 |
| C3 | D3 | E3 | E3 |
| A2 | B2 | C3 | B2 |

Lass uns das Tempo, den Grundton und die Skala setzen:

    Clock.bpm = 128
    Root.default=”A”
    Scale.default=Scale.minor

    chords = var([(0, 2, 4, 6), (1, 1, 3, 6), (2, 4, 6), (1, 4, 6)])
    s1 >> prophet(chords, oct=4, dur=4, sus=4)

Der sichere Fall besteht darin, Grundtonnoten von Akkorden als Bassnoten zu verwenden und diese Noten in der Oktave zu senken

| A1 | G1 | C2 | E2 |
|----|----|----|----|

    bass1 = [0, -1, 2, 4]

Eine andere Möglichkeit, eine Basslinie zu erstellen, ist die Suche nach Noten innerhalb der Akkorde (obwohl die 7. schwierig sein kann).

| A1 | B1 | C2 | B1 |
|----|----|----|----|

Du kannst auch die Dauer der Basslinie ändern, um eine rhythmische Komponente zu erhalten:

Benutze anstelle von *dur=4* >> *dur=1*, *dur=[1/2,1]*, oder *dur=[1,2,1]*.

Eine andere Möglichkeit besteht darin, den Grundton eines Akkords einen Schritt in die vorherige Akkordleiste zu verschieben.

Mit dur = 1

    bassline2=[0, 0, 0, -1, -1, -1, -1, 2, 2, 2, 2, -3, -3, -3, -3, 0]

Oder du verwendest Oktavsprünge

    bassline3=[0, 0, 0, 0, -1, -1, -1, -1, 2, 2, 2, 2, -3, -3, -3, -3]

mit *dur=1* und *oct=[3, 3, 4, 3]*

Zu guter Letzt eine Melodie als Basslinie:

| A1 | G1 | A1 | B1 | A1 | G1 | G1 | A1 | C2 | C2 | A1 | G1 | E1 | E1 | F1 | G1 |
|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|

    bassline4 = [0, -1, 0, 1, -1, -1, 0, 1, 3, 3, 0, -1, -3, -3, -2, -1]


#### Von den Basslinien bis zur Akkordfolge

Wir beginnen mit Tempo, Grundton, Tonleiter und einer einfachen Basslinie:

    Clock.bpm = 128
    Root.default=”A”
    Scale.default=Scale.minor
    bassline=[0, 0, 0, 0, 0, 0, 0, 1]

Jetzt bauen wir Akkorde entlang des Moll-Akkords, wie: Am, Bm / A, G / A, Am.

    chords = var([(0, 2, 4), (1, 3, 5), (0, 2, 4), (-1, 1, 3), (0, 2, 4)])

Bm / A und G / A bedeuten "über A", da die Basslinie immer noch A als Grundton des Akkords behält.

Die entsprechenden Synth-Beispiele für Bass und Akkorde sind:

    s1 >> jbass(bassline, oct=3, dur=1/2, shape=2/5) #Bass
    s2 >> dirt(chords, oct=5, dur=[4, 3, 1, 4, 4], amplify=3/5) #Akkorde

Schlagzeug:

    b1 >> play("<V....V..><..o.><....k..d>←--[--]>", sample=var([4, 2], 16), amplify=2/5)
    b2 >> play(var(["[ss]",".[ss]"]), amplify=3/5)

Einige zusätzliche Bemerkungen zu einer Basslinie

**Benutze zeitgemäß nur eine Note im unteren Frequenzbereich!**


#### [Zurück zum Index](#index)

### 3.3. Skalen und Modi <a name="scales"></a>

Eine Tonleiter oder Skala ist technisch definiert eine Folge von auf- oder absteigenden Einheitstönen, die eine Palette von Noten bilden, die verwendet werden können, um eine Melodie zu bilden. Die meisten Tonleitern in der westlichen Musik entsprechen einer bestimmten Tonart. Das heißt, eine Sequenz von Noten, die standardmäßig Dur oder Moll ist. Dies trifft nicht fuer die chromatische Tonleiter zu, welche eine Tonleiter aller möglichen Halbtonschritte der westlichen Musik ist. Auch die Ganztonleiter ist eine Tonleiter, die aus Intervallen besteht, welche zwei Halbtöne voneinander entfernt sind.

Innerhalb einer bestimmten Tonart gibt es 7 Noten in einer einzelnen Oktave, bevor die 8. Note erreicht wird, die den gleichen Namen wie die erste Note hat und die doppelte Frequenz hat. Die sieben Noten haben unterschiedliche Intervalle zwischen benachbarten Noten. Manchmal ist es ein Halbton (Halbton), manchmal ein ganzer Ton (zwei Halbtöne). Das Muster der Ganzton-/Halbtonintervalle, die die Noten einer Tonart bestimmen, beginnend mit der Note, während die Tonart benannt ist, ist ganz-ganz-halb-ganz-ganz-ganz-halb. Innerhalb einer einzelnen Tonart könnte jede dieser sieben Noten als Grundnote einer aufsteigenden Sequenz verwendet werden. Jede solche Sequenz, die erstellt wird, indem mit einer anderen Note in der Tonart begonnen wird, ist ein Modus dieser Tonart, und jeder Modus hat einen Namen. Zum Beispiel:

***W >> Vollton (Whole)***
***H >> Halbton (Half)***

* Ionian - beginnt mit dem "Tonikum"; die Note, für die der Schlüssel benannt ist. In der Tonart C beginnt der Ionische Modus mit C. Dieser Modus ist der gebräuchlichste und wird umgangssprachlich als "Dur-Tonleiter" bezeichnet. Das Muster ist WWHWWWH.
* Dorian - beginnt mit der nächsten Note höher in der Tonart als die Tonika (D, in der Tonart C). WHWWWHW.
* Phrygian - beginnt mit der Note, die eine große Terz höher als die Tonika (E) ist. HWWWHWW.
* Lydian - beginnt mit der Note, die eine volle Quarte höher ist als die Tonika (F). WWWHWWH.
* Mixolydian - beginnt mit der Note, die eine Quinte höher als die Tonika (G) ist. WWHWWHW.
* Aeolian - beginnt mit der Note eine große Sexte höher als die Tonika (A). In der modernen Musik ist dieser Modus ebenfalls sehr wichtig und wird als "natürliche Moll-Tonleiter" bezeichnet. WHWWHWW.
* Locrian - beginnt mit der Note eine große Septime höher als die Tonika (B). HWWHWWW.

In FoxDot werden Python-Listen verwendet, um alle Noten einer Tonleiter aus der chromatischen Tonleiter zu enthalten. Wie bei einem Klavier hat eine Oktave 12 Tasten. Die chromatische Skala in FoxDot:

>> print(Scale.chromatic)

***Output:*** P[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

Der Unterschied zwischen der traditionellen Version und der FoxDot-Version beträgt genau 1. Während die traditionelle Musiktheorie mit 1 - 12 für eine Oktave in Bezug auf eine Tonleiter oder einen Modus arbeitet, beginnen FoxDot-Skalen und -Noten mit 0 - 11. Das hat mit der Handhabung von Python Listen zu tun.

Hier ein Beispiel:

>> print(Scale.major)

***Output:*** P[0, 2, 4, 5, 7, 9, 11]

Die Liste der Dur-Tonleiter sind die Zahlen, die sich auf die Note in der chromatischen FoxDot-Tonleiter beziehen.

Diese Tonleiter hat 7 Noten. In Python beginnt die Position von Zahlen in einer Liste mit 0, also haben wir eine Liste von Zahlen, die wir von 0 bis 6 adressieren können.

Hier ein Beispiel:

  Als Erstes wird der Tonleiter ein Variablennamen zugewiesen:

  >> scale = Scale.major

  >> print(scale)

  ***Output:*** P[0, 2, 4, 5, 7, 9, 11]

Um die Noten in der Liste zu spielen oder aufzurufen, benutze die Tonleiterliste mit einer in eckige Klammern eingebetteten Positionsnummer (Index):

  >>print(scale[1])

  ***Output:*** 2

Das letzte Beispiel zeigt, was eigentlich geschieht, wenn Zahlen in FoxDot als Toene verwendet werden. Tatsächlich werden die Position der Noten in der Tonleiterliste aufgerufen (Standard: Scale.major)

Wenn also die Noten melodie = [0, 2, 4] sind, bedeutet das eigentlich ***Spiele die 1., 3. und 5. Note der Tonleiter***, welche 0 = 0, 1 = 2 und 2 = 4 in Scale.major P[0, 2, 4, 5, 7, 9, 11] ist.


---

**Skalentabelle**

<img src="images/foxdot_scalen.jpg" width="960"/><br>


#### 3.3.1. Modi verwenden

**Intro Beispiel 1:**

Beginnen wir mit Intro Beispiel 1: Speed, Root and Scale

Tempo:

    Clock.bpm=140

Grundnote **D**:

    Root.default.set(2)

Skala:

    Scale.default = Scale.dorian

Schlagzeug als nächstes:

Bass Drum 1:

    b1 >> play("V..v....", rate=4/5, sus=4, sample=2, formant=var([4,3,2,1],4), lpf=1000, amplify=3/5)

Bass Drum 2:

    b2 >> play("v..V....", rate=1, sample=-1, room=1/2, mix=1/2, amplify=4/5)

Bass Drum 3:

    b3 >> play(Pvar(["..","V."], 64), rate=2, sample=-1, amplify=3/5)

Snare 1:

    b4 >> play(Pvar(["....o...","....o..o"], [12, 4]), rate=7/5, sample=4, lpf=linvar([2400, 4000], 8), room=4/5, mix=1/2, amplify=3/5)

Snare 2:

    b5 >> play(Pvar(["..[i.].","..i[.i]"], [12, 4]), sample=7, room=2/3, mix=1/2, amplify=3/5)

Shaker:

    b7 >> play(".[ss]..", rate=7/5, room=1/2, mix=1/2)

HiHat 1:

    b8 >> play("-.-.").every(16, "mirror")

HiHat 2:

    b9 >> play(Pvar(["..","-[--]"],64), sample=8, amplify=4/5)


Nun setzen wir einige Basslinien ein, welche die Referenz für die Akkorde sein werden.

Setze diese Bassnotenlisten ein:

    bassline=[0, 2, 4, 0, 7]				
    bassline2 = PShuf(bassline)

Bassline 1:

    s1 >> klank(bassline, oct=4, dur=[6, 4, 2, 2, 2], shape=1/5, amplify=3/5)

Bassline 2:

    s2 >> space(PShuf(Scale.default), oct=3, shape=1/4, room=3/5, mix=1/2)

Erstelle nun Akkorde aus der 3. und 5. Basslinie, während die inversen Noten für eine gute Akkordfolge zu hoch oder zu niedrig sind:

    chords = Pvar([[-3, 0, 2], [-3, -1, 2], [-3, -2, 1], [-3, 0, 2], [-3, 0, 2]], [6, 4, 2, 2, 2])
    s3 >> pluck(chords, oct=6, dur=1/2, room=4/5, mix=1/2, shape=2/6, amplify=3/5)

Endlich eine Melodie aus den Akkordnoten:

    seq = [2, -1, -3, 2, 5, 4, 2] 			
    s4 >> sinepad(seq, oct=6, dur=[3, 3, 2, 3, 1, 2, 2], room=3/4, mix=1/2, amplify=4/5)


**Intro Beispiel 2:**

Und hier ist ein weiteres Intro-Beispiel: Wieder erste Geschwindigkeit, Grundton, Skalierung

Tempo:
    Clock.bpm=120

Grundton:

    Root.default.set(4)

Skala:

    Scale.default=Scale.phrygian

Schlagzeug:

Bass Drum:

    b1 >> play("d..d....", rate=3/5, sus=2, sample=8, formant=1, lpf=1400, room=3/4, mix=1/2, amplify=3/5)

Snare:

    b2 >> play(Pvar(["....g...","....g..g"],4), rate=1, sample=4, room=2/5, mix=1/2, amplify=2/5)

HiHat:

    b3 >> play("----", rate=7/5, sample=2, amplify=2/5)

Trippige Basslinie:

    bassline=[0, 0, 0, 0, 0, 0, 0, 1]
    bassline2=[-1, -1, -1, -1, -1, -1, -1, 0] #Hier eine Sekunde für eine bessere Akkordfolge
    s1 >> donk(Pvar([bassline, bassline2, bassline], [96, 32, 32]), oct=5, dur=1/2, shape=1/3, amplify=3/5, amp=var([0, 1],[16, 144]))
    s2 >> bass(Pvar([bassline, bassline2, bassline], [96, 32, 32]), oct=5, dur=1/2, shape=1/5, amplify=3/5, amp=var([0, 1],[32, 128]))

Akkordfolge:

    chords = Pvar([(0, 2, 4), (0, 2, 5), (0, 3, 5), (0, 3, 4)])
    chords2 = Pvar([(-1, 2, 4), (-1, 2, 5), (-1, 3, 5), (-1, 3, 4)])

Akkorde:

    s3 >> saw(Pvar([chords, chords2, chords],[96, 32, 32]), oct=6, dur=4, room=3/5, mix=1/2, lpf=1300, amplify=4/5, amp=var([0, 1],[32, 128]))

Melodie:

    seq = [2, -1, -3, 2, 5, 4, 2]
    s4 >> sinepad(seq, oct=6, dur=[3, 3, 2, 3, 1, 2, 2], shape=1/5, formant=0, room=3/4, mix=1/2, amplify=3/5, amp=Pvar([0, 1],[48, 16, 16, 16, 16, 16, 16, 16]))

**Hinweis: Wie du vielleicht in den letzten beiden Beispielen sehen konntest, habe ich die Lautstärke des Instruments mit amplify eingestellt, während ich die Master-Lautstärke für Kompositionszwecke mit amp eingestellt habe.**

Da es sich um Intro-Beispiele handelt, benötigen wir keine Gruppen, um die Komposition anzuordnen. Wir werden später Gruppen haben.

Derzeit werden die Instrumente direkt über eine Amp-Liste ein- und ausgeschaltet, die durch die Beats zählt.

amp=var([0, 1],[16, 144])

Pvar([bassline, bassline2, bassline], [96, 32, 32])


#### 3.3.2. Verwenden von Skalen

* Eine Skala ist im Wesentlichen eine Teilmenge der Noten (Tonhöhen) zwischen einer Note, z.B. C und das gleiche eine Oktave höher.
* Die Startnote ist der Schlüssel der Skala.
* Ab C sind diese Noten:

| C | C# | D | D# | E | F | F# | G | G# | A | A# | B (H) |
|----|----|----|----|----|----|----|----|----|----|----|----|

* Dieser Satz aller Noten wird als chromatische Skala bezeichnet.
* Wenn dies eine Python-Liste namens chromatisch wäre, würde chromatisch [0] C zurückgeben, chromatisch [1] würde C # zurückgeben, chromatisch [2] würde D zurückgeben und so bis chromatisch [11], was B zurückgeben würde.
* Da jede Musikskala eine Teilmenge dieser Tonhöhen ist, können wir uns jede Tonleiter als eine Liste von Indizes für den Zugriff auf Tonhöhen in der chromatischen Skala vorstellen.

chromatic = [C, C#, D, D#, E, F, F#, G, G#, A, A#, B]

| C | C# | D | D# | E | F | F# | G | G# | A | A# | B (H) |
|----|----|----|----|----|----|----|----|----|----|----|----|
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 |

* Um eine Liste der verfügbaren Skalen anzuzeigen, führe einfach den Befehl print(Scale.names ()) aus.
* Standardmäßig verwendet jeder Spieler eine global zugängliche Standardskala namens Scale.default
* Dies kann auf drei Arten geändert werden:

Ordne die Skala einfach Scale.default zu:

    Scale.default  = Scale.minor

Du kannst auch den Zeichenfolgennamen verwenden:

    Scale.default = "minor"

Und du kannst die "set"-Methode verwenden, die weitere Optionen ermöglicht:

    Scale.default.set("minor")

Es ist auch möglich, die Skala der Spieler individuell zu ändern:

Zwinge ein Playerobjekt die Moll-Skala zu verwenden:

    p1 >> pluck([0, 1, 2, 3], scale=Scale.minor)


#### Übung:
*Verwende den folgenden Code, um alle verfügbaren Skalen zu durchlaufen, die FoxDot bereitstellt.*

Zeigt alle verfügbaren Skalen an:

    print(Scale.names())

Weise die gewählte Skala als Standard zu:

    Scale.default=Scale.chromatic

Variable, um jeder Note der Skala einen Schritt zuzuweisen:

    steps=len(Scale.default)

Spiele die Noten durch die Skala:

    p1 >> pluck(P[:steps])


#### [Zurück zum Index](#index)

### 3.4. BeatBox <a name="beatbox"></a>

* Versuche, deine Beats mit Variationen, Modulationen und / oder Swings anzureichern, um sie interessanter zu gestalten.
* Variationen sind Änderungen in der Beat-Struktur von einem Takt zum anderen.
* Modulationen wirken sich auf das gesamte Schlagzeug oder auf einzelne Teile des Schlagzeugs aus.
* Passe einige Off-Noten an, um eine andere Dynamik im Beat zu erzielen, und gebe deinem Beat etwas Schwung.
* Achte darauf, nicht zu dynamisch zu werden, sonst verlierst du dadurch den Antrieb des Basskicks.

#### 3.4.1. Beats erstellen

Beginne mit dem Grundmuster aus einem Kick, einer Snare und einem HiHat.

    b1 >> play(“X...X...”)
    b4 >> play(“..o...o.”)
    b7 >> play(“-.-.-.-.”)

“.” (Punkt) wird als Platzhalter verwendet, um die Anzeige zu erleichtern.

Da wir unseren Beat in Zukunft erhöhen werden lasse 3 Spieler für die Trommel oder trommelähnliche Klänge, 3 Spieler für die Snare und Snare-ähnliche Klänge und 3 für hihat, openhats usw. Dies ist nur eine Möglichkeit, die Beatbox zu organisieren, da du eine bessere Übersicht über Frequenzbereiche behältst. Du kannst natürlich auch die Struktur durch unterschiedlichen Buchstaben bilden.

Fügen wir nun dem Hihat eine Variation hinzu:

    b7 >> play(“-.-.-.-.”).every(16, ”mirror”).every(8, ”stutter”, 2)

Und hier ein weiteres Beispiel:

    b8 >> play(“--------”, sample=3, amplify=[1/3, 1/3, 2/3, 1/3, 1/3, 2/3, 1/3, 2/3])

Du kannst auch "Geister"-Noten hinzufügen, die normalerweise leiser sind. 16 ungewöhnliche Noten vor oder nach der "Main"-Note. Dazu verwenden wir <> zum Überlagern, um die Lautstärke an die Geister-Note anzupassen

    b4 >> play(“<..o...o.><.[.o]......>”, amplify=(3/4, 1/3))


#### 3.4.2. Attribute

Hier sind diejenigen Attribute, die mit dem *play* Spielerobjekt funktionieren:

*dur, delay, sample, sus, pan, slide, slidedelay, glide, glidedelay, bend, benddelay, coarse, striate, rate, pshift, hpf, hpr, lpf, lpr, swell, shape, chop, tremolo, echo, echotime, spin, cut, verb, room, mix, formant, shape, drive, blur*

Wenn du zum Beispiel *pshift* verwendest, kannst du die Tonhöhe des Samples verändern:

    b1 >> play("#", dur=2, pshift=linvar([0, 8], 16))


#### Übung:

**Folgende Beispiele werden dir helfen, das Konzept anhand bekannter Rhythmen und Beats zu erfahren. Benutze darueber hinaus deine eigenen Argumente.**

Source: [341_Beatbox_CreatingBeats.py](/files/code/341_Beatbox_CreatingBeats.py)

#### Beispiel: House Beat

Tempo:

    Clock.bpm=128

Bass:

    b1 >> play("X.", rate=4/5, sample=2, amplify=2/3)

Clap:

    b4 >> play("..*.", sample=3, amplify=2/5)

Snare:

    b5 >> play("......o.", rate=7/5, sample=1, amplify=1/2)

HiHat:

    b7 >> play(".-", rate=4/5, sample=3, delay=PRand([0, Pwhite(-1/20, 1/20)]), amp=2/3)

Cymbal:

    b8 >> play("#", rate=5/4, sample=0, dur=16, sus=8, amplify=4/5)


#### Beispiel: Drum N Bass

Tempo:

    Clock.bpm=170

Bass:

    b1 >> play("V....V..VV...V..", rate=4/5, sample=2, amplify=2/3)
    b2 >> play("v......[vvvv]", sample=4, amplify=2/3)

Snare:

    b4 >> play(Pvar(["..o.","..o[.o.]"],[12, 2]), sample=2, amplify=2/5)
    b5 >> play("..i.", amplify=PRand([3/5, PWhite(2/3, 3/5)]))

Shaker:

    b7 >> play("s", rate=4/5, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))

Closed HiHat:

    b8 >> play("-", rate=7/5, pshift=linvar([0, 16], 8), sample=2, shape=1/3, amplify=7/5)


#### Beispiel: Dubstep

Tempo:

    Clock.bpm=140

Bass:

    b1 >> play(Pvar(["V...V...", "V[..V.]..[V..V][..V.].[..V.]"], 16), dur=1, rate=6/5, sample=6, amplify=2/3)
    b2 >> play(Pvar(["X...X...", "X[..X.]..[X..X][..X.].[..X.]"], 16), dur=1, sample=2, amplify=2/3)
    b3 >> play(Pvar(["v...v...", "v[..v.]..[v..v][..v.].[..v.]"], 16), dur=1, sample=4, amplify=2/3)

Snare:

    b4 >> play(Pvar(["..o...o.", "..o...oo"], 16), dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(Pvar(["..i...i.", "..i...ii"], 16), dur=1, sample=4, amplify=2/5)
    b6 >> play(Pvar(["..h...h.", "..h...hh"], 16), dur=1, sample=5, amplify=2/5)

Closed HiHat:

    b7 >> play("-", dur=1/2, rate=3/5, pshift=linvar([0, 8], 8), sample=4, amplify=4/5)
    b8 >> play("s", dur=1/2, rate=1, sample=1, amplify=PRand([3/5, PWhite(2/3, 3/5)]))

BuildUp:

    c1 >> play("V.", dur=Pvar([1, 1/2, 1/4, 1/16],[16,8,4,4]), rate=6/5, sample=6, amplify=Pvar([2/3, 0], [30, 2]))
    c2 >> play("X.", dur=Pvar([1, 1/2, 1/4, 1/16],[16, 8, 4, 4]), sample=2, amplify=Pvar([2/3, 0], [30, 2]))
    c3 >> play("v.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=4, amplify=Pvar([2/3, 0], [30, 2]))
    c4 >> play("o.", dur=Pvar([1, 1/2, 1/4, 1/16],[16, 8, 4, 4]), rate=3/4, sample=2, amplify=Pvar([2/5, 0], [30, 2]))
    c5 >> play("i.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=4, amplify=Pvar([2/5, 0], [30, 2]))
    c6 >> play("h.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=5, amplify=Pvar([2/5, 0],[30, 2]))

Erstelle Gruppen, um eine ganze Sammlung von Playerobjekten rechtzeitig zu steuern:

    gB = Group(b1, b2, b3, b4, b5, b6, b7, b8)
    gC = Group(c1, c2, c3, c4, c5, c6)

Kontrolliere die Lautstaerke im Laufe der Zeit:

    gB.amp=Pvar([1, 0], [64, 32])
    gC.amp=Pvar([0, 1], [64, 32])


#### Übung:
*Füge stretch, pshift, rate oder reverse hinzu, um verschiedene Muster zu erstellen!*


#### Beispiel: Trap

Tempo:

    Clock.bpm=140

Bass:

    b1 >> play("[VV]..V[.V]V.[.V].V..V.V.V.", dur=1, rate=6/5, sample=-1, amplify=2/3)
    b2 >> play("[XX]..X[.X]X.[.X].X..X.X.X.", dur=1, sample=2, amplify=2/3)

Snare:

    b4 >> play("..o.", dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(".H..", dur=1, rate=7/5, sample=1, pan=(-5/7, 5/7), amplify=3/5)

Closed HiHat:

    b7 >> play("[--]", dur=PRand([4, 2, 1, 1/2, PDur(3,8)*2, PDur(3,7)*2], 1/4), rate=3/4, sample=3, amplify=3/5)
    b8 >> play("[--]", dur=PRand([4, 2, 1, 1/2, 1/4]), rate=2/4, sample=-1, amplify=3/5)

Hier ein paar Instrumente dazu:

    s1 >> dub(PRand([0, 2, 3], 1/4), oct=(3,4), dur=4, chop=PRand([6, 8]), shape=2/3, amplify=1/3)
    s2 >> space(s1.degree, oct=(4,5), dur=4, chop=PRand([3, 4]), room=3/5, mix=1/2, amplify=6/5).offbeat()
    s3 >> pulse([2, 3, 5, 7, 9], oct=var([3, 4, 5]), dur=PRand([1/2, 1/4], 6), shape=2/3, formant=var([3, 0, 2], 1/2), room=3/4, mix=1/2, pan=[-2/3, 2/3], amplify=3/5)


#### Beispiel: HipHop

Tempo:

    Clock.bpm=80

Bass:

    b1 >> play("X..X....X.XX....", rate=var([4/5, 1], 8), formant=2, sample=5, amplify=5/3, amp=1)

Snare:

    b4 >> play("..i.", rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(".H.......H......", dur=1/2, rate=7/5, sample=1, delay=1/16, pan=(-5/7, 5/7), amplify=2/5)

Closed HiHat:

    b7 >> play("--.-", rate=3/4, sample=3, amplify=3/5)

Open Hat / Shaker:

    b8 >> play(".............#..", rate=7/5, sample=2, amplify=1, amp=1)
    b9 >> play("[ss]", rate=3/4, sample=2, hpf=linvar([800, 6000], 1), amplify=3/5, amp=1).every(PRand([4, 8, 12, 16]), "stutter", PRand([2, 3, 4]))


#### Beispiel: Footwork

Tempo:

    Clock.bpm=154

Bass:

    b1 >> play("X..X..X.X..X..X.", dur=1, rate=6/5, sample=-1, amplify=2/3)
    b2 >> play("V..V..V.V..V..V.", dur=1, sample=1, amplify=2/3)
    b3 >> play("{([XX])([X.])([X...])}", dur=1, rate=PRand([3/4, 3/5, 1, 7/5], 1/4), shape=linvar([1/7, 3/5], 16), amplify=2/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))

Snare:

    b4 >> play("............H...", dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)])).every(PRand([4, 8, 12]), "stutter", PRand([2, 3]))
    b5 >> play("......o.......o[oo.o]",dur=1,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)
    b6 >> play("",dur=PRand([4, 2, 1, 1/2, PDur(3, 8)*2, PDur(3,7)*2], 1/4), rate=2/4, sample=-1, amplify=3/5)

HiHat:

    b7 >> play("..-.....", rate=3/4, sample=3 , amplify=3/5)
    b8 >> play("-", dur=1, sample=3, amplify=4/5)
    b9 >> play("{([--])(M)}", dur=1, rate=PRand([3/4, 3/5, 1, 7/5], 1/4), sample=2, shape=linvar([1/7, 3/5], 16), amplify=1/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([3, 5]))


#### Example: Funk

Tempo:

    Clock.bpm=118

Bass:

    b1 >> play("VV...[VV]..", dur=1/2, rate=6/5, sample=-1, amplify=1/3)
    b2 >> play("VV...[VV]..", dur=1/2, sample=linvar([0, 5], 4), amplify=1/3)

Snare:

    b4 >> play("..[o.][.o][.o].[o.][.o]", dur=1/2, rate=2, sample=5, amplify=PRand([2/5, PWhite(1/3, 2/5)]))
    b5 >> play("....i..i.i..i..i", dur=1/4, rate=1, sample=3, pan=(-5/7, 5/7), amplify=3/5)
    b6 >> play("..o.", dur=1, rate=2, sample=5, pan=(-5/7, 5/7), amplify=3/5)

HiHat:

    b7 >> play("[-.-.][-.--][-...][-.-.][--..][-.-.][-.-.][-...]", dur=1, rate=1, sample=2, amplify=1)
    b8 >> play("[-.]", dur=1/2, rate=1, sample=-1, amplify=4/5, amp=1)
    b9 >> play("[ll].-.", sample=var([3, 4, 0], 16), formant=linvar(5, 8), amplify=4/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))
    b0 >> play("[ss]", rate=1, sample=2, shape=2/3, amplify=4/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))
    gBeats = Group(b1, b2, b3, b4, b5, b6, b7, b8, b9, b0)


#### 3.4.2. Übergänge erstellen

* Hochfahren, dann atmen, dann erneut schlagen (Trommelwirbel ... Stille ... Beat). Hier kann man vorzüglich die Gruppenzuweisungen z. B. mit *gBeats.hpf=linvar([0, 5000], [12, 0], start=Clock.now())*, dann auf Schlag *gBeats.amp=var([0, 1], [4, inf], start=Clock.now())*.
* Subtrahiere, bevor du hinzufügst. Nehme z. B. den Bassbeat raus, und lass nur Snare und HiHat laufen.
* Rolle und beschleunige z. B. Snare, HiHat und Shaker mit Achtel- und Sechzehntelnoten (Funktioniert auch mit Kicks und Toms).
* Wenn du einen Übergang von einem Abschnitt zu einem anderen benötigst, ohne große Subtraktion wie das Herausnehmen des Schlagzeugschlags, sei spitzfindig.


#### [Zurück zum Index](#index)


### 4. Live Jam Session – Lass uns alle zusammen spielen <a name="livejam"></a>

* Um gemeinsam mit FoxDot Musik zu erstellen, verwenden wir [Troop](https://github.com/Qirky/Troop), eine in Python geschriebene Server-Client-Umgebung.
* Dadurch wird eine Server- <-> Client-Verbindung über ein Netzwerk hergestellt, vorzugsweise über WLAN.


### 4.1. Stelle eine Verbindung zum Wifi-Netzwerk her <a name="wifi"></a>

* Um gemeinsam mit FoxDot Musik zu erstellen, verwenden wir Troop, eine in Python geschriebene Server-Klient-Umgebung
* FoxDot wird OSC (ein digitales besseres MIDI) für die Kommunikation verwenden, genauso wie es unter der Haube mit SuperCollider kommuniziert.
* Der Server verarbeitet nur alle OSC-Nachrichten des Klienten.
* Die Klienten führen den SuperCollider-Audioserver unter FoxDot aus, um den Sound zu erzeugen.
* Wenn du Ihr eigenes Netzwerk verwendest, überprüfe auf dem Server die IP-Adresse, mit der eine Verbindung zum WLAN-Hotspot hergestellt wird. Dies ist die IP, die du in Troop verwendest!
* Um ein Wifi Offlinenetzwerk zu erzeugen, kann man einen der beteiligten PCs/Laptops als Hotspot Router nutzen. Die anderen können sich dann via Wifi  miteinander verbinden. Der Hotspot Router PC/Laptop kann dann ebenfalls das Server-Skript von Troop parallel laufen lassen.
* Eine weitere Möglichkeit eines Offlinenetzwerk ist die Nutzung des [W.O.N.D. Dongle](https://gitlab.com/iShapeNoise/wond), welchen ich aus diesen Grund konzipiert habe.


### 4.2. Starte Troop <a name="runtroop"></a>

* Starte Supercollider und führe die Befehlszeile *FoxDot.start* aus (Quark "FoxDot" und "BatLib" sollte installiert sein. Check mit Befehlszeile *Quarks.gui*).
* Öffne ein Terminal und führe das Server-Skript aus. ***Nur der Teilnehmer*in, welche*r den Server starten wird***
* Öffne ein Terminal und führe das Klient-Skript aus. ***Inklusive Teilnehmer*in mit dem laufenden Serverskript***
* Das Client-Programm öffnet ein Popup-Fenster. Trage die IP-Adresse des Server Computers als Host (IP-Adresse), einen eigenen Namen (Dein Spitzname?) und Passwort ein. Auch das Kennwort wird die Serverperson bereitstellen.
* Nachdem du auf "OK" geklickt habst, sollte ein Fenster angezeigt werden, das genau wie der Texteditor der FoxDot-Benutzeroberfläche aussieht.


#### Jamming:

*!Starte stets mit einer geringen Lautstärke, wenn du ein neues Instrument oder Sample einfügst und blende langsam den Sound mit amplify oder amp im Rhythmus ein!*

1. Beginne zunächst klein mit einem der von Ihnen ausgewählten Instrumente und/oder Samples mit *amp=0.1*. Dann erhöhe die Lautstärke.
2. Wenn jemand anderes das Instrument oder das Sample bereits verwendet, verwende die nächste in deiner persönlichen Liste.
3. Bleibe bei diesem einen Instrument / Sample und probiere verschiedene Effekte und Attribute damit aus.
4. Nimm dir Zeit, dies kann zunächst chaotisch sein. Einfach Ruhe bewahren!
5. Ändere auch die Codezeilen der anderen, dadurch lernen alle voneinander.
6. Man kann den Code zwischenspeichern, um Snapshots zu bewahren.
7. Mache nach 10 - 30 Minuten eine Pause, um deine Ohren zu erholen und/oder noch einmal zu starten.
8. Und nicht vergessen: Im kollaborativen Musizieren geht es um die gemeinsame Freude am Musizieren.


## Ende Teil 1

### Ich hoffe, der Teil 1 dieses Kurses hat dir gefallen. Im zweiten Teil dieses Kurses tauchst du tiefer in die Musiktheorie, lernst mehr vom Komponieren in FoxDot, dem Erzeugen eigener Synthesizer in SuperCollider, dem Anschließen externer Programme und Geräte ... und vieles mehr.

***Vielen Dank, dass du die Zeit genommen hast. Feedback, Verbesserungsvorschläge und Kommentare sind sehr willkommen!***

#### contact@jensmeisner.net
