# FoxDot_CodingMusic_Part1


<img src="images/FoxDot_CodingMusic_FeatureImg.png" width="960"/>

## Description

FoxDot is a simple, Python-based interactive programming environment. It communicates with SuperCollider, a powerful audio synthesis for making music. FoxDot organizes musical events in a user-friendly and easy-to-understand manner. This brings fun to making music with LiveCode, for both novice programmers and veterans alike.

The workshop guides you through the basics of FoxDot. You can then immediately start making music alone or with other people. The FoxDot environment can also be used in the traditional way for writing musical compositions.

3 virtual rehearsal rooms are available at [pitchglitch](https://pitchglitch.cc/).

---

## Index <a name="index"></a>

[Preparation](#preparation)

1. [An Introduction](#introduction)

   1.1. [What is live coding?](#livecoding)

   1.2. [Why using code to make music?](#whycode)

   1.3. [What is FoxDot?](#whatfoxdot)

2. [First steps in FoxDot](#firststeps)

   2.1. [Player objects](#players)

   2.2. [Patterns](#patterns)

   2.3. [TimeVars](#timevars)

   2.4. [Player "play" und "loop"](#play&loop)

   2.5. [Groups](#groups)

3. [A little dive into music theory](#musictheory)

   3.1. [Song structure](#songstructure)

   3.2. [Chords and notes](#chords&notes)

   3.3. [Scales and modes](#scales)

   3.4. [BeatBox](#beatbox)

4. [Live Jam](#livejam)

   4.1. [Connect to Wifi](#wifi)

   4.2. [Start Troop](#runtroop)


---

## Preparation <a name="preparation">

For the course we need various software installed. These instructions are intended to help you to install the software on various platforms yourself.

### SuperCollider

SuperCollider is the basis that we need to be able to use the "FoxDot" platform at all. It is the computer language with which the instruments that we will play in the course have been programmed. It also installs the audio server that produces the actual sound.

| Operating system | Instructions |
|----------------|-----------------------|
| Windows  |  Install [SuperCollider 3.10.4 32bit](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4_Release-x86-VS-95e9507.exe) or [SuperCollider 3.10.4 64bit](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4_Release-x64-VS-95e9507.exe)|
| Mac   |  Install [SuperCollider 3.10.4](https://github.com/supercollider/supercollider/releases/download/Version-3.10.4/SuperCollider-3.10.4-macOS-signed.zip) |
| Linux   | 1. Update the system in the terminal:<br>***sudo apt update***<br>2. Install Linux Package manager<br>***sudo apt-get install synaptic***<br>3. Start the program with:<br>***sudo synaptic***<br>4. Find, activate und install **supercollider**. |


### sc-plugins

In order to get even more out of the software and also later from FoxDot, we also have to install all the necessary plug-ins. These come in the form of a complete package, which we will copy into a specific directory.


| Operating system | Instructions |
|----------------|-----------------------|
| Windows / Mac   | 1. Download [sc-plugins](https://github.com/supercollider/sc3-plugins/releases/tag/Version-3.10.0-rc1) and unzip the archive in the download directory <br>2. Start the SuperCollider program and enter the following line to specify the path to the extension folder:<br>***Platform.userExtensionDir***<br>3. To read code in SuperCollider, place the cursor on the line of code and press Cmd + Enter (macOS) or Ctrl + Enter (Win).<br>4. Move the entire sc-plugin directory into this extension folder |
| Linux   |  1. Start the package manager with:<br>***sudo synaptic***<br>2. Find, activate und install **sc-plugins**.  |


### Python 3

FoxDot is written in Python, an easy-to-learn programming language. To install and start FoxDot, Python3 must be installed. It is possible that it is already part of the operating system as a standard (e.g. some Linux versions).


| Operation system | Instructions |
|----------------|-----------------------|
| Windows | Download and install [Python 3](https://www.python.org/downloads/windows/) |
| Mac | Download and install [Python 3](https://www.python.org/downloads/mac-osx/) |
| Linux   |  1. Start the program manager in the terminal with:<br>***sudo synaptic***<br>2. Find, activate und install **python3**.  |


### pip

pip is a library installer for Python and makes it easier to install extensions and programs for / from Python.

| Operation system | Instructions |
|----------------|-----------------------|
| Windows   | 1. Download [get-pip.py](https://bootstrap.pypa.io/get-pip.py) and save the file where Python3 is installed.<br><img src="images/pip_install_1_win.jpg" width="512"/><br>2. Open a terminal and run the following commands:<br>***cd Path_to_install_Python3***<br>***python3 get-pip.py***<br>3. After the installation, you can check the installed version with the following command:<br>***pip3 --version***|
| Mac   | 1. Download [get-pip.py](https://bootstrap.pypa.io/get-pip.py) and save the file where Python3 is installed. <br> 2. Open a terminal and run the following commands:<br>***cd Path_to_install_Python3***<br>***python3 get-pip.py***<br>3. After the installation, you can check the installed version with the following command:<br>***pip3 --version*** |
| Linux   |  1. Start the program manager in the terminal with:<br>***sudo synaptic***<br>2. Find, activate and install **python3-pip**.   |

### FoxDot

Now that we have pip installed, the following steps apply to any operating system.

1. Open a terminal on your computer and run the following command lines:

  > pip install python3-tk

  > pip install FoxDot

2. Start SuperCollider and install FoxDot necessary modules in SuperCollider with the following command line:

  > Quarks.install("FoxDot")

  > Quarks.install("SafetyNet")

**As before, go with your cursor over the respective line and press Ctrl + Enter (Cmd + Enter) to trigger the command**

*Hint: There is a graphical window version for installing Quark elements. Use the command line for this:*

> Quarks.gui


### git

Git is free software for the distributed version management of files, which was initiated by Linus Torvalds. We will use this to download Troop. This can also be used directly for special versions for FoxDot.


| Operating system | Instructions |
|----------------|-----------------------|
| Windows | Download and install [Git](https://git-scm.com/download/win) |
| Mac | Download and install [Python 3](https://sourceforge.net/projects/git-osx-installer/files/) |
| Linux   |  1. Start the program manager in the terminal with:<br>***sudo synaptic***<br>2. Find, activate and install **git**.  |


### Troop

Now that we've installed git, the following steps apply to any operating system.

1. Open a terminal and clone Troop to your computer with the following command:

> git clone https://github.com/Qirky/Troop

2. To run Troop, enter the following commands within the Troop directory.

* For the server computer:

  >python3 run-server.py

* For the client computer:

  >python3 run-client.py


#### Hope it all worked out. Here we go!


---

#### [Back to Index](#index)

## 1. An Introduction <a name="introduction">

### 1.1. What is Live Coding? <a name="livecoding"></a>

* Interactive programming as an art performance, like music or video art
>“Live Coding is a new direction in electronic music and video, and is getting somewhere interesting. Live Coders exposes and rewire the innards of software while it generates improvised music.” - toplap.org
* Using code to describe rules for an art piece
* Live notation / composition as performance
* Code can be changed and re-executed in real-time, while the program is running (compose music while performing)
* Takes computer language into a social environment, thus making coding to a social activity

<img src="images/foxdot_workshop_1.jpg" width="960"/>


### 1.2. Why using code for music? <a name="whycode"></a>

* Classical music with notation on sheets is already a code to write musical pieces
* Pitch, duration, loudness in sheet music is a code, that can be read by musicians
* With Live coding, you can:
  * flexible describe rules
  * hack the code without an UI
  * interact with your composition, while it is playing
  * operate on the edge of liveness

<img src="images/foxdot_workshop_2.jpg" width="960"/>


### 1.3. What is FoxDot? <a name="whatfoxdot"></a>

* Developer: Ryan Kirkbride, Leeds UK
* FoxDot is a Python package that comes with its own IDE and plays music by accessing any SynthDefs stored on a local SuperCollider server with some custom bits of syntax to boot.
* Python is a user-friendly object-oriented and near human-readable programming language.
* SuperCollider is a programming language originally released in 1996 by James McCartney for real-time audio synthesis and algorithmic compositions, that runs underneath the FoxDot environment.
* Live-Coding with Python via FoxDot offers accessible states through its reactive and dynamic objects.
* FoxDot focuses on musical patterns, not the digital signal processing (DSP), which is programmed by SuperCollider and controlled via OSC (OpenSoundControl).
* FoxDot has a clean syntax, that is easy to read, so the code can be understood by the audience and traditional musicians.
* System diagram (Schematic) shows what FoxDot does:

  <img src="images/foxdot_workshop_3.jpg" width="960"/><br>

  - FoxDot is a python library, which can be used in own Python projects, or via Tkinter IDE.
  - It is also possible to configure other IDE’s to work with FoxDotn.
  - The music-making objects can be schedule events, like Timing, States, and Patterns.
  - SuperCollider provides SynthDefs (Instruments/FXs) and PBinds (Patterns/Sequences), that can be addressed and managed via OSC.

#### An extension to FoxDot is Troop
  - Allows collaborative music-making with FoxDot.
  - Contains a server and client script written in Python.
  - It can connect several computers to one sound pc, that runs the SuperCollider audio server and does all the work of digital signal processing for FoxDot.
  - With this many people can perform one musical piece, and interact with the code written by others in the group.


  <img src="images/foxdot_workshop_4.jpg" width="960"/><br>

---

#### [Back to Index](#index)

### 2. First steps in FoxDot <a name="firststeps"></a>

Let's learn the basic functions of the editor step by step.
First, you need to start FoxDot.

1. Supercollider-IDE ausführen
2. Type in FoxDot.start and press CTRL + ENTER
>The audio server should start, and all synths and effects got loaded into the memory
>Now Supercollider is listening to the messages coming from FoxDot
3. Open a terminal and move into your project folder
4. Now execute “python3 -m FoxDot” or “python -m FoxDot”
>The GUI Editor of FoxDot should open

#### Exercise:
*Go through the menu and check out all the options. Find and activate:*

> Toogle Beat Counter

*and*

> Use SC3-Plugins

The beat counter shows the beat over 4 bar units at the bottom left of the program. It's like a visual metronome.
The second option enables all plug-ins. This e.g. gets the piano to work.

The editor is interactive, so if you have the cursor over a line or gap-less block, or you select a block by click LMB and dragging the mouse to select it, or you use SHIFT + Arrows, you will be able to execute the code in real-time by pressing CTRL + ENTER.

#### Exercise:
*Let's try a little python code to learn how to use it!*

Enter the following line in the text part of the editor and press Ctrl / Cmd / Ctrl + Enter while the cursor is positioned on this line.

    2 + 2

The output of an executed code is displayed in the console in the lower window of the program.
The console displays the line you entered. Use the Python function print () to display the result:

    print(2 + 2)

Now we're going to wrap the equation in a variable. We will use variables often. Write the 2 lines directly below each other so that it can be completely executed as a block:

    a = 2 + 2
    print(a)

Variables can also be combined:

    a = 2
    b = 3
    c = a + b
    print(c)

If you only want to execute one line within the block, move the cursor over the line and press Alt + Enter.

The general philosophy of FoxDot is to create “player” objects as simply as possible that take keyword arguments that mirror the SuperCollider Pbind-SynthDef relationship and schedule their actions on a globally accessible clock.

If you want to know more about a function or class – just type help followed by the name of that Python object in brackets:

    help(object)

A SynthDef is essentially your digital instrument and FoxDot creates players that use these instruments with your guidance.


#### [Back to Index](#index)

### 2.1. Player objects<a name="players"></a>

Foxdot has a number of different instruments available that you can use as player objects.

To have a look at the existing selection of FoxDot SynthDefs, just execute:

    print(SynthDefs)

Choose one and create a FoxDot player object using the double arrow syntax like in the example below. If my chosen SynthDef was “pluck” then I could create an object “p1”:

    p1 >> pluck()

To stop an individual player object, simply execute p1.stop(). To stop all player objects, you can press CTRL+., which is a shortcut for the command *Clock.clear()*.

The variable *p1* can consist of 2 letters or 1 letter + 1 number (e.g. pp or s1). You will find your own strategy in naming so that you avoid the double use of names.

The >> in Python is usually reserved for a type of operation, like + or -, but it is not the case in FoxDot.

If you now give your player object some arguments, you can change the notes being played back. The first argument, the note degree, doesn’t need explicit naming.

    s1 >> pluck([0, 2, 4])

Use var to control the trigger of each note player over time:

    s1 >>pluck(var([0, 2, 4], 4))

Beat 0 –> Note 0 >> Beat 4 –> Note 2 >> Beat 8 –> Note 4 >> Beat 12 –> Note 0 >> Beat 16 –> Note 2 >> Beat 20 –> Note 4 …

Use another list to give notes an explicit time:

    s1 >>pluck(var([0, 2, 4], [2, 2, 4]))

Now use labeled arguments to shape the way the instrument is played. In the following example the octave *oct* is increased (standard is 5), the note playing time *dur* (standard is 1) and the volume *amp* varies (standard is 1).

    s1 >> pluck([0, 2, 4], oct=6, dur=[1, 1/2, 1/2], amp=[1, 3/4, 3/4])

You can group notes and variables by enclosing multiple values of arguments in parentheses. In the following example we play 2 notes at the same time and expand the stereo effect in the *pan* attribute:

    s2 >> bass([(0, 9),(3, 7)], dur=4, pan=(-1, 1))

You can even have a player item follow another player. In the example s2 adds a triad to every bass note played by s1:

    s1 >> bass([0, 2, 3, 4], dur=4)
    s2 >> pluck(dur=1/2).follow(s2) + (0, 2, 4)

In addition to *.follow ()*, you can also use the *.degree* argument (without brackets) to follow other players:

    s3 >> pluck(s1.degree + 2)

This is also possible with all other arguments. For example *s1.oct*, *s1.dur* and so on.

#### Exercise:
*1. Use print(SynthDef) to see all available synthesizers and try them out.*
*2. Create a small bass line with 1-8 notes, chords with 1-8 chords, and a small melody.*
*3. Use some of the attributes: the octave variable oct =, the duration variable dur = and / or the amplitude gain value amplify = to get a better result!*

**Note: The standard octave in FoxDot is 5, which in conventional music theory is octave 3!**

In my example, I include 3 players to improve a full piano:

    p1 >> piano([0, 1, 0, -1], oct=4, dur=2, amplify=3/4)
    p2 >> piano([(2, 4), (0, 2), (3, 5), (1, 3), (2,4), (0,2), (-1,1), (-3,-1)], dur=1, amplify=2/3)
    p3 >> piano([0, 4, 2, 4, 1, 2, 1, 3, 2, 3, 5, 7, -1, 3, -3, 1], oct=6, dur=1/2).every(32, "reverse")

This is the same as:

    bassline = [0, 1, 0, -1]
    chords = [(2, 4), (0,2), (3, 5), (1, 3), (2, 4), (0, 2), (-1, 1), (-3, -1)]
    melody = [0, 4, 2, 4, 1, 2, 1, 3, 2, 3, 5, 7, -1, 3, -3, 1]
    p1 >> piano(bassline, oct=4, dur=2, amplify=3/4)
    p2 >> piano(chords, dur=1, amplify=2/3)
    p3 >> piano(melody, oct=6, dur=1/2).every(32, "reverse")

With print(Player.get_attributes ()) you get a list of the available attributes. Most of these attributes have a default value of 0 or 1.

**When you start trying out these values, use speakers instead of headphones to protect yourself from hearing damage**

Sound samples can also be played using the Sample Player object, which is created as follows:

    b1 >> play("x-o-")

Each character refers to a different audio file. To play samples at the same time, just create a new player object:

    b1 >> play("xxox")
    b2 >> play("---(-=)", pan=0.5)

The letters / characters of *b2* in round brackets shorten the spelling, which would otherwise be *“------- =”*. This saves a lot of space and leads to more complex ways of playing.

If you put characters in square brackets, they will be played back twice as fast. This means that the 2 characters in square brackets represent the same time of a single character:

    b3 >> play("x[--]o(=[-o])")

You can also use methods to vary the order of the audio files or notes played.

    b4 >> play(“----”, sample=-1).every(4, ”stutter”,3)

#### Exercise:
*Go through the characters and listen to the different examples available. Use the attribute sample=[:8]. The audio files or samples will be repeated if the character contains fewer than 9 samples (0 - 8 are 9 numbers) in the dedicated folder!*


| Name        | Letter/Character  |
|-------------|-------------------|
| Kick        | A v V x X W       |
| Snare/Rim   | D i I o O t u     |
| Hihat       | : = - a n N       |
| Clap/Snap   | * h H             |
| Cymbal/Crash| / # e E           |
| Tom/Tom-like| m M p P w         |
| Percussion  | & + d f l r R y   |
| SoundFX     | \ b F k L Q Y z Z |
| Voice       | 1 2 3 4 ! < ? c C |
| Bell        | T                 |
| Various     | $ ; B g G j J K q U |
| Noise       | @ %               |
| Shaker      | s S               |
| Ride        | ~                 |


#### [Back to Index](#index)

### 2.2. Patterns<a name="patterns"></a>

FoxDot uses a container type called a ‘Pattern’ to help solve this problem. They act like regular lists but any mathematical operation performed on it is done to each item in the list and done so pair-wise if using a second pattern.

In this section, we'll dig deeper into lists and patterns. The basic knowledge will open the door to a high degree of flexibility and offer you interesting design forms.

For example, try multiplying a list by two like this:

    print([1,  2,  3] * 2)

Console output >> [1, 2, 3, 1, 2, 3]

Does the result meet your expectations?

If you want to edit the internal values in Python you need to use a for loop:

    l = []
    for i in [1,  2,  3]:
          l.append(i * 2)
    print(l)

or in the list understanding:

    print([i*2 for i in [1,2,3]])

Console output >> [2, 4, 6]

But what if you want to multiply the values in a list by 2 and 3 alternately?

FoxDot uses a type of container called "Pattern" to solve this problem. They behave like regular lists, but any math operation performed on them is performed on each item in the list, and paired if a second pattern is used.

The basic pattern can be created as follows:

    print(P[1, 2, 3] * 2)

Console output >> P[2, 4, 6]

    print(P[1, 2, 3] + [3, 4])

Console output >> P[4, 6, 6, 5, 5, 7]

Notice how in the second operation the output is any combination of the two patterns >> [1 + 3, 2 + 4, 3 + 3, 1 + 4, 2 + 3, 3 + 4].

#### Exercise:
*Try some other math operators and see what results you get!*

What if you group numbers in brackets like P [1,2,3] * (1,2)?

    P[P(1, 2), P(2, 4), P(3, 6)]

There are several other pattern classes in FoxDot that you can use to generate arrays of numbers, but they behave just like the base pattern.

    print(classes(Patterns.Sequences))
    print(classes(Patterns))

In Python you can use the syntax area (start, stop, step) to generate a range of integers. By default, Start is 0 and Step 1.

With *PRange (start, stop, step)* you can create a sample object with the appropriate values. The first example shows the equivalent function in Python, the second is the simplified sample function in FoxDot *PRange*:

    print(list(range(10)))

Console output >> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


    print(PRange(10))

Console output >> P[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


    print(PRange(10) * [1, 2])

Console output >> P[0, 2, 2, 6, 4, 10, 6, 14, 8, 18]

But what about combining patterns? In Python you can concatenate (append) two lists with the + operator. However, FoxDot patterns use this to supplement the data in the list. To connect two Pattern objects together, you can use the pipe symbol, which Linux users may be familiar with. It is used to connect command line programs by sending the output of one process as input to another.

    print(PRange(4) | [1,7,6])

Console output >> P[0, 1, 2, 3, 1, 7, 6]

There are several types of pattern sequences in FoxDot (and the list is still growing) that make generating these numbers a little easier. For example, to play the first octave of a pentatonic scale from bottom to top and back again, you can use two PRange objects:

    p1 >> pluck(PRange(5) | PRange(5,0,-1), scale=Scale.default.pentatonic)

The PTri class does this for you:

    p1 >> pluck(PTri(5), scale=Scale.default.pentatonic)


#### Exercise:
*Try the examples below to get an insight into the possible sample functions!*


#### Pattern functions

There are several functions that generate a pattern of values for us to do useful things in FoxDot, such as: B. Rhythms and melodies. This section is a list of pattern functions with descriptions and examples.

Used as input arguments for FoxDot players, these can themselves be treated as patterns and their methods applied directly, e.g. B. *PDur (3, 8).reverse ()*. You can also replace each input argument with a pattern or a TimeVar function to create an advanced pattern or a Pvar pattern. Let's look at some examples:

**PStep(n, value, default=0)** >> Returns a pattern where every n-term is 'value', otherwise 'standard'.

    s1 >> varsaw(PStep(3, [0, 2, 1, 4, 2, 5], [-2, [-2, -1]]), oct=(4, 6), dur=1/4, sus=1/8, lpf=linvar([200, 4000], 8), amplify=3/4)

**PSum(n, total, **kwargs)** >> Returns a pattern of length 'n', the sum of which is 'total'. For example: PSum(3,8) -> P[3, 3, 2] PSum(5,4) -> P[1, 0.75, 0.75, 0.75, 0.75].

    s1 >> donk(P[:2], oct=[[5, 6], 6], dur=PSum(12, 8), sus=1/2, amplify=2/4)

**PRange(start,stop=None,step=None)** >> Returns a pattern equivalent to Pattern(range (start, stop, step)).

    s1 >> piano([0, 2, 0, 1], oct=4, dur=2, sus=1, amplify=3/5)
    s2 >> piano(Pvar([[0, 2, 4, 2], [0, 4, 2, 1], PRange(0, 8, var([2, 1], 4))], [4, 4, 8]), dur=Pvar([1/2, PDur([3, 5], 8)], [1, 3]), amplify=5/7)

**PTri(start, stop=None, step=None)** >> Returns a pattern equivalent to Pattern(range (start, stop, step)) with the inverted shape appended.

    s1 >> piano([0, 2, 0, 1], oct=4, dur=2, sus=1, amplify=3/5)
    s2 >> piano(Pvar([[0, 2, 4, 2], [0, 4, 2, 1], PTri(0, 8, var([2, 1], 4))], [4, 4, 8]), dur=Pvar([1/2, PDur([3, 5], 8)], [1, 3]), amplify=5/7)

**PEuclid(n, k)** >> Returns the Euclidean rhythm that distributes 'n' pulses as evenly as possible over 'k' steps. e.g. PEuclid(3, 8) returns P[1, 0, 0, 1, 0, 0, 1, 0].

    s1 >> blip(Pvar([P[:2], P[:3]], 16), oct=4, dur=1/2, amplify=3/7*PEuclid([3, 5, 5, 3], [7, 8]))


**PSine(n=16)** >> Returns values of one cycle of a sine wave divided into 'n' parts.

    s1 >> fuzz(PSine(8), dur=1/2, sus=1/4, formant=1, room=1/2, mix=1/3, pan=PSine(32), amplify=2/3)

**PDur(n, k, dur=0.25)** >> Returns the actual duration based on Euclidean rhythms (see PEuclid), where dur is the length of each step. e.g. PDur(3, 8) returns P[0.75, 0.75, 0.5].

    s1 >> bass(PWalk(3), oct=5, dur=Pvar([PDur(5, 7), PDur(5, 8)], 16), amplify=2/5)
    s2 >> pulse(Pvar([P[:3], P[:2]], 8), oct=5, dur=PDur(2, 3), sus=1/8, lpf=expvar([400, 4000], 16), lpr=3/4, amplify=4/7, amp=P10(16))

**PBern(size=16, ratio=0.5)** >> Returns a pattern of ones and zeros based on the ratio value (between 0 and 1). This is known as the Bernoulli sequence.

    b1 >> play("S", sample=[1, 3], amplify=2/3, amp=PBern(16, 1/2))
    b2 >> play("S", dur=PBern(24, 1/2), delay=[0, 1/2], sample=5, amplify=2/3, amp=1)

**PBeat(string, start=0, dur=0.5)** >> Returns a pattern of durations based on an input string, where non-spaces denote a pulse.

    s1 >> donk(dur=PBeat(". . . ..", start=0, dur=[1] + [1/2] + [1] + [1/2] * 2), amplify=2/3)
    s2 >> bell(dur=PBeat(".   . .   ..", start=0, dur=1/2), amplify=1/3)

**PSq(a=1, b=2, c=3)**

    s1 >> piano(PSq(1, 2, 3) - var([0, P[:2] * 2], [4, 8]), amplify=2/3)
    print(PSq(1, 2, 3))


#### Pattern generators

We know that patterns have a fixed length and can be generated based on a function. However, sometimes it is useful to have patterns of infinite length, for example when generating random numbers. This is where pattern generators come into play. Similar to Python generators where not all values are kept in memory at once, except when Python generators usually have an end - FoxDot pattern generators don't!

**PRand(lo,hi,seed=None)/PRand([values])** >> Returns a series of random integers between lo and hi, inclusive. If hi is omitted, the range is 0 to lo. A list of values can be provided in place of the range and PRand returns a series of values chosen at random from the list.

    var.ch1 = var([PRand([0, 2, 4, 8], seed=PxRand(200))], 4)
    var.ch2 = var([PRand([0, 1, 3, 5], seed=PxRand(200))], [8, 4, 4])
    s1 >> piano([var.ch1, var.ch2], dur=1/2, amplify=2/3)

**PxRand(lo, hi) / PxRand([values])** >> Identical to PRand, but no elements are repeated.

    s1 >> pluck(PWalk(4), dur=PxRand([2, 2/3, 2/3, 1/3, 1, 1, 1/2, 1/2, 3/4]), oct=6, formant=3, tremolo=3, room=2/3, mix=1/3, amplify=2/3)

**PwRand([values], [weights])** >> Uses a list of weights to indicate how often items with the same index are selected from the list of values.
A weight of 2 means it is twice as likely to be picked as an item weighing 1.

    s1 >> sitar(PWalk(4), dur=PwRand([2, 2/3, 1/3, 1, 1/2, 3/4, 1/4], [2, 4, 5, 3, 7, 6, 1]), oct=PwRand([6, 6, 7, 5], [4, 3, 2, 1]), room=2/3, mix=1/2, amplify=2/3)

**P10(n)**>> Returns a pattern of length n of a randomly generated series of ones and zeros.

    s1 >> pulse(Pvar([[0, 1], [0, 2]], 16), oct=4, dur=2, sus=1, amplify=3/4)
    s2 >> pulse(P[:4], dur=1/2, sus=1/4, amplify=3/4, amp=P10(16))

**PAlt(pat1, pat2, *patN)** >> Returns a pattern generated by alternating the values in the specified sequences.

0, -2, 0, 8, 2, 1, 0, 9, 4, 3, 7, 0, -2, 0, 5 ...

    mtf1 = [0, 2, 4]
    mtf2 = [-2, 1, 3]
    mtf3 = [0, 0, 2]
    s1 >> piano(PAlt(mtf1, mtf2, mtf3, [8, 9, 7, 5]), dur=1/2)

**PJoin(patterns)** >> Assembles a list of patterns.

    mtf1 = [0, 2, 6, 4]
    mtf2 = [1, 3, 7, 5]
    s1 >> arpy(Pvar([mtf1, mtf2, mtf1, PJoin([mtf1, mtf2])], 8), oct=5, dur=1/2, formant=3, room=1/2, mix=1/3)

**PPairs(seq,func=<lambda>)** >> Links a sequence to a second sequence obtained by executing a function on the original. By default, this is lambda n: 8 - n.

    s1 >> sitar(PPairs([0, 4, 2, 0, 6, 4], lambda n: var([n*3, n-1], [12, 4])), oct=4, dur=1/2, amplify=3/7)

**PQuicken(dur=0.5, stepsize=3, steps=6)** >> Returns a group of delay amounts that gradually decrease.

    b1 >> play("m", dur=1, delay=[PQuicken(dur=2, stepsize=2, steps=3), PQuicken(dur=2, stepsize=2, steps=6)], sus=1/8, amplify=2/5)
    b2 >> play("t", dur=4, delay=PQuicken(dur=1, stepsize=4, steps=3), sample=2, amplify=3/5)
    b3 >> play("S", dur=4, delay=2 + PQuicken(dur=1/2, stepsize=2, steps=3), amplify=2/3)

**PRhythm(durations)** >> Converts all tuples / PGroups into delays, which are calculated with the PDur algorithm.

    b1 >> play("V", dur=PRhythm([0, 1/2, 0, 1/4, 1, 3/4]), delay=0, sample=12, amplify=2/3)

**PShuf(seq)** >> Returns a mixed version of seq. This example uses a function to automatically shuffle the list.

    def updateShuffle(n=0):
        beats=32
        if n % beats == 0:
            var.mtf = var([PShuf([0, 1, 3, 4, -1])], 1)
        Clock.future(1, updateShuffle, args=(n+1,))
    updateShuffle()
    s1 >> ambi(var.mtf, oct=(5, 6), dur=1, sus=1/4, echo=[0, 1/2], echotime=2, room=2/3, mix=1/3, amplify=1/2)

**PStretch(seq, size)** >> Returns 'seq' as a pattern and is looped until its length is 'size', e.g. PStretch ([0,1,2], 5) returns P [0, 1, 2, 0, 1].

    var.mtf1 = var([0, 1, 2, 4, [3, 5], 0, 2, 4], 1/2)
    s1 >> karp(PStretch(var.mtf1, 12), oct=6, dur=[1/2, 2/3], shape=1/8, formant=0, rate=1/8, amplify=2/3)

**PStrum(n=4)**

    var.mtf1 = var([0, 1, 2, 0, [4, 2], 3, -2, [-1, 4]], 1/2)
    s1 >> marimba(var.mtf1, oct=var([5, 6], [1/2, 3/2]), dur=Pvar([PStrum(5), PStrum(2)], 16), shape=1/4, room=1/2, mix=1/2, amplify=1)

**PStutter(seq, n=2)** >> Creates a pattern so that each element in the array is repeated n times (n can be a pattern).

    var.mtf1 = var([0, 6, 4, 2], 2)
    s1 >> quin(PStutter([var.mtf1], 2), oct=4, dur=PStutter([1, 1/2], 4), sus=1/4, amplify=2/3)

**PZip(pat1, pat2, patN)** >> Generates a pattern that 'zips' multiple patterns. PZip([0,1,2], [3,4]) creates the pattern P[(0, 3), (1, 4), (2, 3), (0, 4), (1, 3), (2, 4)].

    s1 >> faim(PZip([0, 2], [2, -2, 4, 6]), oct=6, dur=2, atk=1/6, chop=2, lpf=1800, vib=2, amplify=1/2)

**PZip2(pat1, pat2, rule=<lambda>)** >> Like PZip, but only uses two patterns. Connects values if they meet the rule.

    s1 >> faim(PZip2([0, 2], [2, -2, 4, 6], rule = <lambda>), oct=6, dur=2, atk=1/6, chop=2, lpf=1800, vib=2, amplify=1/2)

**Pvar** >> TimeVar, which saves lists instead of individual values (var, sinvar, linvar, expvar).

    s1 >> gong(P[Pvar([[0, 2], [2, 4], [4, 6], [2, 4]], 2)], dur=1/2, lpf=expvar([800, 8000], [4, 0]), pan=sinvar([-2/3, 2/3], 8), amplify=4/5)

**PWhite(lo, hi)** >> Returns random floating point numbers between lo and hi.

    s1 >> arpy((0, var(PRand([Scale.default]), 8)), oct=var([5, 6], [24, 8]), dur=PDur(5, 8), room=1/2, mix=sinvar(1/3, 3/4), pan=PWhite(-1, 1), amplify=2/3)

**PChain(mapping_dictionary)** >> Based on a simple Markov chain with equal probabilities. Takes a dictionary of elements, states, and possible future states. Every future state has an equal chance of being selected. If a possible future state is not valid, a KeyError is raised.

    s1 >> rave(PChain([0, 8, 6, 3, -2, 0, -3]), dur=1/4, sus=1/8, amplify=1/2)

**PWalk(max = 7, step = 1, start = 0)** >> Returns a series of integers with each element an increment apart and with a value in the range of +/- the maximum. The first element can be selected with start.

    s1 >> dirt(PWalk(6, 2), dur=[1/2, PSum(4, 3)], oct=6, shape=1/3, lpf=1800, pan=(-2/3, 2/3), amplify=1/4)

**PFibMod()** >> Returns the Fibonacci sequence.

    s1 >> feel(PFibMod()[:7] + var([0, -3, 0], 8), dur=1, shape=1/4, chop=128, room=3/4, mix=1/2)


#### [Back to Index](#index)

### 2.3. TimeVars <a name="timevars"></a>

A TimeVar is an abbreviation of “Time Dependent Variable” and is a key feature of FoxDot.

A TimeVar has a series of values that it changes between after a pre-defined number of beats and is created using a var object with the syntax var([list_of_values],[list_of_durations]).

Example:

The duration can be a single value

    a = var([0, 3], 4)

'a' initially has the value 0

    print(int(Clock.now()), a)

Console output >> 0, 0

After 4 hits the value changes to 3:

    print(int(Clock.now()), a)

Console output >> 4, 3

After another 4 hits, the value changes to 0:

    print(int(Clock.now()), a)

Console output >> 8, 0

When a TimeVar is used in a math operation, the affected values also become TimeVars, which change state when the original TimeVar changes state. This can even be used with patterns:

Beat is 0:

    a = var([0, 3], 4)
    print(a + 5)

Console output >> 5

Beat is 4:

    print(a + 5)  

Console output >> 8

Beat is 8 and a has a value of 0 again:

    b = PRange(4) + a
    print(b)

Console output >> P[0, 1, 2, 3]

Beat is 12 and has a value of 3:

    print(b)

Console output >> P[3, 4, 5, 6]


### 2.3.1. Types of TimeVar

**linvar(values, dur)** >> This TimeVar alternates between values on a linear scale, hence the name linvar. As with all TimeVars, it takes a number of values and durations as input. If dur is omitted, the value of the current meter is used.

Over time, the value changes so that it is exactly 1 after the specified duration (in this example 4 beats). After this period has elapsed, the Linvar begins to change its value linearly in the direction of the next input value. this is 0. During the next 4 beats, the value decreases linearly in the direction of 0.

**sinvar(values, dur)** >> Instead of changing between values at a linear rate, a sinvar changes at a rate derived from a sine wave.


**expvar(values, dur)** >> The rate of change of the expvar is exponential, so it starts small but ends in big steps.

Example with linvar () type:

    s1 >> donk(P[:2], oct=[[5, 6], 6], dur=PSum(12, 8), sus=1/2, amplify=linvar([0, 1], 4))


### 2.3.2. Usage

It should be noted that when a player object uses a Gradually changing TimeVar function, the value stored in it will be used at the time the note was triggered. This means that after playing a note you will not hear a change in value over time in the note itself. Try these lines of code for yourself:

No gradual change in high pass frequency:

    p1 >> dirt(dur=4, hpf=linvar([0, 4000], 4))

Apparent gradual change in high pass frequency:

    p2 >> dirt(dur=1/4, hpf=linvar([0, 4000], 4))

You can also use a duration of 0 to immediately skip the gradual change and move on to the next value. This is useful for "resetting" values and creating drops.

Raise the high pass frequency filter to 4000Hz, then back to 0:

    p1 >> dirt(dur=1/4, hpf=expvar([0, 4000], [8, 0]))

As with normal TimeVars functions, TimeVars can be nested within other TimeVars as they gradually change to better manage the application of the values. For example, we can only increase the high pass filter frequency on the last 4 beats of a 32 beat cycle as follows.

Use a normal TimeVar function to set the value to 0 for 28 beats:

    p1 >> dirt(dur=1/4, hpf=var([0, expvar([0, 4000], [4, 0])], [28, 4]))


### 2.3.3. TimeVars as Patterns

**Pvar(patterns, dur)** >> So far we have only saved individual values in a TimeVar, but sometimes it makes sense to save an entire Pattern object. You cannot do this with a regular TimeVar because each pattern in the input list of values is treated as a nested list of individual values. To avoid this behavior, you have to use a Pvar, short for Pattern-TimeVar (time variable pattern). It is created just like any other TimeVar, but values can be entire lists / patterns.

    a = Pvar([[0, 1, 2, 3], [4, 5, 6]], 4)
    print(Clock.now(), a)

Console output >> 0, P[0, 1, 2, 3]

You can even nest a pvar within a pattern like you would with a normal pattern to play alternate values.

Alternate the alternating notes every 8 beats:

    p1 >> pluck([0, 1, 2, Pvar([[4, 5, 6, 7], [11, 9]], 8)], dur=1/4, sus=1)


#### [Back to Index](#index)

### 2.4. Player Object “play” und “loop” <a name="play&loop"></a>

There are currently two special case synthesizers in FoxDot for playing back a saved audio file *play* and *loop*. We'll take a closer look at these in this section.

#### 2.4.1 The “play” object

Unlike other synthesizers in FoxDot, the first argument to "play" should be a string, not numbers. You have already used this in the upper part. As a result, more information can be encoded in the character string than the character itself means. Each character relates to a range of audio files such as kicks, hi-hats, snares, and other sounds.

The simplest drum pattern for disco is:

    b1 >> play("x-o-")

You can use different types of brackets to add more information to the sequence. If you put two or more characters in round brackets, the sound alternates with the new loop one after the other:

Simple pattern:

    b1 >> play("(x-)(-x)o-")

Nested brackets for more variety:

    b1 >> play("(x-)(-(xo))o-")

If you put several characters in square brackets, they will be played one after the other in one step:

Play a triplet in the fourth beat:

    b1 >> play("x-o[---]", dur=1)

You can also use the square brackets in the round brackets:

    b1 >> play("(x-)(-[-x])o-")

Within the square brackets, you can also use round brackets:

    b1 >> play("x-o[-(xo)]")

You can also use curly braces to randomly select a sample, adding variety to your sequence:

In the fourth beating step, choose a sample at random:

    b1 >> play("x-o{-ox}")

The curly brackets can also contain square brackets:

    b1 >> play("x-o{[--]ox}")

Or the curly brackets can be used within square brackets:

    b1 >> play("x-o[-{ox}]")

#### Attribute *sample*

Each character refers to audio files in a folder with the same name. The audio files are arranged in alphabetical order. Use the sample attribute to select an audio file in this folder.

    b1 >> play("x-o-", sample=1)

As with any other argument, this can be a list (one at a time) or even a tuple (simultaneously) of values.

    p1 >> play("x-o-", sample=[0, 1, 2])

    p1 >> play("x-o-", sample=(0, 3))

The example for a single character can be given within the character string itself by surrounding the character with a "|" + the position number:

Play sample = 2 for the letter 'o':

    p1 >> play("x-|o2|-")

This will overwrite the specified value under sample:

    p1 >> play("x-|o2|-", sample=3)

This syntax can contain any of the parentheses previously used for the character and numbers:

Change the sample number:

    p1 >> play("x-|o(12)|-")

Change the sign:

    p1 >> play("x-|(o*)2)|-")

Play several different samples in one step:

    p1 >> play("x-|o[23]|-")

Play a random sample:

    b1 >> play("x-|o{1[23]}|-")


#### Layering sequences

You can also use less than and greater than signs to layer multiple sequences simultaneously. Let’s start with two separate sequences and then put them together in a single line of code.

    b1 >> play("x-o-")
    b2 >> play("..+.+.[.+]")

***Note: The dot is equivalent to the space. In my opinion, the point helps to better recognize the temporal positioning.***

We can place any sequence between "<>" characters in a single sequence and have them play at the same time:

    b1 >> play("<x-o-><..+.+.[.+]>")

This is equivalent to:

    b1 >> play(P["x-o-"].zip(P["..+.+.[.+]"]))

*Zip can be understood as a zipper.*

Each ‘layer’ relates to the index of a group such that, for a group of values given to a player object, each ‘layer’ is affected only by one of those given values. This is best demonstrated by an example:

Pan each sequence hard on the left and right channels using square brackets in the *pan* attribute:

    b1 >> play("<x-o-><..+.+.[.+]>", pan=[-1, 1])

Expand the stereo effect by using round brackets:

    b1 >> play("<x-o-><..+.+.[.+]>", pan=(-1, 1))

Change the audio file used in the first layer:

    b1 >> play("<x-o-><..+.+.[.+]>", sample=(2, 0))

Be careful when combining multiple layers with some functions like offadd as that creates new layers if they don’t exist, or affects them if they do.

The following code will only affect the second layer and, so, the first layer is unaffected:

    b1 >> play("<x-o-><..+.+.[.+]>", sample=(2, 0)).every(4, "sample.offadd", 2)

#### Exercise:

*Try to create a 16 beat rhythm with your preferred samples.
Use Clock.bpm=120 to change the beat per minutes, or speed of rhythm in time!*


#### 2.4.2. The “loop” synth

In this section we will start with an exercise right away.

#### Exercise:
*Search under [www.wavsource.com](https://www.wavsource.com/) or [www.findsounds.com](https://www.findsounds.com/) and for 2-3 short audio files. Voices, vocals, beat loops, instruments or ambient noise are best.*

The loop synth is designed to let you play longer audio files (>1 sec) and manipulate them. To get started, just supply the filename you want to play and the duration you want to play in beats:

    l1 >> loop("path/to/my/file.wav", dur=32)

You can put files in a special folder located in FoxDot/snd/_loop_ which can be opened by going to “Help & Settings” and then “Open Samples Folder” from the FoxDot menu. You don’t need to supply the full path (or extension) for files in this folder:

    l1 >> loop("my_file", dur=4)

To see all the files in this folder use print(Samples.loops). If you want to play with the play back order, you can supply a “position” argument after the file name that FoxDot will iterate through based on the duration.

Play the first 4 beats of audio in order:

    l1 >> loop("my_file", P[:4], dur=1)

Spiele die ersten 4 Schläge (Beats) in zufälliger Reihenfolge ab:

    l1 >> loop("my_file", P[:4].shuffle(), dur=1)

If you know the bpm of the audio file and wish to play it at the current tempo, you can supply the player with a tempo argument. For example, my_file could be a drum beat at 135 bpm but the current tempo is 120, I can fit the tempo of my_file to the clock like so:

First 4 beats in 1 beat steps:

    l1 >> loop("my_file", P[:4], dur=1, tempo=135)

First 4 beats in 1/2 beat steps:

    l1 >> loop("my_file", P[:8]/2, dur=1/2, tempo=135)

Time stretching the audio in this fashion will change the pitch, so if the audio is pitched, you may wish the time-stretch it without losing that information.This is possible using the striate. This cuts the file into lots of little segments and plays them back spread out over the course of the duration value – this will play the entire audio file. The larger the audio file, the larger the number you will probably want to use. Using the example above, you may want to use a striate value of 100-200 for a smoother playback:

Stretch the file using 100 segments:

    l1 >> loop("my_file", dur=4, striate=100)

Stretch it using 10 segments - listen to the difference:

    l1 >> loop("my_file", dur=4, striate=10)

**Please note that this does not currently work for time-stretching files to faster tempos!**


All FoxDot effects can be used with the loop synth, so experiment and find out what works best for your audio. Using *slide* with negative values can recreate the “DJ scratching” effect from old school hip-hop as it slows the playback rate to 0 then back again:

    l1 >> loop("my_file", P[:8]/2, dur=1/2, slide=[0,0,-2])

Spiele einen aufgenommenen Titel ab:

    p1 >> loop("/absolute_path_to_your_workshop_directory/125bpm_robgrace.wav", dur=32)

#### Exercise:
*Implement your audio files in a rhythm and / or use timevar functions and attributes to create an audio collage!*


#### [Back to Index](#index)

### 2.5. Groups <a name="groups"></a>

Groups are useful for controlling multiple player objects at the same time. A piano can consist of a bass line, chord line and melody line. Attributes such as volume can then be adjusted more easily. This is also useful if you want to arrange transitions with filter effects (e.g. high pass filters on the entire drum kit).

    s1 >> piano(Pvar([[0, 3, 7, -2, 0, 5], [3, 0, 7, 3, 0]], [12, 8]), oct=4, dur=PDur(3, 8), sus=var([s1.dur, s1.dur*2], [6, 2]), amplify=var([2/3, 1/2], 8), amp=1)
    s2 >> piano(Pvar([[2, 5], [0, 7]], 16),oct=var([5, 6], [6, 2]), dur=var([1, 2], 32), amplify=var([1/2, 2/3], 16), amp=1)
    s3 >> piano((s1.degree,note), oct=(4, 5), dur=var([PDur(3, 8), 1], PRand(8)), amplify=2/5, amp=1)
    gPiano = Group(s1, s2, s3)

To turn the piano down, just use:

    gPiano.amp = 0

Several group objects already exist in FoxDot for specific groups of player objects based on variable names ending with the suffix '_all'. So for every character, e.g. "s" there is a group called s_all, which contains s1, s2, s3, ..., s9. So if you organize your players by variable names, you can easily apply effects or stop them all at once:

    s1 >> pads([0, 4, -2, 3], dur=4)
    s2 >> pluck([0, 1, 3, 4], dur=1/4)

Use the group to apply the filter attribute to all player objects:

    s_all.hpf = 500

This is also useful for:

    s_all.amp = 0

With *.stop()* you can interrupt the entire group of players:

    s_all.stop()

With *.solo()* all other player objects are muted, i. H. only the player objects of this group can be heard:

    s_all.solo()

*.only()* stops all players who are not in the group:

    s_all.only()


---

#### [Back to Index](#index)

### 3. First, a little bit music theory <a name="musictheory"></a>

### 3.1. Song structure <a name="songstructure"></a>

A typical Pop Arrangement has Intro, Verses, Chorus, Bridge, Refrain, and Outro. There are different radiation of it, but that is the basics.

Common Structures for a song is as followed:

* Intro (4 Bars)
* 1. Verse (8 -16 Bars) + Prechorus (Optional)
* Chorus (8 - 16 Bars)
* 2. Verse (8 - 16 Bars) + Prechorus (Optional)
* Chorus (8-16 Bars)
* Outro (4 Bars)

Bars are 4 beats or beats. So 4 bars in FoxDot means 16 counted as beats.

Further structures, whereas A is Verse, B is Chorus, C is Bridge:

**ABABCA** >> Verse / Chorus / Verse / Chorus / Bridge / Chorus
**AABA** >> Verse / Verse / Bridge / Verse
**ABAB** >> Verse / Chorus / Verse / Chorus (simplified version of the ABABCB)
**AAA** >> Verse / Verse / Verse

The following is an example of a song structure in common electronic music:

| Intro | Break | Buildup   | Drop  | Break |  Buildup  | Drop  | Outro |
|-------|-------|-----------|-------|-------|-----------|-------|-------|
|16 Bars|16 Bars|4/8/16 Bars|16 Bars|16 Bars|4/8/16 Bars|16 Bars|16 Bars|


#### 3.1.1 Intro

* The intro is pretty much anything you want it to be.
* Many songs start with just the melody that is rising up.
* You can even create a melodic question that is answered by the rest of the song or something of the sort.
* The important thing is to not stay too long at the intro and make it tie in quickly.


#### 3.1.2. Break

* Less loud, less bass heavy, less instruments.
* This is used to break up what the listener has paid attention to. In electronic music, you usually take out the drums and add a rising sound to the next part.
* A bridge/break can be more powerful by adding new instruments or changing the key
* Try to keep this at 8 measures or less.
* The bridge is a departure from what we’ve heard in a song, previously.
* This goes for both the lyrics and the music.
* Lyrically it’s an opportunity for a new perspective.
* Musically, it’s a chance to offer the listener something they haven’t heard before to keep the song interesting.


#### 3.1.3. Buildup

* Goes usually from Break to Drop, can be even silence.
* It creates an emotional tension in the listener, which is then dissolved in the drop.


#### 3.1.4. Drop

* Loudest, most fun to listen to.
* The moment in a dance track when tension is released and the beat kicks in. This releases an enormous energy during a song's progression.
* After the momentum build, the pitch rising, the tension mounting, bigger, louder, until suddenly — the drop.


#### 3.1.5. Riser

* A Riser is just like a break except that it is arpeggiating or having some sort of buildup that is released with the next section coming in.
* Usually no beat and last 8 measures or 16.
* When the next part comes in, it will have a lot more energy and should be the climax of the piece.


#### 3.1.6. Outro

* This is used to resolve the song and come in for a smooth landing.
* Some song’s don’t have an outro and others have a long outro.
* You can also add a final sense by adding a Coda, or strong cadence at the end of your track.


#### [Back to Index](#index)

### 3.2. Chords and Notes <a name="chords&notes"></a>

#### 3.2.1. Chords

In music, a chord is the simultaneous sounding of at least three different tones that can be interpreted harmonically.

We can divide chords into different types depending on how many notes they have. We can have them in:

* Groups of two notes - called intervals or dyads
* Groups of three notes - known as triad chords
* Groups of four or more notes - usually called seventh chords or expanded chords

A triad occupies the root or first note of the scale, the third degree, and the fifth degree, with each interval being a third.

For example, the C minor scale has the notes C - D - Eb - F - G - Ab - B - C. Take the 1st, 3rd and 5th notes (C - Eb - G) to get a C- To form a minor triad.

A seventh chord uses the root, 3rd, 5th, and 7th degree degrees, so a Cmin7 chord would add the Bb (C - Eb - G - Bb).
C minor seventh chord.

Extended chords add the 9th, 11th, and 13th scales (the octaves of the 2nd, 4th, and 6th, respectively).


**Inversion**

If you have a chord where the lowest note is not the note the chord is named after, we call this a chord inversion. A chord inversion takes a different starting note (also called a bass note) and builds the chord from there. Chord inversions are mainly used to allow easier voice guidance through different chord progressions, especially in the bass.

Features of a chord inversion are:

* The root note is not in the bases.
* et a smooth dynamic by rearranging the chords through changing the octave of notes to align closer to first chord, thus in versing  the highest note to the bass note.
* Inversion of a 5th becomes a 4th and visa verse.
* Major 2nd inverted becomes a minor 7th and a minor 7th becomes a Major 2nd.
* Major 6th inverted becomes a minor 3rd and a minor 3rd becomes a Major 6th.

**Chord progression**

To create a nice sounding and interesting melody, you need to carefully choose how each note moves to the next note and how each note relates to the notes in its vicinity. Notes can't be too far apart, and usually you want the notes to stay within the key or related keys.

The same concept is used for harmony. Since a song usually consists of more than one chord, you need to relate each chord to the one before and after it in order for the harmonic movement to sound good and interesting. This is where a chord progression comes into play.

A chord progression is when several different chords are played one after the other.


**Dur <<>> Moll**

Major and minor form the two sides of the proverbial coin when it comes to defining the key of a song or composition.
Songs are in either a major or a minor key. Sometimes more complex songs or pieces contain modulations (key changes), and we can see both major and minor keys represented in a single work.
However, major and minor keys (and their correlating modes) cannot occur simultaneously, at least in tonal music.

Each piece or section of a piece must be either major OR minor. You can't be both. Major and minor songs are based on their respective scales (modes). This provides information about both the content of the melody and the harmony of a piece.

In other words, songs with a major key are selected from notes found in a particular seven-note major scale (like C major or F major, etc.). Songs tuned in minor are selected from seven-part minor scales (such as C minor or F minor, etc.). In the case of minor, however, there is a superordinate minor scale called the natural minor, as well as two variants, each called the harmonic and melodic minor.

In addition, major and minor chord progressions usually follow the primary cadences (harmonic touchstones) of the mode from which they are derived. Pieces tuned to major almost always end on a major “homebase” chord. This chord is usually referred to as "I" using Roman numerals.

The opposite is the case with songs in the minor key. Occasionally, however, classical pieces with a minor key surprise the listener by suddenly ending with a major third in the “homebase” or “I” chord. This unexpected switch gives the music a sudden boost. The classic term for this is Picardy third.


Create a minor from a major chord by lowering the 3rd, 6th and 7th degrees by one note.

minor:

| Moll | Dim | Dur | Moll | Moll | Dur | Dur |
|------|-----|-----|------|------|-----|-----|

Major:

| Dur  | Moll | Moll | Dur | Dur | Moll | Dim |
|------|------|------|-----|-----|------|-----|

Am Example:

| Am | B0 | C | Dm | Em | F | G |
|----|----|---|----|----|---|---|

Cm Example:

| Cm | D0 | D# | Fm | Gm | G# | A# |
|----|----|----|----|----|----|----|


* A minor scale can be achieved by lowering the 3rd, 6th and 7th major tones by one note.

    print(Scale.major)

Console output >> P[0, 2, 4, 5, 7, 9, 11]

    print(Scale.minor)

Console output >> P[0, 2, 3, 5, 7, 8, 10]

If you only want to change one chord to minor, lower the third note.

A melodic minor scale is created by raising the 6th and 7th notes of the minor scale.

    print(Scale.minor)

Console output >> P[0, 2, 3, 5, 7, 8, 10]

    print(Scale.melodicMinor)

Console output >> P[0, 2, 3, 5, 7, 9, 11]

Examples 7th minor scale of E (E3, F # 3, G3, A3, B3, C4, D4, E4)

* E3, G3, B3, D4 – m7 (add F#4 for m9)
* F#3, A3, C4, E4 – Dim7 (add G4 for Dim9)
* G3, B3, D4, F#4 – Maj7 (add A4 for Maj9)
* A3, C4, E4, G4 – m7 (add B4 for m9)
* B3, D4, F#4, A4 – m7 (add C5 for m9)
* C4, E4, C4, B4 – Maj7 (add D5 for Maj9)
* D4, F#4, A4, C5 – Major chord with minor7 – Dom7 (add E5 for Dom9)



#### Example: Billy Jean Intro (Michael Jackson)

* Scale: minor
* Root: E
* Chords:

| Em |F#m/E| Em7 | F#m/E |
|----|-----|----|-----|
|    | C#3 | D3 | C#3 |
| B2 | A2  | B2 | A2  |
| G2 | F#2 | G2 | F#2 |
| E2 | E2  | E2 | E2  |

The number after it refers to the octave. In FoxDot, the middle C = 5, so you always have to add 2 when composing from the sheet music.

Tempo:

    Clock.bpm = 117

Root E:

    Root.default = “E”

Scale to minor:

    Scale.default = Scale.minor

Chords in a list:

    chords=[(0, 2, 4), (0, 1, 3, 5), (0, 2, 4, 6), (0, 1, 3, 5)]

Player object:

    s1 >> pluck(chords,oct=3,dur=[3/2,5/2],sus=2)

Drums:

    b1 >> play("<V....V..V...[VV]V..><..o.><---->")


#### Exercise:
*Try to create your own example! You can also leave the guideline below and try your own piece!*

#### Bassline and chords of “Get Lucky” (Daft Punk)

Bass:

| B1 | D2 | F#2| E2 |
|----|----|----|----|

Chords:

| Bm | D | F#m| Em |
|----|----|----|----|

In the fourth chord there is a note borrowed from the neighbor F # m (circle of fifths):

| F#2 | A2 | C#3| B2 |
|----|----|----|----|
| D2 | F#2| A2 | G#2 (F#m chord key) |
| B1 | D2 | F#2| E2 |

As an extra, you can try to create a little variety using timevars:

Drop: Thinner no beats
Break: No voice
Buildup: Mix BreakNDrop

With 4 notes / chords played every 16 beats, the song structure is as follows:

| Intro | Break | Buildup   | Drop  | Break | Buildup   | Drop  | Outro |
|-------|-------|-----------|-------|-------|-----------|-------|-------|
|16 Beats|32 Beats|32 Beats|64 Bars|32 Bars|32 Bars|64 Bars|48 Bars|


#### 3.2.2. Melodies

#### Melodies – From chords to the melody

One way is to create a chord progression, and than to find the melody in the chords.

| E3 | D3 | F3 | E3 |
|----|----|----|----|
| C3 | B2 | D3 | C3 |
| A2 | G2 | A2 | G2 |

As mentioned before, the octave in FoxDot is 2 steps above the usual, middle C is 5 not 3.

Lets do the chords with 93 bpm with A as Root note and minor scale:

    Clock.bpm = 93
    Root.default=”A”
    Scale.default=Scale.minor
    chords = var([(0,2,4), (-1,1,3), (0,3,5), (-2,2,4)])

Chords:

    s1 >> swell(chords, oct=5, dur=4, sus=5)

Hit the drums can help find a good melody:

    b1 >> play("<X....X..X..[X.].X..><..o.><---->",sample=0)

The easiest way to start a melody is to take the highest notes of the chords.

However, you want to add some non-chord notes to your chord notes:

| E4 | F4 | D4 | F4 | D4 | G4 | E4 | D4 |
|----|----|----|----|----|----|----|----|

    seq=[4,5,3,5,3,6,4,3]
    s2 >> pulse(seq, oct=6, dur=[3, 1, 3, 3, 1, 1, 2, 2])


#### From the melody to the chords

In this example we start with a melody in order to get suitable chords from it, here the melody.

| A3 | B3 | C4 | B3 | E4 | F4 | C4 | G4 | E4 | D4 |
|----|----|----|----|----|----|----|----|----|----|

Let's set the tempo, the root and the scale:

    Clock.bpm = 93
    Root.default=”A”
    Scale.default=Scale.minor

Die basierende Melodie:

If you can't remember the numbers on the scale list, use print(Scale.minor).

    seq=[0, 1, 2, 1, 4, 5, 2, 6, 4, 3]

Synth:

    s1 >> saw(seq, dur=[2, 1, 1, 4, 3, 1, 1, 1, 1, 1], formant=4, amplify=2/5)

The available chords (with 7th) for the notes played in the melody are as follows:


| G4 | A4 | B4 | C5 | D5 | E5 | F5 |
|----|----|----|----|----|----|----|
| E4 | F4 | G4 | A4 | B4 | C5 | D5 |
| C4 | D4 | E4 | F4 | G4 | A4 | B4 |
| A3 | B3 | C4 | D4 | E4 | F4 | G4 |

Here is a good example of a trip-hop-like track:

| E4 | D4 | E4 | E4 |
|----|----|----|----|
| C4 | B3 | C4 | G4 |
| A3 | G3 | A3 | C4 |

Let's add the chords to the melody:

    chords = var([(0, 2, 4),(-1, 1, 3),(0, 2, 4),(-1, 6,4 )])
    s2 >> keys(chords, oct=4, dur=4, shape=3/5)

And a drum hit:

    b1 >> play("<X....X..X..[X.].X..><..o.><---→", sample=0)


**Add a counter melody (arpeggio)**

Let's keep it simple and use the chord notes to play with the chords.
With the counter melody we want to add a rhythm to the track.

As the 4th of the sequence in a 4-beat measure, we add the 2nd as shown here:

    chords = var([(0, 2, 4), (-1, 1, 3), (0, 2, 4), (-1, 4, 6)])

becomes

    seq2 = [0, 2, 4, 2, -1, 1, 3, 1, 0, 2, 4, 2, -1, 4, 6, 4]

Now let's add another instrument that plays the counter melody:

    s3 >> karp(seq2, dur=1)


#### From the chord progression to the bass lines

In the example below, the chord progression is based on A minor, while raising a root to a higher octave and lowering a root.

| G3 | G3 |    |    |
|----|----|----|----|
| E3 | F3 | G3 | G3 |
| C3 | D3 | E3 | E3 |
| A2 | B2 | C3 | B2 |

Let's set the tempo, root and scale:

    Clock.bpm = 128
    Root.default=”A”
    Scale.default=Scale.minor

    chords = var([(0, 2, 4, 6), (1, 1, 3, 6), (2, 4, 6), (1, 4, 6)])
    s1 >> prophet(chords, oct=4, dur=4, sus=4)

The safe case is to use chord root notes as bass notes and lower those notes in the octave:

| A1 | G1 | C2 | E2 |
|----|----|----|----|

    bass1 = [0, -1, 2, 4]

Another way to create a bass line is to find notes within the chords (although the 7th can be tricky).

| A1 | B1 | C2 | B1 |
|----|----|----|----|

You can also change the duration of the bass line to get a rhythmic component:

Use instead of *dur=4* >> *dur=1*, *dur=[1/2,1]*, or *dur=[1,2,1]*.

Another option is to move the root note of a chord one step up the previous chord row.

With dur = 1:

    bassline2=[0, 0, 0, -1, -1, -1, -1, 2, 2, 2, 2, -3, -3, -3, -3, 0]

Or you use octave jumps:

    bassline3=[0, 0, 0, 0, -1, -1, -1, -1, 2, 2, 2, 2, -3, -3, -3, -3]

with *dur=1* and *oct=[3, 3, 4, 3]*

Finally, a melody as a bass line:

| A1 | G1 | A1 | B1 | A1 | G1 | G1 | A1 | C2 | C2 | A1 | G1 | E1 | E1 | F1 | G1 |
|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|

    bassline4 = [0, -1, 0, 1, -1, -1, 0, 1, 3, 3, 0, -1, -3, -3, -2, -1]


#### From the bass lines to the chord progression

We'll start with tempo, root note, scale, and a simple bass line:

    Clock.bpm = 128
    Root.default=”A”
    Scale.default=Scale.minor
    bassline=[0, 0, 0, 0, 0, 0, 0, 1]

Now let's build chords along the minor chord, like: Am, Bm / A, G / A, Am.

    chords = var([(0, 2, 4), (1, 3, 5), (0, 2, 4), (-1, 1, 3), (0, 2, 4)])

Bm / A and G / A mean "above A" because the bass line still keeps A as the root of the chord.

The corresponding synth examples for bass and chords are:

    s1 >> jbass(bassline, oct=3, dur=1/2, shape=2/5) #Bass
    s2 >> dirt(chords, oct=5, dur=[4, 3, 1, 4, 4], amplify=3/5) #Akkorde

Drums:

    b1 >> play("<V....V..><..o.><....k..d>←--[--]>", sample=var([4, 2], 16), amplify=2/5)
    b2 >> play(var(["[ss]",".[ss]"]), amplify=3/5)

Some additional notes on a bass line:

**Only use one note at the time, as low frequency easy go “muddy”!**


#### [Back to Index](#index)

### 3.3. Scales and Modes <a name="scales"></a>

A musical scale, or scale, is technically defined as a series of ascending or descending unitary tones that form a range of notes that can be used to form a melody. Most of the scales in Western music correspond to a specific key. That is, a sequence of notes that is major or minor by default. This does not apply to the chromatic scale, which is a scale of all possible semitones in Western music. The whole tone scale is also a scale that consists of intervals that are two semitones apart.

Within a given key there are 7 notes in a single octave before reaching the 8th note, which has the same name as the first note and is twice the frequency. The seven notes have different intervals between adjacent notes. Sometimes it's a semitone (semitone), sometimes it's a whole tone (two semitones). The pattern of whole tone / semitone intervals that determine the notes of a key, starting with the note while the key is named, is whole-whole-half-whole-whole-whole-half. Within a single key, any of these seven notes could be used as the base note of an ascending sequence. Any such sequence created by starting with a different note in the key is a mode of that key, and each mode has a name. For example:

***W >> Whole Note***<br>
***H >> Half Note***

* Ionian - begins with the "tonic"; the note for which the clef is named. In the key of C, the Ionic mode begins with C. This mode is the most common and is colloquially referred to as the "major scale". The pattern is WWHWWWH.
* Dorian - starts with the next note higher in key than the tonic (D, in the key of C). WHWWWHW.
* Phrygian - starts with the note that is a major third higher than the tonic (E). HWWWHWW.
* Lydian - begins with the note that is a full fourth higher than the tonic (F). WWWHWWH.
* Mixolydian - starts on the note that is a fifth higher than the tonic (G). WWHWWHW.
* Aeolian - begins with the note a major sixth higher than the tonic (A). This mode is also very important in modern music and is known as the "natural minor scale". WHWWHWW.
* Locrian - begins with the note a major seventh higher than the tonic (Bb). HWWHWWW.


In FoxDot, python lists are used to contain all notes of a scale taken from the chromatic scale. Like with a piano, one octave has 12 keys. The chromatic scale in FoxDot:

>> print(Scale.chromatic)

***Output:*** P[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

The difference between the traditional version and the FoxDot version is exactly 1. While the traditional music theory works with 1 - 12 for an octave in relation with a scale or mode, FoxDot scales and notes start with 0 - 11. This has to do with handling lists in python.

Here an example:

>> print(Scale.major)

***Output:*** P[0, 2, 4, 5, 7, 9, 11]

The list of the major scale are the numbers that refer to the note in the FoxDot chromatic scale.

This scale has 7 notes. In Python the position of numbers in a list starts with 0, so we have a list of numbers we can address from 0 to 6.

Here an example:

  First, I create a variable, that contains the particular Scale I want to use:

  >> scale = Scale.major

  >> print(scale)

  ***Output:*** P[0, 2, 4, 5, 7, 9, 11]

  In order to play or call the notes in the list, you use the index of a list in Python with an index number embedded in squared brackets:

  >>print(scale[1])

  ***Output:*** 2

  The last example shows, what you actually do, when using numbers in FoxDot as notes. You actually calling the indexed position of the list of notes dedicated by setting the scale (Default: Scale.major)

  So if the notes are notes = [0, 1, 2]...it actually means ***Play the 1st, 3rd and 5th note of that scale***, which is in P[0, 2, 4, 5, 7, 9, 11] (Scale.major) 0 = 0, 1 = 2, and 2 = 4.

---

**Scales chart**

<img src="images/foxdot_scalen.jpg" width="960"/><br>


#### 3.3.1. Using Modes

**Intro Examples 1:**

Lets start of with Intro Example 1: Speed, Root and Scale

Tempo:

    Clock.bpm=140

Root **D**:

    Root.default.set(2)

Scale:

    Scale.default = Scale.dorian

Drums:

Bass Drum 1:

    b1 >> play("V..v....", rate=4/5, sus=4, sample=2, formant=var([4,3,2,1],4), lpf=1000, amplify=3/5)

Bass Drum 2:

    b2 >> play("v..V....", rate=1, sample=-1, room=1/2, mix=1/2, amplify=4/5)

Bass Drum 3:

    b3 >> play(Pvar(["..","V."], 64), rate=2, sample=-1, amplify=3/5)

Snare 1:

    b4 >> play(Pvar(["....o...","....o..o"], [12, 4]), rate=7/5, sample=4, lpf=linvar([2400, 4000], 8), room=4/5, mix=1/2, amplify=3/5)

Snare 2:

    b5 >> play(Pvar(["..[i.].","..i[.i]"], [12, 4]), sample=7, room=2/3, mix=1/2, amplify=3/5)

Shaker:

    b7 >> play(".[ss]..", rate=7/5, room=1/2, mix=1/2)

HiHat 1:

    b8 >> play("-.-.").every(16, "mirror")

HiHat 2:

    b9 >> play(Pvar(["..","-[--]"],64), sample=8, amplify=4/5)


Next we set some bass lines, while first will be the reference for chords.

Set bass lines:

    bassline=[0, 2, 4, 0, 7]				
    bassline2 = PShuf(bassline)

Bassline 1:

    s1 >> klank(bassline, oct=4, dur=[6, 4, 2, 2, 2], shape=1/5, amplify=3/5)

Bassline 2:

    s2 >> space(PShuf(Scale.default), oct=3, shape=1/4, room=3/5, mix=1/2)

Now create chords made of bassline 3rd and 5th note, while inverse notes the are to high or to low for a good chord progression:

    chords = Pvar([[-3, 0, 2], [-3, -1, 2], [-3, -2, 1], [-3, 0, 2], [-3, 0, 2]], [6, 4, 2, 2, 2])
    s3 >> pluck(chords, oct=6, dur=1/2, room=4/5, mix=1/2, shape=2/6, amplify=3/5)

Finally a melody from the chord notes:

    seq = [2, -1, -3, 2, 5, 4, 2] 			
    s4 >> sinepad(seq, oct=6, dur=[3, 3, 2, 3, 1, 2, 2], room=3/4, mix=1/2, amplify=4/5)


**Intro Beispiel 2:**

And here is another Intro example: Again, first speed, root note, scale.

Tempo:
    Clock.bpm=120

Root:

    Root.default.set(4)

Scale:

    Scale.default=Scale.phrygian

Drums:

Bass Drum:

    b1 >> play("d..d....", rate=3/5, sus=2, sample=8, formant=1, lpf=1400, room=3/4, mix=1/2, amplify=3/5)

Snare:

    b2 >> play(Pvar(["....g...","....g..g"],4), rate=1, sample=4, room=2/5, mix=1/2, amplify=2/5)

HiHat:

    b3 >> play("----", rate=7/5, sample=2, amplify=2/5)

Trippy Bassline:

    bassline=[0, 0, 0, 0, 0, 0, 0, 1]
    bassline2=[-1, -1, -1, -1, -1, -1, -1, 0] #Hier eine Sekunde für eine bessere Akkordfolge
    s1 >> donk(Pvar([bassline, bassline2, bassline], [96, 32, 32]), oct=5, dur=1/2, shape=1/3, amplify=3/5, amp=var([0, 1],[16, 144]))
    s2 >> bass(Pvar([bassline, bassline2, bassline], [96, 32, 32]), oct=5, dur=1/2, shape=1/5, amplify=3/5, amp=var([0, 1],[32, 128]))

Chord progression:

    chords = Pvar([(0, 2, 4), (0, 2, 5), (0, 3, 5), (0, 3, 4)])
    chords2 = Pvar([(-1, 2, 4), (-1, 2, 5), (-1, 3, 5), (-1, 3, 4)])

Synths:

    s3 >> saw(Pvar([chords, chords2, chords],[96, 32, 32]), oct=6, dur=4, room=3/5, mix=1/2, lpf=1300, amplify=4/5, amp=var([0, 1],[32, 128]))

Melody:

    seq = [2, -1, -3, 2, 5, 4, 2]
    s4 >> sinepad(seq, oct=6, dur=[3, 3, 2, 3, 1, 2, 2], shape=1/5, formant=0, room=3/4, mix=1/2, amplify=3/5, amp=Pvar([0, 1],[48, 16, 16, 16, 16, 16, 16, 16]))

**Note: As you may have seen in the last two examples, I set the instrument's volume with amplify, while I set the master volume with amp for composing purposes. As they are Intro examples, we do not need Groups to arrange the composition. We will have Groups later. For now, the instruments are switch on and off directly by an amp list, that counts through beats.**

amp=var([0, 1],[16, 144])

Pvar([bassline, bassline2, bassline], [96, 32, 32])


#### 3.3.2. Using Scales

* A scale is essentially a subset of the musical notes (pitches) between one note, e.g. C, and the same one an octave higher.
* The starting note is the key of the scale.
* Starting at C, these notes are:

| C | C# | D | D# | E | F | F# | G | G# | A | A# | B (H) |
|----|----|----|----|----|----|----|----|----|----|----|----|

* This set of all the notes is called the chromatic scale.
* If this was a Python list called chromatic, then chromatic[0] would return C, chromatic[1] would return C#, chromatic[2] would return D, and so until chromatic[11], which would return B.
* Because each musical scale is a subset of these pitches, we can think of each scale as a list of indices for accessing pitches in the chromatic scale.

chromatic = [C, C#, D, D#, E, F, F#, G, G#, A, A#, B]

| C | C# | D | D# | E | F | F# | G | G# | A | A# | B (H) |
|----|----|----|----|----|----|----|----|----|----|----|----|
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 |

* To see a list of the scales available just run command print(Scale.names()).
* By default, each player uses a globally accessibly default scale called Scale.default
* This can be changed in 3 ways:

Simply assigning the scale object to Scale.default:

    Scale.default  = Scale.minor

You can use the string name:

    Scale.default = "minor"

You can also use the "set" method, which allows more options:

    Scale.default.set("minor")

It is also possible to change the scale of players individually.

Force a player to use the minor scale:

    p1 >> pluck([0, 1, 2, 3], scale=Scale.minor)


#### Exercise:
*Use the following code to iterate through all of the available scales FoxDot provides.*

Displays all available scales:

    print(Scale.names())

Assign the selected scale as the default:

    Scale.default=Scale.chromatic

Variable to assign a step to each note on the scale:

    steps=len(Scale.default)

Play the notes through the scale:

    p1 >> pluck(P[:steps])


#### [Back to Index](#index)

### 3.4. BeatBox <a name="beatbox"></a>

* Try to add variations, modulations, and/or swing to your beats to keep it alive.
* Variations are changes in the beat structure from one bar to another.
* Modulations are effects on the entire drum set, or on single parts of the drum set.
* Adjust some off notes to get a different dynamic within the beat, give your beat some swing.
* Be careful not to get to dynamic, thus losing the drive through the bass drum.

#### 3.4.1. Creating beats

Start with the basic pattern made of a kick, a snare, and a HiHat.

    b1 >> play(“X...X...”)
    b4 >> play(“..o...o.”)
    b7 >> play(“-.-.-.-.”)

“.” (dot) is used as a placeholder to make it easier to see.

As we will increase our beat in the future, leave 3 player for the drum or drum-like sounds, 3 player for snare and snare-like sounds, and 3 for hihat, openhats s.o.

Now lets add a variation to the hihat:

    b7 >> play(“-.-.-.-.”).every(16, ”mirror”).every(8, ”stutter”, 2)

And here another example:

    b8 >> play(“--------”, sample=3, amplify=[1/3, 1/3, 2/3, 1/3, 1/3, 2/3, 1/3, 2/3])

You also can add “ghost” notes, that are usually quieter 16 offbeat notes before or after the “main” note. For this, we will use <> for layering to adjust the volume to the ghost note:

    b4 >> play(“<..o...o.><.[.o]......>”, amplify=(3/4, 1/3))


#### 3.4.2. Attributes

Here the ones, that work with samples are following:

*dur, delay, sample, sus, pan, slide, slidedelay, glide, glidedelay, bend, benddelay, coarse, striate, rate, pshift, hpf, hpr, lpf, lpr, swell, shape, chop, tremolo, echo, echotime, spin, cut, verb, room, mix, formant, shape, drive, blur*

For example, if you use *pshift*, you can change the pitch of the sample:

    b1 >> play("#", dur=2, pshift=linvar([0, 8], 16))


#### Exercise:

**The following examples will help you to experience the concept using familiar rhythms and beats. In addition, use your own arguments.**

Source: [341_Beatbox_CreatingBeats.py](/files/code/341_Beatbox_CreatingBeats.py)

#### Example: House Beat

Tempo:

    Clock.bpm=128

Bass:

    b1 >> play("X.", rate=4/5, sample=2, amplify=2/3)

Clap:

    b4 >> play("..*.", sample=3, amplify=2/5)

Snare:

    b5 >> play("......o.", rate=7/5, sample=1, amplify=1/2)

HiHat:

    b7 >> play(".-", rate=4/5, sample=3, delay=PRand([0, Pwhite(-1/20, 1/20)]), amp=2/3)

Cymbal:

    b8 >> play("#", rate=5/4, sample=0, dur=16, sus=8, amplify=4/5)


#### Example: Drum N Bass

Tempo:

    Clock.bpm=170

Bass:

    b1 >> play("V....V..VV...V..", rate=4/5, sample=2, amplify=2/3)
    b2 >> play("v......[vvvv]", sample=4, amplify=2/3)

Snare:

    b4 >> play(Pvar(["..o.","..o[.o.]"],[12, 2]), sample=2, amplify=2/5)
    b5 >> play("..i.", amplify=PRand([3/5, PWhite(2/3, 3/5)]))

Shaker:

    b7 >> play("s", rate=4/5, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))

Closed HiHat:

    b8 >> play("-", rate=7/5, pshift=linvar([0, 16], 8), sample=2, shape=1/3, amplify=7/5)


#### Example: Dubstep

Tempo:

    Clock.bpm=140

Bass:

    b1 >> play(Pvar(["V...V...", "V[..V.]..[V..V][..V.].[..V.]"], 16), dur=1, rate=6/5, sample=6, amplify=2/3)
    b2 >> play(Pvar(["X...X...", "X[..X.]..[X..X][..X.].[..X.]"], 16), dur=1, sample=2, amplify=2/3)
    b3 >> play(Pvar(["v...v...", "v[..v.]..[v..v][..v.].[..v.]"], 16), dur=1, sample=4, amplify=2/3)

Snare:

    b4 >> play(Pvar(["..o...o.", "..o...oo"], 16), dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(Pvar(["..i...i.", "..i...ii"], 16), dur=1, sample=4, amplify=2/5)
    b6 >> play(Pvar(["..h...h.", "..h...hh"], 16), dur=1, sample=5, amplify=2/5)

Closed HiHat:

    b7 >> play("-", dur=1/2, rate=3/5, pshift=linvar([0, 8], 8), sample=4, amplify=4/5)
    b8 >> play("s", dur=1/2, rate=1, sample=1, amplify=PRand([3/5, PWhite(2/3, 3/5)]))

BuildUp:

    c1 >> play("V.", dur=Pvar([1, 1/2, 1/4, 1/16],[16,8,4,4]), rate=6/5, sample=6, amplify=Pvar([2/3, 0], [30, 2]))
    c2 >> play("X.", dur=Pvar([1, 1/2, 1/4, 1/16],[16, 8, 4, 4]), sample=2, amplify=Pvar([2/3, 0], [30, 2]))
    c3 >> play("v.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=4, amplify=Pvar([2/3, 0], [30, 2]))
    c4 >> play("o.", dur=Pvar([1, 1/2, 1/4, 1/16],[16, 8, 4, 4]), rate=3/4, sample=2, amplify=Pvar([2/5, 0], [30, 2]))
    c5 >> play("i.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=4, amplify=Pvar([2/5, 0], [30, 2]))
    c6 >> play("h.", dur=Pvar([1, 1/2, 1/4, 1/16], [16, 8, 4, 4]), sample=5, amplify=Pvar([2/5, 0],[30, 2]))

Erstelle Gruppen, um eine ganze Sammlung von Playerobjekten rechtzeitig zu steuern:

    gB = Group(b1, b2, b3, b4, b5, b6, b7, b8)
    gC = Group(c1, c2, c3, c4, c5, c6)

Kontrolliere die Lautstaerke im Laufe der Zeit:

    gB.amp=Pvar([1, 0], [64, 32])
    gC.amp=Pvar([0, 1], [64, 32])


#### Exercise:
*Add stretch, pshift, rate or reverse to create different patterns!*


#### Example: Trap

Tempo:

    Clock.bpm=140

Bass:

    b1 >> play("[VV]..V[.V]V.[.V].V..V.V.V.", dur=1, rate=6/5, sample=-1, amplify=2/3)
    b2 >> play("[XX]..X[.X]X.[.X].X..X.X.X.", dur=1, sample=2, amplify=2/3)

Snare:

    b4 >> play("..o.", dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(".H..", dur=1, rate=7/5, sample=1, pan=(-5/7, 5/7), amplify=3/5)

Closed HiHat:

    b7 >> play("[--]", dur=PRand([4, 2, 1, 1/2, PDur(3,8)*2, PDur(3,7)*2], 1/4), rate=3/4, sample=3, amplify=3/5)
    b8 >> play("[--]", dur=PRand([4, 2, 1, 1/2, 1/4]), rate=2/4, sample=-1, amplify=3/5)

Here are a few instruments:

    s1 >> dub(PRand([0, 2, 3], 1/4), oct=(3,4), dur=4, chop=PRand([6, 8]), shape=2/3, amplify=1/3)
    s2 >> space(s1.degree, oct=(4,5), dur=4, chop=PRand([3, 4]), room=3/5, mix=1/2, amplify=6/5).offbeat()
    s3 >> pulse([2, 3, 5, 7, 9], oct=var([3, 4, 5]), dur=PRand([1/2, 1/4], 6), shape=2/3, formant=var([3, 0, 2], 1/2), room=3/4, mix=1/2, pan=[-2/3, 2/3], amplify=3/5)


#### Example: HipHop

Tempo:

    Clock.bpm=80

Bass:

    b1 >> play("X..X....X.XX....", rate=var([4/5, 1], 8), formant=2, sample=5, amplify=5/3, amp=1)

Snare:

    b4 >> play("..i.", rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)]))
    b5 >> play(".H.......H......", dur=1/2, rate=7/5, sample=1, delay=1/16, pan=(-5/7, 5/7), amplify=2/5)

Closed HiHat:

    b7 >> play("--.-", rate=3/4, sample=3, amplify=3/5)

Open Hat / Shaker:

    b8 >> play(".............#..", rate=7/5, sample=2, amplify=1, amp=1)
    b9 >> play("[ss]", rate=3/4, sample=2, hpf=linvar([800, 6000], 1), amplify=3/5, amp=1).every(PRand([4, 8, 12, 16]), "stutter", PRand([2, 3, 4]))


#### Example: Footwork

Tempo:

    Clock.bpm=154

Bass:

    b1 >> play("X..X..X.X..X..X.", dur=1, rate=6/5, sample=-1, amplify=2/3)
    b2 >> play("V..V..V.V..V..V.", dur=1, sample=1, amplify=2/3)
    b3 >> play("{([XX])([X.])([X...])}", dur=1, rate=PRand([3/4, 3/5, 1, 7/5], 1/4), shape=linvar([1/7, 3/5], 16), amplify=2/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))

Snare:

    b4 >> play("............H...", dur=1, rate=3/4, sample=2, amplify=PRand([3/5, PWhite(2/3, 3/5)])).every(PRand([4, 8, 12]), "stutter", PRand([2, 3]))
    b5 >> play("......o.......o[oo.o]",dur=1,rate=7/5,sample=1,pan=(-5/7,5/7),amplify=3/5)
    b6 >> play("",dur=PRand([4, 2, 1, 1/2, PDur(3, 8)*2, PDur(3,7)*2], 1/4), rate=2/4, sample=-1, amplify=3/5)

HiHat:

    b7 >> play("..-.....", rate=3/4, sample=3 , amplify=3/5)
    b8 >> play("-", dur=1, sample=3, amplify=4/5)
    b9 >> play("{([--])(M)}", dur=1, rate=PRand([3/4, 3/5, 1, 7/5], 1/4), sample=2, shape=linvar([1/7, 3/5], 16), amplify=1/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([3, 5]))


#### Example: Funk

Tempo:

    Clock.bpm=118

Bass:

    b1 >> play("VV...[VV]..", dur=1/2, rate=6/5, sample=-1, amplify=1/3)
    b2 >> play("VV...[VV]..", dur=1/2, sample=linvar([0, 5], 4), amplify=1/3)

Snare:

    b4 >> play("..[o.][.o][.o].[o.][.o]", dur=1/2, rate=2, sample=5, amplify=PRand([2/5, PWhite(1/3, 2/5)]))
    b5 >> play("....i..i.i..i..i", dur=1/4, rate=1, sample=3, pan=(-5/7, 5/7), amplify=3/5)
    b6 >> play("..o.", dur=1, rate=2, sample=5, pan=(-5/7, 5/7), amplify=3/5)

HiHat:

    b7 >> play("[-.-.][-.--][-...][-.-.][--..][-.-.][-.-.][-...]", dur=1, rate=1, sample=2, amplify=1)
    b8 >> play("[-.]", dur=1/2, rate=1, sample=-1, amplify=4/5, amp=1)
    b9 >> play("[ll].-.", sample=var([3, 4, 0], 16), formant=linvar(5, 8), amplify=4/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))
    b0 >> play("[ss]", rate=1, sample=2, shape=2/3, amplify=4/5, amp=1).every(PRand([2, 4, 8, 16]), "stutter", PRand([2, 3, 5]))
    gBeats = Group(b1, b2, b3, b4, b5, b6, b7, b8, b9, b0)


#### 3.4.2. Creating transitions

* Ramp up, then breath, then beat again (drum roll...silence…beat). Here one can excellently the group assignments z. B. with *gBeats.hpf = linvar([0, 5000], [12, 0], start = Clock.now ())*, then all of a sudden *gBeats.amp = var([0, 1], [4 , inf], start = Clock.now ())*.
* Subtract before you add, like no bass beat only snare and hihat.
* RRoll and ramp it up with 8th and 16th notes of e.g. snare, hihat, shaker.
* If you need a transition from one section to another, without big subtraction like taking drum beat out, be subtle.


#### [Back to Index](#index)


### 4. Live Jam Session – Let us play all together <a name="livejam"></a>

* In order to collectively create music with FoxDot, we will use [Troop](https://github.com/Qirky/Troop), a server <--> client environment written in Python
* This will create a server<->client connection through a network, preferably using Wifi to connect to the network.


### 4.1. Connect to the Wifi Network <a name="wifi"></a>

* In order to collectively create music with FoxDot, we will use Troop, a server <--> client environment written in Python.
* FoxDot will use OSC (a digital better of ol' MIDI) to communicate, which is the same way it communicates to SuperCollider under the hood.
* The server will only deal with all the clients OSC messages.
* The clients will run the SuperCollider audio server underneath of FoxDot in order to get sound out of the can.
* If you use your own network, check on the server computer the IP address, that is used to connect to the Wifi hotspot. This will be the IP you are using in Troop!
* In order to create a WiFi offline network, one of the participating PCs / Laptops can be used as a hotspot router. The others can then connect to each other via WiFi. The hotspot router PC / laptop can then also run the Troop server script in parallel.
* Another possibility of an offline network is the use of the [W.O.N.D. Dongle](https://gitlab.com/iShapeNoise/wond), which I designed for this reason.

### 4.2. Run Troop <a name="runtroop"></a>

* Run Supercollider, and execute *FoxDot.start* (If this is your first time, execute *Quark.gui* first. Select "FoxDot" and "BatLib", and recompile).
* Open a terminal and run the server script. ***Only the provider of the server.***
* Everyone, including the person who runs the server, run the client script on their computers.
* The client program will open a popup window. Check on the ip the terminal windows shows, and use it to fill in Host (IP Adress), Name (Choose one!), and the password the server person will provide.
* After pressing “ok”, you should see a window, that looks exactly like the FoxDot GUI text editor.


#### Jamming:

*Always start with a low volume when you insert a new instrument or sample and slowly fade in the sound with amplify or amp in time with the rhythm!*

1. At first start small with one of the instruments and samples you choose.
2. If someone elses  uses the instrument or sample already, use the next in your personal list.
3. Stay with that one instrument / sample, and try different effects and attributes with it.
4. Take your time, this will be messy at first.
5. Do change the lines from each other, you will learn from others through changes.
6. You can save the code to keep snapshots.
7. After 10-30 minutes, take a break to relax your ears and / or start over.
8. And don't forget: Collaborative music-making is about the joy of making music together.

## END OF PART 1

### I hope you enjoyed the Part 1 of this course. The second part of this course is going deeper into music theory, composing in FoxDot, building your own synths, connecting external programs and equipment and much more.

***Many thanks for taking the time, and if you got any feedback, it is much appreciated!***

#### contact@jensmeisner.net
