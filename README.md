# FoxDot_CodingMusic_Part1


![](images/FoxDot_CodingMusic_FeatureImg.png)

---

## Description

FoxDot is an easy-to-use Python library that creates an interactive programming environment and talks to the powerful sound synthesis engine, called SuperCollider to make music. FoxDot schedules musical events in a user-friendly and easy-to-grasp manner that makes live coding easy and fun for both programming newcomers and veterans alike.

![](images/FoxDot_Editor.png)

The FoxDot Workshop will lead you through the basics of FoxDot, to point you can make Music with Live Coding alone or with other people together. The FoxDot environment can also be used to write musical compositions in a more traditional way, by dedicating the timing using Python functions.

![](images/FoxDot_Composition.png)

---

## Folder Content

### >> files

* code >> Course code and examples
* flyer >> Course flyer in English and German
* images >> Images used in Course guide
* scales >> Graphics showing all available Scales in English and German
* slides >> Course slides in English and German
* syntax >> Overview of commands and examples

### >> images

* Icons and feature images


---

#### [English Version](files/FoxDot_CodingMusic_Part1_en.md)

#### [Deutsche Version](files/FoxDot_CodingMusic_Part1_de.md)

---

[Flyer (Deutsch)](files/flyer/flyer_de.png)

#### [Workshop Infos DE/EN](https://jensmeisner.net/foxdot-workshop/)
